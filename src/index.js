import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from "react-redux";
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from "./Redux/Reducers"
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
// import AlertTemplate from 'react-alert-template-mui'
import AlertTemplate from 'react-alert-template-basic'
import { Worker } from '@phuocng/react-pdf-viewer';

let store = createStore(rootReducer, applyMiddleware(thunk));

const options = {
  // you can also just use 'bottom center'
  position: positions.BOTTOM_CENTER,
  timeout: 5000,
  offset: '30px',
  // you can also just use 'scale'
  transition: transitions.SCALE
}

ReactDOM.render(
  <Provider store={store}>
     <AlertProvider  template={AlertTemplate} {...options}>
     
     <Worker
            workerUrl="https://unpkg.com/pdfjs-dist@2.4.456/build/pdf.worker.min.js"
        >
            <App />
        </Worker>
          
    </AlertProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
