import React, { useState, useEffect, useReducer } from 'react';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Select from '@material-ui/core/Select';
import DateFnsUtils from '@date-io/date-fns';
import { useSelector, useDispatch } from "react-redux";
import { v4 as uuidv4 } from 'uuid';
import { DataGrid, Column, Editing, Scrolling, Lookup, Summary, TotalItem,SearchPanel } from 'devextreme-react/data-grid';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { fetchBindingInfo, fetchBindingInfofiles, fetchBindingInfofilesview, fetchInforceInfo, fetchInforceInfofiles, fetchInforceInfofilesview } from '../../../../../Redux/Actions/actions';
import FileViewe from '../fileview';
const InforceInfo = () => {
    // The first commit of Material-UI
  const [EffectiveDate, setEffectiveDate] = React.useState(new Date());
  const [ExpiryDate, setExpiryDate] = React.useState(new Date());

  const [selectedFiles, setSelectedFiles] = useState([]);
  const [currentFile, setCurrentFile] = useState([]);
 
  const PolicyReducer = useSelector(state => state.PolicyQuote);
  const stepperList = useSelector((state) => state.TimeLine);
  const [fileInfos, setFileInfos] = useState([]);

  const dispatch = useDispatch();

  const PaymentTypes = ([
    {Ptype:"None"},{Ptype:"Direct Bill"},{Ptype:"Agency Bill"}
    ]);

  const handleEffectiveDate = (date) => {
    setEffectiveDate(date);
    setUserInput(userInput => ({
      ...userInput, 
      ['EffectiveDate']: date
  }));
    console.log("efate",EffectiveDate);
  };
  const handleExpiryDate = (date) => {
    setExpiryDate(date);
    setUserInput(userInput => ({
      ...userInput, 
      ['ExpiryDate']: date
  }));
    console.log("exate",ExpiryDate);
  };

        const [userInput, setUserInput] = useState(
          {
            
            PolicyNo: '',
          
            InForceFiles: []
                 }
          );

          const handleChange = evt => {
            const name = evt.target.name;
            let  newValue = evt.target.value;
            //setUserInput({[name]: newValue});
            setUserInput(userInput => ({
              ...userInput, 
              [name]: newValue
          })

          );
          }
          const selectFile = (event) => {
            // let filedatas = event.target.files;
            // filedatas && filedatas.map((file)=>{
            //   setSelectedFiles(file);
            // })
            setSelectedFiles([
              ...selectedFiles,
                event.target.files[0]
              ,
            ]);
            setFileInfos([
              ...fileInfos,
              {
                Id:uuidv4(),
                Name:event.target.files[0].name,
                fileUrl:URL.createObjectURL(event.target.files[0]),
                filetype:event.target.files[0].type,
              },
            ]);

            console.log("Files Data",event.target.files);
            console.log("Files Path",URL.createObjectURL(event.target.files[0]));
            console.log("Files convert", URL.revokeObjectURL(URL.createObjectURL(event.target.files[0])));
            console.log("Inforce SelectedFiles",selectedFiles);
            
          };
          // useEffect(()=>{
            
          //      console.log("date", userInput)
          // },[selectedFiles])

          useEffect(()=>{
            dispatch(fetchInforceInfofiles(selectedFiles));
            dispatch(fetchInforceInfofilesview(fileInfos));
            setUserInput(userInput => ({
              ...userInput, 
              ['InForceFiles']: fileInfos
          }));
              console.log("date", fileInfos)
              console.log("UserInput",userInput);
         },[fileInfos])

          useEffect(()=>{
          dispatch(fetchInforceInfo(userInput));
          console.log("UserInput2",userInput);
          },[userInput]);

       useEffect(()=>{
           console.log("Boundinfo",PolicyReducer.InforceInfo);
           if (PolicyReducer.InforceInfo) {
             
           
                setUserInput(PolicyReducer.InforceInfo);

                setFileInfos(PolicyReducer.InforceInfofielsview);
                setSelectedFiles(PolicyReducer.InforceInfofiles);
                
            }           

          },[])

          const cellRenderfile = (data) => {
            return <FileViewe Path={data.data.fileUrl} Name={data.data.Name}/>
          }
         
    return(
        <React.Fragment>
            <Grid container  spacing={3} >
            <Grid item md={12}><h3>InForce Details</h3></Grid>
            </Grid>
            <Grid container  spacing={3} >
            <Grid item md={3}>
        <TextField id="PolicyNo" name="PolicyNo" value={userInput.PolicyNo} label="Policy No" onChange={handleChange} variant="outlined" />
        </Grid>
            </Grid>
            <Grid container  spacing={3} >
            <Grid item md={12}>
            <h4 className="clrBlue" style={{'marginTop':'20px'}}>UPLOAD YOUR FILES</h4>
                <div className="fileUpload">
                <Button variant="contained" name="save" color="primary" type="button" ><CloudUploadIcon></CloudUploadIcon></Button>
                <input type="file" className="custom-file-upload"  onChange={selectFile} />
                </div>
            
                </Grid>
            </Grid>
            <Grid container  spacing={3} >
            <Grid item md={12}>
                
            <div className="card" style={{'width':'700px'}}>
        <div className="card-header">List of Files</div>
            <DataGrid
        id="gridContainer"
        dataSource={fileInfos}
        //keyExpr="ID"
        showBorders={true}
        >
        <Editing
          mode="row"
          useIcons={true}
          allowDeleting={true}
          allowUpdating={true}
          />
        <Column type="buttons" width={150}
          buttons={['delete']} />
        <Column dataField="Name" width={400} caption="Name" />
        <Column dataField="Id"  caption="Actions" width={200} cellRender={cellRenderfile} />
      </DataGrid>
</div> 
</Grid> 
            </Grid>
           
        </React.Fragment>
    )
}
export default InforceInfo