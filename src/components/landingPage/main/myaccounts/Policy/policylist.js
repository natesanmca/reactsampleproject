import React, { useState, useEffect } from 'react';

import { DataGrid, Column, Editing, Scrolling, Lookup, Summary, TotalItem, SearchPanel ,HeaderFilter,FilterRow} from 'devextreme-react/data-grid';
//import { Button } from 'devextreme-react/button';
import Button from '@material-ui/core/Button';
import { SelectBox } from 'devextreme-react/select-box';

import CustomStore from 'devextreme/data/custom_store';
import DataSource from "devextreme/data/data_source";
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { formatDate } from 'devextreme/localization';
import 'whatwg-fetch';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import axios from 'axios';
import history from '../../../../../history'
import { useHistory, Link } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { useSelector, useDispatch, connect } from "react-redux";
import { confirmAlert } from 'react-confirm-alert';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import {
    fetchSubmissionNumber, fetchCheckState, fetchSelectedPolicy, fetchTimeLine, fetchType, fetchQuotationNo, fetchInsurerInfo, fetchBindingInfo,
    fetchInsurerInfofilesview, fetchBindingInfofilesview, fetchStatusType
} from '../../../../../Redux/Actions/actions';

const API_URL = process.env.REACT_APP_BASE_API_URL;
const UserStatus = [{
    'StatusId': 0,
    'Status': 'InActive'
},
{
    'StatusId': 1,
    'Status': 'Active'
}];

const userType = [
    {
        'Id': 1,
        'Type': 'Administrator'
    },
    {
        'Id': 2,
        'Type': 'BDR'
    },

    {
        'Id': 4,
        'Type': 'Manager'
    },
    {
        'Id': 5,
        'Type': 'BDM'
    }];



const PolicyList = () => {
    //const users = useSelector(state => state.users.items);
    const Account = useSelector(state => state.Account);
    const [userInput, setUserInput] = React.useState(
        { 
          Insurer:'',
          underwriter: '',
          UnderwriterEmail:'',
          Status:'8',
          Subid:'',
          Accid:0
                  }
        );
    const [hasError, setErrors] = useState(false);
    const [users, setusers] = useState({});
    const [Default, setDefault] = React.useState([]);
    const [Default1, setDefault1] = React.useState([]);
    const [E2Default, setE2Default] = React.useState([]);
    const [E3Default, setE3Default] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    
    
    const [count, setCount] = useState(0);
    let historyNew = useHistory();
    const [status, setstatus] = useState([{
        'StatusId': 0,
        'Status': 'InActive'
    },
    {
        'StatusId': 1,
        'Status': 'Active'
    }]);
    const [types, settypes] = useState([
        {
            'Id': 1,
            'Type': 'Administrator'
        },
        {
            'Id': 2,
            'Type': 'BDR'
        },

        {
            'Id': 4,
            'Type': 'Manager'
        },
        {
            'Id': 5,
            'Type': 'BDM'
        }]);
    const dispatch = useDispatch();
    const pageSizes = [5, 10, 20];

    const Datasource = {
        UserData: new CustomStore({
            key: 'SubMissionId',
            load: () => updateData(`${API_URL}Quote/get-pending-submission/${Account.AcNo}/${3}`),
        }),
        QuotedData: new CustomStore({
            key: 'InsurerQuoteId',
            load: () => updateData(`${API_URL}Policy/get-policy-quoted/${Account.AcNo}/${5}`),
            update: (key, values) => updateData(`${API_URL}Policy/update-quote-status/${key}/${values.QuotedStatus}`)
        }),
        InBoundData: new CustomStore({
            key: 'InsurerQuoteId',
            load: () => updateData(`${API_URL}Policy/get-policy-quoted/${Account.AcNo}/${7}`),
            update: (key, values) => updateData(`${API_URL}Policy/update-quote-status/${key}/${values.QuotedStatus}`)
        }),
        InforceData: new CustomStore({
            key: 'InsurerQuoteId',
            load: () => updateData(`${API_URL}Policy/get-policy-quoted/${Account.AcNo}/${8}`),
        }),
        QuotedStatusData: new CustomStore({
            key: 'StatusId',
            loadMode: 'raw',
            load: () => getQuotedstatus()
        }),
        BoundStatusData: new CustomStore({
            key: 'StatusId',
            loadMode: 'raw',
            load: () => getBoundstatus()
        })
    };

    

    const handleClickOpen = (e) => {

        setOpen(true);
        setCount(e.row.data)
        console.log(e.row.data);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const updateData = (url, data) => {

        // const Bdata = JSON.stringify({
        //   Acno : data.Acno
        // });

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
            //body: Bdata
        };
        return fetch(url, requestOptions)
            .then(handleResponse, handleError)
            .then(user => {
                // login successful if there's a jwt token in the response
                if (user && user.Success) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    //localStorage.setItem('user', JSON.stringify(user));
                }
                return user.Data;

            });
    }

    const updatestatus = (url, data) => {

        // const Bdata = JSON.stringify({
        //   Acno : data.Acno
        // });

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
            //body: Bdata
        };
        return fetch(url, requestOptions)
            .then(handleResponse, handleError)
            .then(user => {
                // login successful if there's a jwt token in the response
                if (user && user.Success) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    //localStorage.setItem('user', JSON.stringify(user));
                }


            });
    }
    const handleResponse = (response) => {
        return new Promise((resolve, reject) => {
            if (response.ok) {
                // return json if it was returned in the response
                var contentType = response.headers.get("content-type");
                if (contentType && contentType.includes("application/json")) {
                    response.json().then(json => resolve(json));
                } else {
                    resolve();
                }
            } else {
                // return error message from response body
                response.text().then(text => reject(text));
            }
        });
    }

    const handleError = (error) => {
        return Promise.reject(error && error.message)
    }
    function getBoundstatus() {
        return [{
            'StatusId': 7,
            'Status': 'Bound'
        },
        {
            'StatusId': 8,
            'Status': 'InForce'
        }];
    }
    const getQuotedstatus = () => {
        return [
            {
                'StatusId': 5,
                'Status': 'Quoted'
            },
            {
                'StatusId': 6,
                'Status': 'Closed Lost'
            },
            {
                'StatusId': 7,
                'Status': 'Bound'
            }];
    }
    function gettype() {
        return [
            {
                'Id': 1,
                'Type': 'Administrator'
            },
            {
                'Id': 2,
                'Type': 'BDR'
            },

            {
                'Id': 4,
                'Type': 'Manager'
            },
            {
                'Id': 5,
                'Type': 'BDM'
            }];
    }


    const cellRender = (data) => {
        // return <img src={data.value} />/users/${user.id} activeClassName="active"
        return <Link to={{ pathname: `/myaccounts/accountDetails/policyQuote/LandingPolicy`, state: { id: data.value, type: 'submit' } }}>
            <Button variant="contained" color="primary">
                Enter Quotation
            </Button>
        </Link>
        // return <Link to={{pathname:`/myaccounts/accountDetails/submission/coverage`, state:{id:data.value}}} >{data.value}</Link>
    }
    const decline = (data) => {
        return <Link to={{ pathname: `/myaccounts/accountDetails/decline`, state: { id: data.value, submission: data.SubmissionNo } }}>
        <Button variant="contained" color="primary">
            Decline
        </Button>
    </Link>
    }
    const MoveInBound = (e, data, type, str) => {
        console.log("InBoundData", data);

        if (str === 'Quoted') {

            if (data.row.data.hasbound) {

                if (data.row.data.hasbound) {

                }
                confirmAlert({
                    title: 'Status Change',
                    message: 'Are you sure you want to the change status?',
                    buttons: [
                        {
                            label: 'Yes',
                            onClick: () => UpdateStatus(data.data.InsurerQuoteId, type)
                        },
                        {
                            label: 'No'
                        }
                    ]
                })

            }
            else {
                historyNew.push({ pathname: "/myaccounts/accountDetails/policyQuote/LandingPolicy", state: { id: data.value, type: 'Quoted' } });
            }
        }
        else if (str === 'Bound') {

            if (data.row.data.hasinforce) {
                confirmAlert({
                    title: 'Status Change',
                    message: 'Are you sure you want to the change status?',
                    buttons: [
                        {
                            label: 'Yes',
                            onClick: () => UpdateStatus(data.data.InsurerQuoteId, type)
                        },
                        {
                            label: 'No'
                        }
                    ]
                })
            }
            else {
                historyNew.push({ pathname: "/myaccounts/accountDetails/policyQuote/LandingPolicy", state: { id: data.value, type: 'Bound' } });
            }

        }

    }
    const handleChange = evt => {
        evt.stopPropagation();   
      const name = evt.target.name;
      let newValue = evt.target.value;
      setUserInput(userInput => ({
        ...userInput, 
        [name]: newValue
      })
      
      );
      }
    const cellRenderQuoted = (data, type, str) => {
        // return <img src={data.value} />/users/${user.id} activeClassName="active"
        // return <Link to={{pathname:`/myaccounts/accountDetails/policyQuote/LandingPolicy`, state:{id:data.value,type:'submit'}}}>
        //             <Button variant="contained" color="primary">
        //                 Enter Quotation
        //             </Button>
        //             </Link>
        return <Button variant="contained" color="primary" onClick={e => MoveInBound(e, data, type, str)}>
            Bind
        </Button>
    }

    const cellRenderBound = (data, type, str) => {
        // return <img src={data.value} />/users/${user.id} activeClassName="active"
        // return <Link to={{pathname:`/myaccounts/accountDetails/policyQuote/LandingPolicy`, state:{id:data.value,type:'submit'}}}>
        //             <Button variant="contained" color="primary">
        //                 Enter Quotation
        //             </Button>
        //             </Link>
        return <Button variant="contained" color="primary" onClick={e => MoveInBound(e, data, type, str)}>
            In Force
        </Button>
    }

    const cellRenderfile = (data, type) => {
        // return <img src={data.value} />/users/${user.id} activeClassName="active"
        return <Link to={{ pathname: `/myaccounts/accountDetails/policyQuote/LandingPolicy`, state: { id: data.data.QuotedNo, type: type } }}>
            {data.value}
        </Link>
        // return <Link to={{pathname:`/myaccounts/accountDetails/submission/coverage`, state:{id:data.value}}} >{data.value}</Link>
    }

    const [update, setupdate] = React.useState(false);

    const cellRenderUpdate = (data) => {
        console.log("hasbound", data.row.data.hasbound);
        console.log("hasbounddata", data);
        return data.row.data.hasbound;
    }

    const UpdateStatus = (key, statusid) => {
        axios.post(`${API_URL}Policy/update-quote-status/${key}/${statusid}`)
            .then(res => {

                if (res.data.Success) {
                    //let bdmList = res.data.Data;
                    //setDefault(res.data.Data);
                    Dataload();
                }


                // console.log(operationList);
                //console.log(itemList);

            });
    }

    const Dataload = () => {
        console.log("dataload");
        axios.post(`${API_URL}Policy/get-policy-quoted/${Account.AcNo}/${0}`)
            .then(res => {

                if (res.data.Success) {
                    //let bdmList = res.data.Data;
                    setDefault(res.data.Data);
                }


                // console.log(operationList);
                //console.log(itemList);

            });
    }
    useEffect(() => {
        //dispatch(getAllUsers());
        // fetchData();
        setstatus(UserStatus);
        settypes(userType);
        // `${API_URL}Policy/get-policy-quoted/${Account.AcNo}/${8}
        Dataload();
        dispatch(fetchQuotationNo("0"));
        dispatch(fetchType("InsurerQuote"));
        dispatch(fetchStatusType(""));
        dispatch(fetchInsurerInfo({}));
        dispatch(fetchCheckState({}));
        dispatch(fetchTimeLine([]));
        dispatch(fetchSelectedPolicy([]));
        dispatch(fetchInsurerInfofilesview([]));
        dispatch(fetchBindingInfo({}));
        dispatch(fetchBindingInfofilesview([]));
    }, []);

    useEffect(() => {
        setDefault1([]);
        setE2Default([]);
        setE3Default([]);
        let E1 = [];
        let E2 = [];
        let E3 = [];
        Default.map((dat) => {
            if (dat.QuotedStatus === 5) {
                //setE1Default([...E1Default, {dat}]); 
                E1.push(dat);
            }
            else if (dat.QuotedStatus === 7) {
                //setE2Default([...E2Default, {dat}]);  
                E2.push(dat);

            }
            else if (dat.QuotedStatus === 8) {
                //setE3Default([...E3Default, {dat}]);  
                E3.push(dat);
            }

        });
        setE3Default(E3);
        setE2Default(E2);
        setDefault1(E1);
        console.log("PolicyDefault", Default);
        console.log("PolicyE1Default", Default1);
        console.log("PolicyE2Default", E2Default);
        console.log("PolicyE3Default", E3Default);

    }, [Default]);
    function QuotedPremium(data) {
        let num = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD'
        }).format(data.data.QuotedPremium);
        return num
    }
    function BoundPremium(data) {
        let num = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD'
        }).format(data.data.BoundPremium);
        return num
    }
    function INBoundPremium(data) {
        let num = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD'
        }).format(data.data.BoundPremium);
        return num
    }
    const save = () => {
        const Request = {
            Insurer:userInput.Insurer,
            underwriter:userInput.underwriter,
            UnderwriterEmail:userInput.UnderwriterEmail,
            Status:'8',
            Subid:count.SubMissionId,
            Accid:count.SubmissionNo,
           // otherItem : Default
      
        }
        console.log("test", count);
        console.log("Request",Request)
        // console.log("Policy Submission",PolicyReducer);
    };
    return (
        
        <Container maxWidth="">
        
            {/* <Grid container spacing={3}>
        <Grid item md={12}>
        <p class="statusP"><u>Submitted items</u></p>
            </Grid>
            <Grid item md={4}>
            
            </Grid>
            
            <Grid item md={4}>
       
        
        </Grid> 
        </Grid> */}
            <Grid item md={12}>
                <p class="statusP"><u>Inforce items</u></p>
            </Grid>
            <Grid container spacing={3}>

                <Grid item md={12}>
                    <div className="mainContent">

                        <DataGrid
                            id="gridContainer"
                            dataSource={E3Default}
                            // keyExpr="UserId"
                            showBorders={true}
                            showRowLines={true}
                            showColumnLines={false}
                            wordWrapEnabled={true}>
                                <FilterRow visible={true}/>
                        
                        <HeaderFilter visible={true} />
                            {/* <Editing
                        mode="row"
                        useIcons={true}
                        allowUpdating={true}/> */}
                            <Column dataField="PolicyNo" caption="PolicyNo #" cellRender={e => cellRenderfile(e, 'InForce')} />
                            {/* <Column dataField="SubmissionNo" caption="Submission No" /> */}
                            <Column dataField="Policy" caption="Policy" />
                            <Column dataField="Coverages" caption="Coverages" />
                            {/* <Column dataField="QuotedPremium" caption="Quoted Premium" /> */}
                            <Column dataField="BoundPremium" caption="Premium" cellRender={BoundPremium} />
                            <Column dataField="Expirydate" caption="Expiry date" dataType="date" />
                            <Column dataField="EffectiveDate" caption="EffectiveDate" dataType="date" /> 
                        </DataGrid>
                    </div>
                </Grid>
            </Grid>
            <Grid item md={12}>
                <p class="statusP"><u>Submitted items</u></p>
            </Grid>
            <Grid container spacing={3}>

                <Grid item md={12}>
                    <div className="mainContent">

                        <DataGrid
                            id="gridContainer"
                            dataSource={Datasource.UserData}
                            // keyExpr="UserId"
                            showBorders={true}
                            showRowLines={true}
                            showColumnLines={false}
                            wordWrapEnabled={true}>
                            {/* <Editing
                        mode="row"
                        useIcons={true}
                        allowUpdating={true}/> */}
                            <Column dataField="SubmissionNo" caption="Submission No" />
                            <Column dataField="Policy" caption="Policy" />
                            <Column dataField="Coverages" caption="Coverages" />
                            <Column dataField="SubmissionDate" caption="Create Time" />
                            <Column dataField="SubmissionNo" caption="Action" />
                            <Column dataField="SubmissionNo" caption="Action" cellRender={cellRender} />
                           
                            
                        </DataGrid>
                    </div>
                </Grid>
            </Grid>
            <Grid item md={12}>
                <p class="statusP"><u>Quoted items</u></p>
            </Grid>
            <Grid container spacing={3}>

                <Grid item md={12}>
                    <div className="mainContent">

                        <DataGrid
                            id="gridContainerQuoted"
                            dataSource={Default1}
                            // keyExpr="UserId"
                            showBorders={true}
                            showRowLines={true}
                            showColumnLines={false}
                            wordWrapEnabled={true}>
                            <Editing
                                mode="row"
                                useIcons={true}

                            />
                            <Column dataField="QuotedNo" caption="Quoted No" cellRender={e => cellRenderfile(e, 'Quoted')} />
                            <Column dataField="InsurerName" caption="Insurer Name" />
                            <Column dataField="SubmissionNo" caption="Submission No" />
                            <Column dataField="Policy" caption="Policy" />
                            <Column dataField="Coverages" caption="Coverages" />
                            <Column dataField="QuotedPremium" caption="Quoted Premium" cellRender={QuotedPremium} />
                            <Column dataField="BoundPremium" caption="Bound Premium" cellRender={BoundPremium} />
                            <Column dataField="CreateDate" caption="Create Time" />
                            <Column dataField="QuotedStatus" caption="Change Status" >
                                <Lookup dataSource={Datasource.QuotedStatusData} displayExpr="Status" valueExpr="StatusId" />
                            </Column>
                            {/* <Column type="buttons" caption="Actions" width={110}
                            buttons={['edit']} /> */}
                            <Column dataField="QuotedNo" caption="Action" cellRender={e => cellRenderQuoted(e, 7, 'Quoted')} />

                        </DataGrid>
                    </div>
                </Grid>
            </Grid>
            <Grid item md={12}>
                <p class="statusP"><u>Bound items</u></p>
            </Grid>
            <Grid container spacing={3}>

                <Grid item md={12}>
                    <div className="mainContent">

                        <DataGrid
                            id="gridContainer"
                            dataSource={E2Default}
                            // keyExpr="UserId"
                            showBorders={true}
                            showRowLines={true}
                            showColumnLines={false}
                            wordWrapEnabled={true}>
                            <Editing
                                mode="row"
                                useIcons={true}
                            />
                            <Column dataField="QuotedNo" caption="Quoted No" cellRender={e => cellRenderfile(e, 'Bound')} />
                            <Column dataField="SubmissionNo" caption="Submission No" />
                            <Column dataField="Policy" caption="Policy" />
                            <Column dataField="Coverages" caption="Coverages" />
                            {/* <Column dataField="QuotedPremium" caption="Quoted Premium" /> */}
                            <Column dataField="BoundPremium" caption="Bound Premium" cellRender={INBoundPremium} />
                            <Column dataField="CreateDate" caption="Create Time" />
                            <Column dataField="QuotedStatus" caption="Change Status" >
                                <Lookup dataSource={Datasource.BoundStatusData} displayExpr="Status" valueExpr="StatusId" />
                            </Column>
                            {/* <Column type="buttons" caption="Actions" width={110}
                            buttons={['edit']} /> */}
                            <Column dataField="QuotedNo" caption="Action" cellRender={e => cellRenderBound(e, 8, 'Bound')} />
                        </DataGrid>
                       

                      
                    </div>
                </Grid>
            </Grid>
            
        </Container>

    );

};
export default PolicyList;