import React,{useState, useEffect, useReducer} from 'react';
import axios from 'axios';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';

import { DataGrid, Column, Editing, Scrolling, Lookup, Summary, TotalItem,SearchPanel } from 'devextreme-react/data-grid';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { useSelector, useDispatch } from "react-redux";
import { fetchSubmissionNumber, fetchCheckState, fetchSelectedPolicy, fetchTimeLine, fetchInsurerInfo, fetchInsurerInfofiles, fetchInsurerInfofilesview } from '../../../../../Redux/Actions/actions';
import { v4 as uuidv4 } from 'uuid';
import FileViewe from '../fileview';
const API_URL = process.env.REACT_APP_BASE_API_URL;

const InsurerInfo = (props) => {

  const [selectedFiles, setSelectedFiles] = useState([]);
  const [currentFile, setCurrentFile] = useState([]);
 
  const [QuoteExpiryDate, setQuoteExpiryDate] = React.useState(new Date());
  const PolicyReducer = useSelector(state => state.PolicyQuote);
  const stepperList = useSelector((state) => state.TimeLine);
  const [fileInfos, setFileInfos] = useState(PolicyReducer.InsurerInfofilesview);
  const dispatch = useDispatch();

  
  const handleQuoteExpiryDate = (date) => {
      setQuoteExpiryDate(date);
  };
    const [userInput, setUserInput] = useState(
        {
            Insurer: '',
            InsurerStreetAddress:'',
            InsurerCity:'',
            InsurerPostalCode:'',
            underwriter:'',
            UnderwriterEmail:'',
            InsurerQuoteNumber:'',
            QuoteExpiryDate: QuoteExpiryDate,
            TotalPremium:'',
            // TotalBoundPremium:'',
            InsurerFiles: []
               }
        );
        const handleChange = evt => {
            const name = evt.target.name;
            let  newValue = evt.target.value;
            //setUserInput({[name]: newValue});
            setUserInput(userInput => ({
              ...userInput, 
              [name]: newValue
          })
      );
          }
          
          const selectFile = (event) => {
            // let filedatas = event.target.files;
            // filedatas && filedatas.map((file)=>{
            //   setSelectedFiles(file);
            // })
            setSelectedFiles([
              ...selectedFiles,
                event.target.files[0]
              ,
            ]);
            setFileInfos([
              ...fileInfos,
              {
                Id:uuidv4(),
                Name:event.target.files[0].name,
                fileUrl:URL.createObjectURL(event.target.files[0]),
                filetype:event.target.files[0].type,
              },
            ]);
            
           //setSelectedFiles(event.target.files[0]);
            console.log("Files Data",event.target.files);
            console.log("Files Path",URL.createObjectURL(event.target.files[0]));
            console.log("Files convert", URL.revokeObjectURL(URL.createObjectURL(event.target.files[0])));
            console.log("SelectedFiles",selectedFiles);
          };
          // useEffect(()=>{
            
          //      console.log("date", userInput)
          // },[selectedFiles])

          useEffect(()=>{
            dispatch(fetchInsurerInfofiles(selectedFiles));
            dispatch(fetchInsurerInfofilesview(fileInfos));
            setUserInput(userInput => ({
              ...userInput, 
              ['InsurerFiles']: fileInfos
          }));
              console.log("date", fileInfos)
         },[fileInfos])

          useEffect(()=>{
          dispatch(fetchInsurerInfo(userInput));
          },[userInput]);

          useEffect(()=>{
            if (props.input) {
              setUserInput(props.input);
            }
            else{
              setUserInput(PolicyReducer.InsurerInfo);
            }
            if (props.fileInfos) {
              setFileInfos(props.fileInfos);
            }
            else{
              setFileInfos(PolicyReducer.InsurerInfofilesview);
            }
              
              //setSelectedFiles(PolicyReducer.InsurerInfofiles);

          },[props.input])

          useEffect(()=>{
            if (stepperList) {
              setUserInput(PolicyReducer.InsurerInfo);
              setFileInfos(PolicyReducer.InsurerInfofilesview);
              setSelectedFiles(PolicyReducer.InsurerInfofiles);
              console.log("useeffect",PolicyReducer.InsurerInfofiles);
            }

          },[stepperList])

         


          const cellRenderfile = (data) => {
            return <FileViewe Path={data.data.fileUrl} Name={data.data.Name}/>
          }

    return(
        <React.Fragment>
        <Grid container  spacing={3} >
        <Grid item md={12}><h3>Insurer's Quote Information</h3></Grid>
        </Grid>
        <Grid container  spacing={3} >
        <Grid item md={3}>
        <TextField id="insurer" name="Insurer" value={userInput.Insurer} label="Insurer" onChange={handleChange} variant="outlined" />
        </Grid>
        <Grid item md={3}>
        <TextField id="insurerStreetAddress" name="InsurerStreetAddress" value={userInput.InsurerStreetAddress} label="Insurer Street Address" onChange={handleChange} variant="outlined" />
        </Grid>
        <Grid item md={3}>
        <TextField id="insurerCity" name="InsurerCity" value={userInput.InsurerCity} label="Insurer City" onChange={handleChange} variant="outlined" />
        </Grid>
        <Grid item md={3}>
        <TextField id="insurerPostalCode" name="InsurerPostalCode" value={userInput.InsurerPostalCode} label="Insurer Postal Code" onChange={handleChange} variant="outlined" />
        </Grid>
        <Grid item md={3}>
        <TextField id="underwriter" name="underwriter" value={userInput.underwriter} label="Underwriter" onChange={handleChange} variant="outlined" />
        </Grid>
        <Grid item md={3}>
        <TextField id="underwriterEmail" name="UnderwriterEmail" value={userInput.UnderwriterEmail} label="Underwriter Email" onChange={handleChange} variant="outlined" />
        </Grid>
        <Grid item md={3}>
        <TextField id="insurerQuoteNumber" name="InsurerQuoteNumber" value={userInput.InsurerQuoteNumber} label="Insurer Quote Number" onChange={handleChange} variant="outlined" />
        </Grid>
        <Grid item md={3}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container justify="space-around" className="datePicker">
        <KeyboardDatePicker
          disableToolbar
          name="QuoteExpiryDate"
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="date-picker-inline"
          label="Date picker inline"
          value={QuoteExpiryDate}
          onChange={handleQuoteExpiryDate}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        </Grid>
        </MuiPickersUtilsProvider>        </Grid>
        <Grid item md={3}>
        <TextField id="TotalPremium" name="TotalPremium" value={userInput.TotalPremium} label="Total Quoted Premium" onChange={handleChange} variant="outlined" />
        </Grid>
        {/* <Grid item md={3}>
        <TextField id="TotalBoundPremium" name="TotalBoundPremium" value={userInput.TotalBoundPremium} label="Total Bound Premium" onChange={handleChange} variant="outlined" />
        </Grid> */}
        </Grid>
        <Grid container  spacing={3} >
            <Grid item md={12}>
            <h4 className="clrBlue" style={{'marginTop':'20px'}}>UPLOAD YOUR FILES</h4>
                <div className="fileUpload">
                <Button variant="contained" name="save" color="primary" type="button" ><CloudUploadIcon></CloudUploadIcon></Button>
                <input type="file" className="custom-file-upload" onChange={selectFile} />
                </div>
            
                </Grid>
            </Grid>
        <Grid container  spacing={3} >
            <Grid item md={12}> 
            <div className="card" style={{'width':'700px'}}>
        <div className="card-header">List of Files</div>
            <DataGrid
        id="gridContainer"
        dataSource={fileInfos}
        //keyExpr="ID"
        showBorders={true}
        >
        <Editing
          mode="row"
          useIcons={true}
          allowDeleting={true}
          allowUpdating={true}
          />
        <Column type="buttons" width={150}
          buttons={['delete']} />
        <Column dataField="Name" width={400} caption="Name" />
        <Column dataField="Id"  caption="Actions" width={200} cellRender={cellRenderfile} />
      </DataGrid>
</div> 

</Grid>
            </Grid>
           
        
        
        </React.Fragment>
    )
}
export default InsurerInfo