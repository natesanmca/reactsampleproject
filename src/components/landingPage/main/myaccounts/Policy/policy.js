import React,{useState, useEffect} from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import { useSelector, useDispatch } from "react-redux";
import { fetchSubmissionNumber, fetchCheckState, fetchSelectedPolicy, fetchTimeLine,fetchType, fetchQuotationNo, fetchInsurerInfo, fetchBindingInfo, 
  fetchInsurerInfofilesview, fetchBindingInfofilesview, fetchStatusType, fetchInforceInfo, fetchInforceInfofilesview } from '../../../../../Redux/Actions/actions';
import BindInfo from './bound';
import InsurerInfo from './insurerInfo';
import InforceInfo from './inforce';
const API_URL = process.env.REACT_APP_BASE_API_URL;

const Policy = (props) => {

    const [state, setState] = React.useState({});
    const [isFirstloaded, setisFirstloaded] = React.useState(true);
    const [policy, setPolicy] = React.useState([]);
    const [timeLine, setTimeline] = React.useState([]);
    const [value, setValue] = React.useState(0);
    const stepperList = useSelector((state) => state.TimeLine);
    const storePolicyList = useSelector((state) => state.SelectedPolicy);
    const submission = useSelector((state) => state.SubMission);
    const allstate = useSelector((state) => state);
    const [resData,setresdata] = useState({});
    const [fileInfos, setFileInfos] = React.useState([]);
    const [Insurerinput, setinput] = React.useState({});
    const dispatch = useDispatch();
     

    useEffect(()=> {
        // if (props.location.state.id) {
            
        // }
        // else  {

        // }

        console.log("State value",allstate);
        if (props.location.state) {
            // if (props.location.state.id !== "") {
            //     dispatch(fetchSubmissionNumber(props.location.state.id));
            //     console.log("coverage Dispatch",props.location.state.id);
            // }
                
            dispatch(fetchSubmissionNumber(props.location.state.id));
            


            console.log("policy Dispatch",props.location.state.id);
            console.log("policy state",props.location.state);

        }
        
        if (props.location.state.type === "submit" ) {
          console.log("submit");
            let policyIds = [];
            let timedata =[];
         let AcNo = props.location.state.id;
          const AcNo1 = {AcNo};
          console.log(AcNo);
          const statesUpdates = {};
          axios
            .post(`${API_URL}Quote/get-submission-Details`, AcNo1)
            .then((res) => {
              if (res.data.Success) {
                let respdata = res.data.Data;
                console.log("Policy Submission",respdata);
               
                if (respdata.policylist) {
                  
                setTimeline(respdata.policylist);
                dispatch(fetchStatusType(props.location.state.type));

              }
            }
            });
            
          }
          else if (props.location.state.type === "Quoted" ) {
            console.log("quotes");
            let policyIds = [];
            let timedata =[];
         let AcNo = props.location.state.id;
          const AcNo1 = {AcNo};
          console.log(AcNo);
          const statesUpdates = {};
          axios
            .post(`${API_URL}Policy/get-quotedsubmission-Details`, AcNo1)
            .then((res) => {
              if (res.data.Success) {
                let respdata = res.data.Data;
                console.log("Policy Quoted Submission",respdata);
                setresdata(respdata);
                console.log("respdata",resData);
                if (respdata.submissiondata.policylist) {
                  
                setTimeline(respdata.submissiondata.policylist);
                dispatch(fetchQuotationNo(props.location.state.id));
                dispatch(fetchInsurerInfo(respdata.submissiondata.InsurerInfoview));
                
                dispatch(fetchBindingInfo(respdata.submissiondata.BindingInfoview));
                dispatch(fetchInforceInfo(respdata.submissiondata.InforceInfoview));
                let Ifiles = respdata.files.InsurerFiles;
                Ifiles && Ifiles.map((file)=>{
                  let url = file.fileUrl;
                  file.fileUrl = API_URL + url;
                });

                console.log("get files",Ifiles);
                dispatch(fetchInsurerInfofilesview(Ifiles));
                setFileInfos(Ifiles);
                setinput(respdata.submissiondata.InsurerInfoview);

                let Bfiles = respdata.files.BindingFiles;
                Bfiles && Bfiles.map((file)=>{
                  let url = file.fileUrl;
                  file.fileUrl = API_URL + url;
                });

                console.log("get files",Ifiles);
                dispatch(fetchBindingInfofilesview(Bfiles));

                let Cfiles = respdata.files.InForceFiles;
                Cfiles && Cfiles.map((file)=>{
                  let url = file.fileUrl;
                  file.fileUrl = API_URL + url;
                });

                console.log("get files",Ifiles);
                dispatch(fetchInforceInfofilesview(Cfiles));
                
                dispatch(fetchType("InsurerQuote"));
                dispatch(fetchStatusType(props.location.state.type));

              }
            }
            });
            
          }
          else if (props.location.state.type === "Bound" ) {
            console.log("Bound")
            let policyIds = [];
            let timedata =[];
         let AcNo = props.location.state.id;
          const AcNo1 = {AcNo};
          console.log(AcNo);
          const statesUpdates = {};
          axios
            .post(`${API_URL}Policy/get-quotedsubmission-Details`, AcNo1)
            .then((res) => {
              if (res.data.Success) {
                let respdata = res.data.Data;
                console.log("Policy Quoted Submission",respdata);
                setresdata(respdata);
                console.log("respdata",resData);
                if (respdata.submissiondata.policylist) {
                  
                setTimeline(respdata.submissiondata.policylist);
                dispatch(fetchQuotationNo(props.location.state.id));
                dispatch(fetchInsurerInfo(respdata.submissiondata.InsurerInfoview));
                dispatch(fetchBindingInfo(respdata.submissiondata.BindingInfoview));
                dispatch(fetchInforceInfo(respdata.submissiondata.InforceInfoview));
                let Ifiles = respdata.files.InsurerFiles;
                Ifiles && Ifiles.map((file)=>{
                  let url = file.fileUrl;
                  file.fileUrl = API_URL + url;
                });

                //console.log("get files",Ifiles);
                dispatch(fetchInsurerInfofilesview(Ifiles));
                setFileInfos(Ifiles);
                setinput(respdata.submissiondata.InsurerInfoview);

                let Bfiles = respdata.files.BindingFiles;
                Bfiles && Bfiles.map((file)=>{
                  let url = file.fileUrl;
                  file.fileUrl = API_URL + url;
                });

             //   console.log("get files",Ifiles);
                dispatch(fetchBindingInfofilesview(Bfiles));

                let Cfiles = respdata.files.InForceFiles;
                Cfiles && Cfiles.map((file)=>{
                  let url = file.fileUrl;
                  file.fileUrl = API_URL + url;
                });

                //console.log("get files",Ifiles);
                dispatch(fetchInforceInfofilesview(Cfiles));
                
                dispatch(fetchType("InsurerQuote"));
                dispatch(fetchStatusType(props.location.state.type));

              }
            }
            });
            
          }
          else if (props.location.state.type === "InForce" ) {
            let policyIds = [];
            let timedata =[];
         let AcNo = props.location.state.id;
          const AcNo1 = {AcNo};
          console.log(AcNo);
          const statesUpdates = {};
          axios
            .post(`${API_URL}Policy/get-quotedsubmission-Details`, AcNo1)
            .then((res) => {
              if (res.data.Success) {
                let respdata = res.data.Data;
                console.log("Policy Quoted Submission",respdata);
                setresdata(respdata);
                console.log("respdata",resData);
                if (respdata.submissiondata.policylist) {
                  
                setTimeline(respdata.submissiondata.policylist);
                dispatch(fetchQuotationNo(props.location.state.id));
                dispatch(fetchInsurerInfo(respdata.submissiondata.InsurerInfoview));
                dispatch(fetchBindingInfo(respdata.submissiondata.BindingInfoview));
                dispatch(fetchInforceInfo(respdata.submissiondata.InforceInfoview));
                let Ifiles = respdata.files.InsurerFiles;
                Ifiles && Ifiles.map((file)=>{
                  let url = file.fileUrl;
                  file.fileUrl = API_URL + url;
                });

                console.log("get files",Ifiles);
                dispatch(fetchInsurerInfofilesview(Ifiles));
                setFileInfos(Ifiles);
                setinput(respdata.submissiondata.InsurerInfoview);

                let Bfiles = respdata.files.BindingFiles;
                Bfiles && Bfiles.map((file)=>{
                  let url = file.fileUrl;
                  file.fileUrl = API_URL + url;
                });

                console.log("get files",Ifiles);
                dispatch(fetchBindingInfofilesview(Bfiles));

                let Cfiles = respdata.files.InForceFiles;
                Cfiles && Cfiles.map((file)=>{
                  let url = file.fileUrl;
                  file.fileUrl = API_URL + url;
                });

                console.log("get files",Ifiles);
                dispatch(fetchInforceInfofilesview(Cfiles));
                
                dispatch(fetchType("InsurerQuote"));
                dispatch(fetchStatusType(props.location.state.type));

              }
            }
            });
            
          }
      }, [props.location]);

      useEffect(() => {
   

        // dispatch(fetchCheckState(state));
         dispatch(fetchTimeLine(timeLine));
         dispatch(fetchSelectedPolicy(timeLine));
           
         console.log("TimeLine",timeLine);
        // console.log("state dispatch",state);
       
    }, [timeLine]);


    const renderFirstAccordian = () => {
             
      if (props.location.state.type === "submit") {
        return (
           <InsurerInfo input={Insurerinput} fileInfos={fileInfos}></InsurerInfo>
        );
      }
      else if(props.location.state.type === "Quoted"){
        return (
        <BindInfo></BindInfo> 
        );
      }
      else{
        return (
        <InforceInfo></InforceInfo> 
        );
      }
    };

    return(
        <div className="coverage">
        <Grid container spacing={3}>
    <Grid item md={12}>
    {/* <InsurerInfo input={Insurerinput} fileInfos={fileInfos}></InsurerInfo> */}
    {renderFirstAccordian()}
        </Grid>

        </Grid>
    </div>
    )
}
export default Policy