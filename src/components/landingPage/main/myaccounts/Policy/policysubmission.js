import React,{useState, useEffect} from "react";
import Container from '@material-ui/core/Container';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';

import CoverageDataDynamic from "../coveragedynamic";
import WrapUp from "../DynamicTab/wrapup";
import CyberCoverage from "../DynamicTab/cyber";
import CocCoverage from "../DynamicTab/coc";
import Property from "../DynamicTab/property";
import EBIndex from "../DynamicTab/EBPolicy/ebindex";
import PollutionIndex from "../DynamicTab/Pollution/pollutionindex";
import PolicyStepper from "./policystepper";
import InsurerInfo from "./insurerInfo";
import {useSelector} from 'react-redux'
import Policy from "./policy";
import BindInfo from "./bound";
import AGLCoverage from "../DynamicTab/agl";
import AutoCoverage from "../DynamicTab/Auto/autocoveragetab";
import InforceInfo from "./inforce";

//import Property from "./DynamicTab/property";
const PolicySubmission=()=>{
    const PolicyReducer = useSelector(state => state.PolicyQuote);
    useEffect(()=>{
console.log("Policy Submission",PolicyReducer);
    },[])
    return(<div className="policy_coverage">
         <Grid container>
        
        <Grid container spacing={3}>
       
            
            <Grid item md={12} > 
            <PolicyStepper></PolicyStepper></Grid>
            </Grid>
            </Grid>
       
        <Switch>
        <Route path="/myaccounts/accountDetails/policyQuote" render={({match: {url}}) => (
            <><Route path={`${url}/InsurerInfo`} component={InsurerInfo}></Route>
            <Route path={`${url}/BindingInfo`} component={BindInfo}></Route>
            <Route path={`${url}/InforceInfo`} component={InforceInfo}></Route>
            <Route path={`${url}/LandingPolicy`} component={Policy}></Route>
              {/* <Route path={`${url}/CoverageData`} component={CoverageData}></Route> */}
              <Route path={`${url}/CoverageData`} component={CoverageDataDynamic}></Route>
              <Route path={`${url}/wrapup`} component={WrapUp}></Route>
              <Route path={`${url}/cyber`} component={CyberCoverage}></Route>
              <Route path={`${url}/coc`} component={CocCoverage}></Route>
              <Route path={`${url}/property`} component={Property}></Route>
              <Route path={`${url}/BreakDown`} component={EBIndex}></Route>
              <Route path={`${url}/Pollution`} component={PollutionIndex}></Route>
              <Route path={`${url}/AGL`} component={AGLCoverage}></Route>
              <Route path={`${url}/AutoFleet`} component={AutoCoverage}></Route>
              {/* <Route path={`${url}/`} component={AccountDetails}></Route> */}
              </>
          )}>
           
          </Route>
        </Switch>
    </div>)
}


export default PolicySubmission