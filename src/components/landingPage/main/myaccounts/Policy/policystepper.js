import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import {Link} from "react-router-dom";
import {useSelector} from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

function TabPanel(props) {
  const { children, value, index, ...other } = props;


  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  stepper:{}
}));

export default function PolicyStepper(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const stepperList = useSelector(state => state.TimeLine);
  const submission = useSelector((state) => state.SubMission);
  const PolicyReducer = useSelector(state => state.PolicyQuote);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    
    <Grid container className="coverageMain">
    <div className={classes.root, classes.stepper}>
      <AppBar position="static" color="default" className="stepper">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
          className="stepper"
        >

         {/* {(PolicyReducer.StatusType === "Quoted"|| PolicyReducer.StatusType === "submit" || PolicyReducer.StatusType === "Bound" || PolicyReducer.StatusType === "InForce") &&  <Tab  to={{pathname:`/myaccounts/accountDetails/policyQuote/InsurerInfo`}}   {...a11yProps(-1)}
        component={Link} label="Insurer's Quote Information" /> } */}
         { (PolicyReducer.StatusType === "Bound" || PolicyReducer.StatusType === "InForce") &&  <Tab  to={{pathname:`/myaccounts/accountDetails/policyQuote/InforceInfo`}}   {...a11yProps(-1)}
        component={Link} label="InForce Details" />}

       { PolicyReducer.StatusType !== "submit" &&  <Tab  to={{pathname:`/myaccounts/accountDetails/policyQuote/BindingInfo`}}   {...a11yProps(-1)}
        component={Link} label="Binding Details" />}

        <Tab  to={{pathname:`/myaccounts/accountDetails/policyQuote/InsurerInfo`}}   {...a11yProps(-1)}
        component={Link} label="Insurer's Quote Information" />



         {/* { PolicyReducer.StatusType === "Bound" &&  <Tab  to={{pathname:`/myaccounts/accountDetails/policyQuote/BindingInfo`}}   {...a11yProps(-1)}
        component={Link} label="Binding Details" />}

{PolicyReducer.StatusType === "InForce" &&  <Tab  to={{pathname:`/myaccounts/accountDetails/policyQuote/BindingInfo`}}   {...a11yProps(-1)}
        component={Link} label="Binding Details" />} */}

          {stepperList.timeline && stepperList.timeline.map((timeLineData, index)=>{
      let coverageDetails="";
      let rootpath = timeLineData.policyTypeId == "62b702b9-5212-461e-a8e3-d0b8f8a7e7af" ? '/myaccounts/accountDetails/policyQuote/CoverageData' :
      timeLineData.policyTypeId == "ecbaea95-f8c8-429e-bc54-2077f58e30cd" ? `/myaccounts/accountDetails/policyQuote/wrapup` : 
      timeLineData.policyTypeId == "bee64e56-4944-4de0-8245-6dd3ecc212c1" ? `/myaccounts/accountDetails/policyQuote/cyber` : 
      timeLineData.policyTypeId == "f4b4cb62-86a3-48b2-b03f-3264d05952d3" ? `/myaccounts/accountDetails/policyQuote/coc` :
      timeLineData.policyTypeId == "9e6f5f4e-b3b0-498b-8556-9b151281ad40" ? `/myaccounts/accountDetails/policyQuote/property` : 
      timeLineData.policyTypeId == "2d45db49-7697-43d0-a810-c8057b9ab042" ? `/myaccounts/accountDetails/policyQuote/BreakDown` : 
      timeLineData.policyTypeId == "fda1fbdc-88e7-4a5b-b0e8-da65b20ec56e" ? `/myaccounts/accountDetails/policyQuote/Pollution` : 
      timeLineData.policyTypeId == "e4e98be6-0292-4741-8ed1-ca73f965bd04" ? `/myaccounts/accountDetails/policyQuote/AGL` : 
      timeLineData.policyTypeId == "04871333-72f9-48e1-bdd4-b18c05b9f6ea" ? `/myaccounts/accountDetails/policyQuote/AutoFleet` : `/myaccounts/accountDetails/policyQuote/CoverageData`;
      // { timeLineData.policyTypeId == "" ? rootpath = ""}
      timeLineData.coverages.map((coverage, cIndex) => {
        coverageDetails = `${coverageDetails} ${coverageDetails==="" ? "" : ","} ${coverage.coverageName}`
      })
      console.log("rootpath",rootpath);
      return(
       <Tab  to={{pathname: rootpath, state:{id:timeLineData,index:index,total:stepperList.timeline.length}}}  key={index} {...a11yProps({index})}
        component={Link} label={`${timeLineData.policyType} & ${coverageDetails}`} /> 
        
      

        
    )  
      
    })}
        </Tabs>
      </AppBar>
    
    </div>
        <Grid container spacing={3}>

        </Grid>
        </Grid>

   
    
  );
}
