import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import Favorite from "@material-ui/icons/Favorite";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import DeleteIcon from "@material-ui/icons/Delete";

import {
  fetchTimeLine,
  fetchSelectedPolicy,
  fetchCheckState,
} from "../../../../../../Redux/Actions/actions";
import Equipmentbreakdown from "./equipmentbreakdown";

const API_URL = process.env.REACT_APP_BASE_API_URL;
const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    "&$checked": {
      color: green[600],
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

const EBLocation = (props) => {
  const [state, setState] = React.useState({});
  const [Location, setLocation] = React.useState();
  const [checkedBoxValues, setCheckedValues] = React.useState([]);
  const Account = useSelector((state) => state.Account);
  const List = useSelector((state) => state.TimeLine);
  const[timeline,settimeline] = React.useState(List.timeline);
  const submission = useSelector((state) => state.SubMission);
  const dispatch = useDispatch();
  const DataLoad = () => {
    const urlstr =
      API_URL + 'Building/get-all-buildingInfos/' +
      Account.AcNo;
    console.log(urlstr);
    axios.get(urlstr).then((res) => {
      if (res.data.Success) {
        let itemLists = res.data.Data;
        //let list = JSON.stringify(classDetails)
        //let listItem = JSON.parse(list)
        setLocation(itemLists.ActiveLocation);

          console.log("location", itemLists.ActiveLocation);

          if (submission.SubmissionNo != "" && submission.SubmissionNo != "0")
          {
            const statesUpdates = {};
          let tempTitleList = [...timeline];
            
          let isPolicyTypeAvailable = tempTitleList.find(
            (tempTitle) => tempTitle.policyTypeId === props.coverages.policyTypeId
          );
          tempTitleList.map((tempTitle) => {
            if (
              tempTitle.policyTypeId === props.coverages.policyTypeId &&
              isPolicyTypeAvailable
            ) {
      
              tempTitle.coverages.map((coverage, coverageIndex) => {
      
                  coverage.coveragedata.map((dat,i)=> {
                    if (dat.locationId !=="") {
                      statesUpdates[dat.locationId] = true;
                      //setState({ ...state, [dat.locationId]: true });
                      const newNames = checkedBoxValues?.includes(dat.locationId)
                        ? checkedBoxValues?.filter(name => name !== dat.locationId)
                        : [...(checkedBoxValues ?? []), dat.locationId];
                      setCheckedValues(newNames);
                    }
                    
                  });
                
              });
      
            }
      
          });

          setState(Object.assign({}, state, statesUpdates));
          console.log("DB Check State location",statesUpdates);
          console.log("After DB Check State location",state);
        }


      }
    });
  };

  const handleChange = (event,checkedName) => {
    const locid = event.target.name;
     let tempTitleList = [...timeline];
   let data =  tempTitleList.find((tempTitle) => tempTitle.policyTypeId === props.coverages.policyTypeId);
    let isPolicyTypeAvailable = tempTitleList.find(
       (tempTitle) => tempTitle.policyTypeId === props.coverages.policyTypeId
       //"9e6f5f4e-b3b0-498b-8556-9b151281ad40"
    );
    console.log("Policy Data",data);
    if (event.target.checked) {
      data.coverages.map((coverage, index) => { 

        if (coverage.coverageId === "7fb24475-cc3d-4999-af11-ade7f2cf022a")
        {
          const ID = coverage.coverageId;
          const subVal = {ID};

          console.log(subVal);
          //return itemLists;
          let itemLists =[];
          axios.post(`${API_URL}Quote/get-coverageItem`, subVal)
          .then(res => {
          if(res.data.Success){ 
                itemLists = res.data.Data;
              //let list = JSON.stringify(classDetails)
              //let listItem = JSON.parse(list)
              if (coverage.coveragedata.length === 0) {
                coverage.coveragedata = [ 
                  {
                    locationId: locid,
                    CoverageItems: res.data.Data[0].CoverageItems,
                    CoverageOtherItem: res.data.Data[0].CoverageOtherItem,
                  },
                ];
              }
              else {
                coverage.coveragedata = [ 
                  ...coverage.coveragedata,
                  {
                    locationId: locid,
                    CoverageItems: res.data.Data[0].CoverageItems,
                    CoverageOtherItem: res.data.Data[0].CoverageOtherItem,
                  },
                ];
              }
              tempTitleList.map((tempTitle,ind) => {
                if (
                  tempTitle.policyTypeId === props.coverages.policyTypeId &&
                  isPolicyTypeAvailable
                ) {
                  tempTitleList[ind] = data;
                }
          
              });
              settimeline(tempTitleList);
              console.log("LocationList_Timeline",timeline);
              console.log("LocationList_tempTitleList",tempTitleList);
              setState({ ...state, [locid]: true });
          }
          });
  
        }
  
      });
      
    }
    else
    {
      setState({ ...state, [locid]: false });
      data.coverages.map((coverage, index) => { 

        if ( coverage.coverageId === "7fb24475-cc3d-4999-af11-ade7f2cf022a")
        {
          coverage.coveragedata.map((data,i) => {
              if (data.locationId === locid) {
                coverage.coveragedata.splice(i, 1);
              }
          });

        }
      });
      tempTitleList.map((tempTitle,ind) => {
        if (
          tempTitle.policyTypeId === props.coverages.policyTypeId &&
          isPolicyTypeAvailable
        ) {
          tempTitleList[ind] = data;
        }
  
      });
      settimeline(tempTitleList);
      console.log("LocationList_Timeline",timeline);
      console.log("LocationList_tempTitleList",tempTitleList);
      

    }
   
    const newNames = checkedBoxValues?.includes(checkedName)
    ? checkedBoxValues?.filter(name => name !== checkedName)
    : [...(checkedBoxValues ?? []), checkedName];
    setCheckedValues(newNames);
    return newNames;

  };

  useEffect(() => {
    DataLoad();

    
  }, []);
  // useEffect(()=>{
  //   if (submission.SubmissionNo != "" && submission.SubmissionNo != "0")
  //   {
  //   let tempTitleList = [...timeline];
      
  //   let isPolicyTypeAvailable = tempTitleList.find(
  //     (tempTitle) => tempTitle.policyTypeId === props.coverages.policyTypeId
  //   );
  //   tempTitleList.map((tempTitle) => {
  //     if (
  //       tempTitle.policyTypeId === props.coverages.policyTypeId &&
  //       isPolicyTypeAvailable
  //     ) {

  //       tempTitle.coverages.map((coverage, coverageIndex) => {

  //           coverage.coveragedata.map((dat,i)=> {
  //             if (dat.locationId !=="") {
  //               setState({ ...state, [dat.locationId]: true });
  //             }
              
  //           });
          
  //       });

  //     }

  //   });
  // }

  // },[Location])
  useEffect(() => {
   

    dispatch(fetchTimeLine(timeline));


    console.log("TimeLine dynamic",timeline);
 
  
}, [timeline]);

  const renderPropertyContent = (coverage,locid) =>{
    console.log("coverage",coverage);
    console.log("locid",locid.toString());
console.log("coveragedata",coverage.coveragedata);
    let coverdata;
    coverage.coveragedata.map((data)=> {
      console.log("datalocationId",data.locationId);
        if (data.locationId === locid) {
          coverdata = data;
        }
    });
    console.log("coverdata",coverdata);
    switch (coverage.coverageId) {
           case "7fb24475-cc3d-4999-af11-ade7f2cf022a":
            return  <Equipmentbreakdown ItemData = {coverdata} coverageId = {coverage.coverageId} policyTypeId = {props.coverages.policyTypeId} LocationId = {locid} location={Location}></Equipmentbreakdown>
            break;
       default:
         break;
     }
  
 }
  const renderCoverage = (locid) => {
    let tempTitleList = [...timeline];
    let data =  tempTitleList.find((tempTitle) => tempTitle.policyTypeId === props.coverages.policyTypeId);
    return data.coverages.map((coverage, index) => {
      if (
        coverage.coverageId === "3b964469-136a-4c5f-8c46-e53eb0d4e3ac" ||
        coverage.coverageId === "c51de115-1cbe-4b58-bf9e-7fd35b98a372" ||
        coverage.coverageId === "7fb24475-cc3d-4999-af11-ade7f2cf022a"
      ) {
        return (
          <Accordion className="accordian" key={index}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>{coverage.coverageName}</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>{renderPropertyContent(coverage,locid)}</Typography>
            </AccordionDetails>
          </Accordion>
        );
      }
    });
  };
  const renderLocation = () => {
    let checkedValues = [];
    for (const key in state) {
      if (state.hasOwnProperty(key)) {
        const value = state[key];
        if (value) {
          const currentLocation = Location.find((loc) => loc.Id === key);
          const locationName =
            currentLocation.Street +
            "," +
            currentLocation.BuildingType +
            "," +
            currentLocation.City;
          checkedValues.push({ LocationName: locationName,LocationId: currentLocation.Id });
        }
      }
    }
    console.log(checkedValues);
    return (
      checkedValues &&
      checkedValues.map((loc, index) => {
        return (
          <Accordion className="accordian" key={index}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>
                Location-{index + 1} :<span>{loc.LocationName}</span>
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>{renderCoverage(loc.LocationId)}</Typography>
            </AccordionDetails>
          </Accordion>
        );
      })
    );
  };
  return (
    <FormGroup row >
      <FormGroup row className="coverageMain propertyCheck" >
      {Location &&
        Location.map((locationlist, index) => {
          return (
            
            <FormControlLabel 
              control={
                <Checkbox
                checked={checkedBoxValues.includes(locationlist.Id)}
                key={index}
                name={locationlist.Id}
                //onChange={handleChange}
                onChange={e => handleChange(e,locationlist.Id)}
              />
              }
              label={
                locationlist.Street +
                "," +
                locationlist.BuildingType +
                "," +
                locationlist.City
              }
            />
          );
        })}
      </FormGroup>
      {renderLocation()}
    </FormGroup>
  );
};
    
export default EBLocation;
