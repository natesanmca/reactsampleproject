import React,{ useReducer,useState,useEffect} from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import { useSelector, useDispatch } from "react-redux";
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import RemoveRoundedIcon from '@material-ui/icons/RemoveRounded';
import Button from "@material-ui/core/Button";
import NumberFormat from 'react-number-format';
import { v4 as uuidv4 } from 'uuid';
import {
  fetchTimeLine,
  fetchSelectedPolicy,
  fetchCheckState,
} from "../../../../../../Redux/Actions/actions";


const Equipmentbreakdown =(props)=>{
    const [itemList, setitemList] = useState([]);
    const List = useSelector((state) => state.TimeLine);
  const[timeline,settimeline] = React.useState(List.timeline);
    const [Default, setDefault] = useState([{ LineItem: "", Limit: "", Deductible:"" }]);
    const dispatch = useDispatch();
    const submission = useSelector((state) => state.SubMission);
    const [userInput, setUserInput] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {
       
        BuildingLimit1: '',
        BuildingLimit2:''
        }
        );
        const limitAmount = ([
          {Limit:"$ 1,000,000"},{Limit:"$ 2,000,000"},{Limit:"$ 3,000,000"},
          {Limit:"$ 4,000,000"},{Limit:"$ 5,000,000"},{Limit:"$ 6,000,000"},
          {Limit:"$ 7,000,000"},{Limit:"$ 8,000,000"},{Limit:"$ 9,000,000"},
          {Limit:"$ 10,000,000"}
            ]);
            const DeAmount = ([
                {Amount:"0"},{Amount:"$ 500"},{Amount:"$ 1,000"},{Amount:"$ 2,500"},
            {Amount:"$ 5,000"},{Amount:"$ 10,000"},{Amount:"$ 15,000"}
            ,{Amount:"$ 20,000"},{Amount:"$ 30,000"},{Amount:"$ 40,000"},
            {Amount:"$ 50,000"}
              ]);
              const Months = ([
                {Month:"12 Months"},{Month:"18 Months"},{Month:"24 Months"}
                ]);
        const handleChange = evt => {
          const name = evt.target.name;
          let  newValue = evt.target.value;
          setUserInput({[name]: newValue});
        }


        const handleInputChange = (e, index) => {
            const { name, value } = e.target;
            const list = [...itemList];
            let rval = value;

    if(e.target.className == 'numberft'){
      rval=value.replace(/\D/g,'');
    }
    list[index][name] = rval;
           // list[index][name] = value;
            setitemList(list);
          };
          const handleInputChangeSub = (event, index,Id) => {
          
            console.log('OperationChages');
            let tempTitleList = [...itemList];
            let isitemAvailable = tempTitleList.find(
                (tempTitle) => tempTitle.ItemId === Id
              );
            //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
              tempTitleList.map((tempTitle) => {
                if (tempTitle.ItemId === Id && isitemAvailable) {
                    
                  tempTitle.ChildItems[index][event.target.name] = event.target.value;
                  setitemList(tempTitleList);
                  console.log(tempTitleList);
                }
            });
          };

          const handleInputChangeother = (e, index) => {
            const { name, value } = e.target;
            const list = [...Default];
            list[index][name] = value;
            setDefault(list);
            let tempTitleList = [...timeline];
        
            let isPolicyTypeAvailable = tempTitleList.find(
              (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
            );
            tempTitleList.map((tempTitle) => {
              if (
                tempTitle.policyTypeId === props.policyTypeId &&
                isPolicyTypeAvailable
              ) {
        
                tempTitle.coverages.map((coverage, coverageIndex) => {
                  if (coverage.coverageId == props.coverages.coverageId) {
                    tempTitle.coverages[coverageIndex].coveragedata[0].CoverageOtherItem = Default;
                  }
                });
        
              }
        
            });
            settimeline(tempTitleList);
            //dispatch(fetchOwnername(Default))
          };

          const handleRemoveClick = index => {
            const list = [...Default];
            list.splice(index, 1);
            setDefault(list);
          };
        
          // handle click event of the Add button
          const handleAddClick = () => {
            setDefault([...Default, { Id:uuidv4(), LineItem: "", Limit: "", Deductible:"", LineItem2:"",GroupName: "" }]);      
          };
          useEffect(()=>{
            console.log("Props building",props);
              if (props.ItemData) {
                setitemList(props.ItemData.CoverageItems);
              }
          },[props.ItemData]);
          useEffect(()=>{
            let tempTitleList = [...timeline];
          
            let isPolicyTypeAvailable = tempTitleList.find(
              (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
            );
            tempTitleList.map((tempTitle) => {
              if (
                tempTitle.policyTypeId === props.policyTypeId &&
                isPolicyTypeAvailable
              ) {
        
                tempTitle.coverages.map((coverage, coverageIndex) => {
                  if (coverage.coverageId == props.coverageId) {
                    coverage.coveragedata.map((dat,i)=> {
                        if (dat.locationId === props.LocationId) {
                          tempTitle.coverages[coverageIndex].coveragedata[i].CoverageItems = itemList;
                        }
                    });
                  }
                });
        
              }
        
            });
            settimeline(tempTitleList);
    
           },[itemList]);
           useEffect(() => {
   

            dispatch(fetchTimeLine(timeline));
        
        
            console.log("TimeLine dynamic",timeline);
         
          
        }, [timeline]);
          var tableStyle = {
            "background": "#2e3f50",
            "color": "#fff",
            "text-transform": "capitalize",
         };
         var BorderStyle = {
            "border-right":"1px solid #333"
          };

    return(
        <React.Fragment>
            <div className="table buildingTable">
       
      <table>
        <thead>

                <th style={{width: '35%'}}></th>
                <th style={{width: '25%'}}>COVERAGE INFO</th>
                <th style={{width: '20%'}}>LIMIT</th>
                <th style={{width: '20%'}}> DEDUCTIBLE</th>
          
        </thead>
       <tbody>
     <React.Fragment>
    {itemList && itemList.map((x, i) => {
        if (x.GroupName === "") {
            
        
        return (
            
               <tr>
                  <td>
                    {x.LineInputType == 1 && <label> {x.LineItem} </label>}
                                       
                    </td>
                    <td></td>
                    <td style={BorderStyle}>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    
                    // <TextField  name="Limit" value={x.Limit}
                    // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    
                    }
                      {x.IsLimit && x.LimitInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="Limit"  
                        value={x.Limit}
                        onChange={e => handleInputChange(e, i)}
                        label="Limit"
                        >

                        { limitAmount && limitAmount.map((item, index)=>{
                          let rval = item.Amount; 
                          let val1;                       
                          val1=rval.replace(/\D/g,'');
                        return(
                        <MenuItem key={index} value={val1}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                     
                    
                        
                    {/* <TextField  name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                    <td>
                     {x.IsDeductible && x.DeductibleInputType == 2 &&
                     
                    //  <TextField  name="Deductible" value={x.Deductible}
                    // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    
                    }
                      {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlinedDeductible"
                        name="Deductible"  
                        value={x.Deductible}
                        onChange={e => handleInputChange(e, i)}
                        label="Deductible"
                        >

                        { DeAmount && DeAmount.map((item, index)=>{
                          let rval = item.Amount; 
                          let val1;                       
                          val1=rval.replace(/\D/g,'');
                        return(
                        <MenuItem key={index} value={val1}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                      
                      </tr>

                        );
                    }
     

      })}
      <tr>
  <td style={tableStyle}>Business Interruption</td>
  <td></td>
  <td style={BorderStyle}></td>
  <td></td>

</tr>

{itemList && itemList.map((x, i) => {
       if(x.Limit == 0){
        x.Limit = '';
      }
      if(x.Deductible == 0){
        x.Deductible = '';
      }
       if (x.GroupName === "Business Interruption") {
            
        
                        return (
                            
                               <tr>
                                  <td>
                                    {x.LineInputType == 1 && <label> {x.LineItem} </label>}
                                                       
                                    </td>
                                    <td>
                                    <div className="radioField">
                    {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                        
                        if (ch.LineInputType === 2) {
                          return(
                       
                            <TextField  name="ValData" value={ch.ValData}
                             onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="Revenue" variant="outlined" /> );
                          }
                        else if (ch.LineInputType === 5) {
                        return(
                     
                          <div className="field2"> <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> Months </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id="demo-simple-select-outlined"
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label=""
                   >

                   { Months && Months.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.Month}>{item.Month}</MenuItem>)

                   })} 

                   </Select>
                </FormControl> </div> );
                        }
                    })}
                    </div>
                                    </td>
                                    <td style={BorderStyle}>
                                    {x.IsLimit && x.LimitInputType == 2 &&
                                    
                                    // <TextField  name="Limit" value={x.Limit}
                                    // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

                                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined"  thousandSeparator={true} prefix={'$'}/>
                                    
                                    }
                                      {x.IsLimit && x.LimitInputType == 5 && <FormControl variant="outlined" className="select">
                                        <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
                                        <Select
                                        labelId="demo-simple-select-outlined-label"
                                        id="demo-simple-select-outlined"
                                        name="Limit"  
                                        value={x.Limit}
                                        onChange={e => handleInputChange(e, i)}
                                        label="Limit"
                                        >
                
                                        { limitAmount && limitAmount.map((item, index)=>{
                                          let rval = item.Amount; 
                                          let val1;                       
                                          val1=rval.replace(/\D/g,'');
                                        return(
                                        <MenuItem key={index} value={val1}>{item.Amount}</MenuItem>)
                
                                        })} 
                
                                        </Select>
                                      </FormControl> }
                                      </td>
                                     
                                    
                                        
                                    {/* <TextField  name="Deductible" value={x.Deductible} 
                                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                                    <td>
                                     {x.IsDeductible && x.DeductibleInputType == 2 &&
                                     
                                    //  <TextField  name="Deductible" value={x.Deductible}
                                    // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
                                    
                                    }
                                      {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
                                        <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
                                        <Select
                                        labelId="demo-simple-select-outlined-label"
                                        id="demo-simple-select-outlinedDeductible"
                                        name="Deductible"  
                                        value={x.Deductible}
                                        onChange={e => handleInputChange(e, i)}
                                        label="Deductible"
                                        >
                
                                        { DeAmount && DeAmount.map((item, index)=>{
                                          let rval = item.Amount; 
                                          let val1;                       
                                          val1=rval.replace(/\D/g,'');
                                        return(
                                        <MenuItem key={index} value={val1}>{item.Amount}</MenuItem>)
                
                                        })} 
                
                                        </Select>
                                      </FormControl> }
                                      </td>
                                      
                                      </tr>
                
                                        );
                                    }

      })}
      </React.Fragment>
             
        </tbody>
      </table>
            </div>
        </React.Fragment>
    )
}

export default Equipmentbreakdown