import React,{ useReducer,useState,useEffect} from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import { useSelector, useDispatch } from "react-redux";
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import RemoveRoundedIcon from '@material-ui/icons/RemoveRounded';
import Button from "@material-ui/core/Button";
import NumberFormat from 'react-number-format';
import { v4 as uuidv4 } from 'uuid';
import {
  fetchTimeLine,
  fetchSelectedPolicy,
  fetchCheckState,
} from "../../../../../../Redux/Actions/actions";


const Crime =(props)=>{
    const [itemList, setitemList] = useState([]);
    const [] = useState([]);
    const [] = useState([]);
    const List = useSelector((state) => state.TimeLine);
  const[timeline,settimeline] = React.useState(List.timeline);
    const [Default, setDefault] = useState([{ LineItem: "", Limit: "", Deductible:"" }]);
    const dispatch = useDispatch();
    const [userInput, setUserInput] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {
       
        BuildingLimit1: '',
        BuildingLimit2:''
        }
        );
        const limitAmount = ([
            {Amount:"1000000"},{Amount:"2000000"},{Amount:"3000000"},
          {Amount:"4000000"},{Amount:"5000000"},{Amount:"6000000"}
          ,{Amount:"7000000"},{Amount:"8000000"},{Amount:"9000000"},
          {Amount:"10000000"}
            ]);
            const DeAmount = ([
              {Amount:"500"},{Amount:"1000"},{Amount:"2500"},
            {Amount:"5000"},{Amount:"10000"},{Amount:"15000"}
            ,{Amount:"20000"},{Amount:"30000"},{Amount:"40000"},
            {Amount:"50000"}
              ]);
        const handleChange = evt => {
          const name = evt.target.name;
          let  newValue = evt.target.value;
          setUserInput({[name]: newValue});
        }


        const handleInputChange = (e, index) => {
           console.log("e",e.target.className);
            const { name, value } = e.target;
            console.log("Test",name);
            let rval = value;

            if(e.target.className == 'numberft'){
              rval=value.replace(/\D/g,'');
            }
            console.log("e",rval);
            //const val = value.replace(/\D/g,'');
            //const str = value;
          //console.log(value.replace(/\D/g,''));    
            const list = [...itemList];
            list[index][name] = rval;
            setitemList(list);
            //dispatch(fetchOwnername(operationList))
            let tempTitleList = [...timeline];
        
            let isPolicyTypeAvailable = tempTitleList.find(
              (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
            );
            tempTitleList.map((tempTitle) => {
              if (
                tempTitle.policyTypeId === props.policyTypeId &&
                isPolicyTypeAvailable
              ) {
        
                tempTitle.coverages.map((coverage, coverageIndex) => {
                  if (coverage.coverageId == props.coverages.coverageId) {
                    tempTitle.coverages[coverageIndex].coveragedata[0].CoverageItems = itemList;
                  }
                });
        
              }
        
            });
            settimeline(tempTitleList);
          };
        
          const handleInputChangeSub = (event, index,Id) => {
          
            console.log('OperationChages');
            let tempTitleList = [...itemList];
            let isitemAvailable = tempTitleList.find(
                (tempTitle) => tempTitle.ItemId === Id
              );
            //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
              tempTitleList.map((tempTitle) => {
                if (tempTitle.ItemId === Id && isitemAvailable) {
                    
                  tempTitle.ChildItems[index][event.target.name] = event.target.value;
                  setitemList(tempTitleList);
                  console.log(tempTitleList);
                }
            });
          };

          const handleInputChangeother = (e, index) => {
            const { name, value } = e.target;
            const list = [...Default];
            let rval = value;

            if(e.target.className == 'numberft'){
              rval=value.replace(/\D/g,'');
            }
            list[index][name] = rval;
            setDefault(list);
            let tempTitleList = [...timeline];
        
            let isPolicyTypeAvailable = tempTitleList.find(
              (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
            );
            tempTitleList.map((tempTitle) => {
              if (
                tempTitle.policyTypeId === props.policyTypeId &&
                isPolicyTypeAvailable
              ) {
        
                tempTitle.coverages.map((coverage, coverageIndex) => {
                  if (coverage.coverageId == props.coverages.coverageId) {
                    tempTitle.coverages[coverageIndex].coveragedata[0].CoverageOtherItem = Default;
                  }
                });
        
              }
        
            });
            settimeline(tempTitleList);
            //dispatch(fetchOwnername(Default))
          };

          const handleRemoveClick = index => {
            const list = [...Default];
            list.splice(index, 1);
            setDefault(list);
          };
        
          // handle click event of the Add button
          const handleAddClick = () => {
            setDefault([...Default, { Id:uuidv4(), LineItem: "", Limit: "", Deductible:"", LineItem2:"",GroupName: "" }]);      
          };
        useEffect(()=>{
          console.log("jhj",setDefault);
          console.log("jhj");
            console.log("Props building",props);
            // console.log("Coverages",props.location.state.id.coverages);
            // props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{
            //   console.log(data);
            if (props.coverages) {
              setitemList(props.coverages.coveragedata[0].CoverageItems);
              setDefault(props.coverages.coveragedata[0].CoverageOtherItem);
            }
            // //   data.coveragedata.CoverageItems && data.coveragedata.CoverageItems.map((item)=>{
            // //     if (item.ItemId === "d4edafea-5b80-483a-8d3e-9e3f59a3e07c") {
            // //         console.log("selectdata",item.ChildItems[0].ValData);
            // //         setselected(item.ChildItems[0].ValData);
            // //     }
        
            // // });
            // });
        
            
           // DataLoad();
          },[props.coverages]);

    return(
        <React.Fragment>
            <div className="table buildingTable">
       
      <table>
        <thead>

                <th style={{width: '40%'}}>Crime Coverage</th>

                <th style={{width: '25%'}}>LIMIT</th>
                <th style={{width: '25%'}}> DEDUCTIBLE</th>
                <th style={{width: '10%'}}></th>
          
        </thead>
       <tbody>
     <React.Fragment>
    {itemList && itemList.map((x, i) => {
      if(x.Limit == 0){
        x.Limit = '';
      }
      if(x.Deductible == 0){
        x.Deductible = '';
      }
        return (
            
               <tr>
                  <td>
                    {x.LineInputType == 1 && <label> {x.LineItem} </label>}
                                       
                    </td>
                    <td>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    
                    // <TextField  name="Limit" value={x.Limit}
                    // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
                    
                    <NumberFormat class="numberft" type="{" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$ '} />
                    }
                      {x.IsLimit && x.LimitInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="Limit"  
                        value={x.Limit}
                        onChange={e => handleInputChange(e, i)}
                        label="Limit"
                        >

                        { limitAmount && limitAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                     
                    
                        
                    {/* <TextField  name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                    <td>
                     {x.IsDeductible && x.DeductibleInputType == 2 &&
                     
                    //  <TextField  name="Deductible" value={x.Deductible}
                    // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />
                    
                    <NumberFormat class="numberft"  name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$ '} />
                    }
                      {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlinedDeductible"
                        name="Deductible"  
                        value={x.Deductible}
                        onChange={e => handleInputChange(e, i)}
                        label="Deductible"
                        >

                        { DeAmount && DeAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                      <td></td>
                      </tr>

                        );
    
         

      })}

{ Default && Default.map((dat, index) => {

return (
    
    <tr >
    <td>
            <TextField  name="LineItem" value={dat.LineItem}
            onChange={e => handleInputChangeother(e, index)} label="" variant="outlined" />
            </td>
            
            <td>
            {/* <TextField  name="Limit" value={dat.Limit}
            onChange={e => handleInputChangeother(e, index)} label="Limit" variant="outlined" /> */}

            <NumberFormat class="numberft" name="Limit" value={dat.Limit} 
            onChange={e => handleInputChangeother(e, index)} label="Limit" variant="outlined"  thousandSeparator={true} prefix={'$ '}  />
            </td>
            <td>
            {/* <TextField  name="Deductible" value={dat.Deductible}
            onChange={e => handleInputChangeother(e, index)} label="Deductible" variant="outlined" /> */}

            <NumberFormat class="numberft" name="Deductible" value={dat.Deductible} 
            onChange={e => handleInputChangeother(e, index)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$ '}/>
            </td>
            <td>
            <div className="btn-box" style={{position: "static"}}>
        {Default.length !== 1 && <Button  type="button"
        className="mr10" variant="contained"
        onClick={() => handleRemoveClick(index)}><RemoveRoundedIcon /></Button>}
        {Default.length - 1 === index && <Button type="button" 
        className={(Default.length > 4) && "hi"} variant="contained" onClick={handleAddClick}><AddRoundedIcon /></Button>}
    </div>
            </td>
            
    </tr>
        
    
);
})}
      </React.Fragment>
             
        </tbody>
      </table>
            </div>
        </React.Fragment>
    )
}

export default Crime