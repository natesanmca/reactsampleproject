import React,{ useReducer,useState,useEffect} from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import { useSelector, useDispatch } from "react-redux";
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import RemoveRoundedIcon from '@material-ui/icons/RemoveRounded';
import Button from "@material-ui/core/Button";
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import NumberFormat from 'react-number-format';
//import CurrencyFormat from 'react-currency-format';
import { v4 as uuidv4 } from 'uuid';
import {
  fetchTimeLine,
  fetchSelectedPolicy,
  fetchCheckState,
} from "../../../../../../Redux/Actions/actions";

import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
  } from '@material-ui/pickers';

const ContractorFloater =(props)=>{
    const [itemList, setitemList] = useState([]);
     const List = useSelector((state) => state.TimeLine);
     const[timeline,settimeline] = React.useState(List.timeline);
    const [Default, setDefault] = React.useState([{ LineItem: "", Limit: "", Deductible:"", LineItem2:"",GroupName: "" }]);
    const [Default1, setDefault1] = React.useState([]);
    const [E2Default, setE2Default] = React.useState([]);
    const [E3Default, setE3Default] = React.useState([]);
    const dispatch = useDispatch();
 
    const [userInput, setUserInput] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {
       
        BuildingLimit1: '',
        BuildingLimit2:''
        }
        );
        const limitAmount = ([
            {Amount:"1000000"},{Amount:"2000000"},{Amount:"3000000"},
          {Amount:"4000000"},{Amount:"5000000"},{Amount:"6000000"}
          ,{Amount:"7000000"},{Amount:"8000000"},{Amount:"9000000"},
          {Amount:"10000000"}
            ]);
            const DeAmount = ([
              {Amount:"500"},{Amount:"1000"},{Amount:"2500"},
            {Amount:"5000"},{Amount:"10000"},{Amount:"15000"}
            ,{Amount:"20000"},{Amount:"30000"},{Amount:"40000"},
            {Amount:"50000"}
              ]);
        const handleChange = evt => {
          const name = evt.target.name;
          let  newValue = evt.target.value;
          setUserInput({[name]: newValue});
        }


        const handleInputChange = (e, index) => {
            const { name, value } = e.target;
            const list = [...itemList];
            let rval = value;

            if(e.target.className == 'numberft'){
              rval=value.replace(/\D/g,'');
            }
            list[index][name] = rval;
            setitemList(list);
            //dispatch(fetchOwnername(operationList))
            let tempTitleList = [...timeline];
        
            let isPolicyTypeAvailable = tempTitleList.find(
              (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
            );
            tempTitleList.map((tempTitle) => {
              if (
                tempTitle.policyTypeId === props.policyTypeId &&
                isPolicyTypeAvailable
              ) {
        
                tempTitle.coverages.map((coverage, coverageIndex) => {
                  if (coverage.coverageId == props.coverages.coverageId) {
                    tempTitle.coverages[coverageIndex].coveragedata[0].CoverageItems = itemList;
                  }
                });
        
              }
        
            });
            settimeline(tempTitleList);
          };
        
          const handleInputChangeSub = (event, index,Id) => {
          
            console.log('OperationChages');
            let tempTitleList = [...itemList];
            let isitemAvailable = tempTitleList.find(
                (tempTitle) => tempTitle.ItemId === Id
              );
            //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
              tempTitleList.map((tempTitle) => {
                if (tempTitle.ItemId === Id && isitemAvailable) {
                    
                  tempTitle.ChildItems[index][event.target.name] = event.target.value;
                  setitemList(tempTitleList);
                  console.log(tempTitleList);
                }
            });
          };

          const handleInputChangeother = (e, index,Id) => {
            const { name, value } = e.target;
            const val = value.replace(/\D/g,'');
            const list = [...Default];
            list.map((other, i) => {
              if (other.Id === Id) {
                list[i][name] = val;
              }
            });
            
            setDefault(list);
            let tempTitleList = [...timeline];
        
            let isPolicyTypeAvailable = tempTitleList.find(
              (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
            );
            tempTitleList.map((tempTitle) => {
              if (
                tempTitle.policyTypeId === props.policyTypeId &&
                isPolicyTypeAvailable
              ) {
        
                tempTitle.coverages.map((coverage, coverageIndex) => {
                  if (coverage.coverageId == props.coverages.coverageId) {
                    tempTitle.coverages[coverageIndex].coveragedata[0].CoverageOtherItem = Default;
                  }
                });
        
              }
        
            });
            settimeline(tempTitleList);
            //dispatch(fetchOwnername(Default))
          };

          const handleRemoveClick = (index,Id) => {
            const list = [...Default];
           
            list.map((other, i) => {
        if (other.Id === Id) {
          list.splice(i, 1);
        }
      });
            
            setDefault(list);
          };
        
          // handle click event of the Add button
          const handleAddClick = (e,Gname) => {
            setDefault([...Default, { Id:uuidv4(), LineItem: "", Limit: "", Deductible:"", LineItem2:"",GroupName: Gname }]);  
          };

         const guid=()=> {
            return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
                this.s4() + '-' + this.s4() + this.s4() + this.s4();
        }
        
        const s4=() => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        useEffect(()=>{
            console.log("Props building",props);

            if (props.coverages) {
              setitemList(props.coverages.coveragedata[0].CoverageItems);
              setDefault(props.coverages.coveragedata[0].CoverageOtherItem);

             }

          },[props.coverages]);

          useEffect(()=>{
            setDefault1([]);
            setE2Default([]);
            setE3Default([]);
            let E1 = [];
            let E2 = [];
            let E3 = [];
            Default.map((dat)=>{
                if (dat.GroupName === "Scheduled Tools") {
                    //setE1Default([...E1Default, {dat}]); 
                    E1.push(dat);
                }
                else if(dat.GroupName === "Scheduled Extensions"){
                    //setE2Default([...E2Default, {dat}]);  
                    E2.push(dat);

                }
                else if(dat.GroupName === "Installation Extensions"){
                    //setE3Default([...E3Default, {dat}]);  
                    E3.push(dat);
                }

              });
              setE3Default(E3); 
              setE2Default(E2); 
              setDefault1(E1); 
              console.log("Default",Default);
              console.log("E1Default",Default1);
              console.log("E2Default",E2Default);
              console.log("E3Default",E3Default);

          },[Default]);

          var tableStyle = {
            "background": "#2e3f50",
            "color": "#fff",
            "text-transform": "capitalize",
         };
         var BorderStyle = {
            "border-right":"1px solid #333"
          };
          var str = "abc's test#s";
str.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_');

console.log("str", str);

    return(
        <React.Fragment>
    <div className="table buildingTable">
       
      <table>
        <thead>
          <th style={{width: '40%'}}>Contractors Floater Coverage</th>

          <th style={{width: '25%'}}>LIMIT</th>
          <th style={{width: '25%'}}> DEDUCTIBLE</th>
          <th style={{width: '10%'}}></th>
        </thead>
       <tbody>
     <React.Fragment>
     <tr>
  <td style={tableStyle}>Scheduled Tools & Equipments</td>
  <td></td>
  <td ></td>
  <td></td>
</tr>
    {itemList && itemList.map((x, i) => {
        if (x.GroupName === "Scheduled Tools") {
          if(x.Limit == 0){
            x.Limit = '';
          }
          if(x.Deductible == 0){
            x.Deductible = '';
          }
        return (
            
               <tr>
                  <td>
                    {x.LineInputType == 1 && <label> {x.LineItem} </label>}
                                       
                    </td>
                    <td>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    // <TextField  name="Limit" value={x.Limit}
                    // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

                    <NumberFormat  class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    }
                      {x.IsLimit && x.LimitInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="Limit"  
                        value={x.Limit}
                        onChange={e => handleInputChange(e, i)}
                        label="Limit"
                        >

                        { limitAmount && limitAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                     
                    
                        
                    {/* <TextField  name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                    <td>
                     {x.IsDeductible && x.DeductibleInputType == 2 &&
                     
                    // <TextField  name="Deductible" value={x.Deductible}
                    // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                    <NumberFormat class="numberft" defaultValue={5} name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    
                    }
                      {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlinedDeductible"
                        name="Deductible"  
                        value={x.Deductible}
                        onChange={e => handleInputChange(e, i)}
                        label="Deductible"
                        >

                        { DeAmount && DeAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                      <td></td>
                      </tr>

                        );
    
                    }

      })}

{ Default1 && Default1.map((dat, index) => {
return (
    
    <tr >
    <td key={guid}>
             {/* <div key={guid} className="OtherField"> */}
             <table class="innerTable">
               <tr>
                 <td>
                 <TextField  name="LineItem" value={dat.LineItem}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Equipment or Tools over" variant="outlined" />
                 </td>
                 <td>
                 <TextField  name="LineItem2" value={dat.LineItem2}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Serial number" variant="outlined" /> 
                 </td>
               </tr>
             </table>
             {/* <div key={guid} className="Ofield1"> <TextField  name="LineItem" value={dat.LineItem}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Equipment or Tools over" variant="outlined" /> </div>
             <div  key={guid} className="Ofield2"> <TextField  name="LineItem2" value={dat.LineItem2}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Serial number" variant="outlined" />  </div> */}

                    {/* </div> */}
            </td>
            
            <td key={guid}>
            {/* <TextField key={guid} name="Limit" value={dat.Limit}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Limit" variant="outlined" /> */}

            <NumberFormat  defaultValue={5} class="numberft" key={guid} name="Limit" value={dat.Limit} 
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
            </td>
            <td key={guid}>
            {/* <TextField key={guid} name="Deductible" value={dat.Deductible}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Deductible" variant="outlined" /> */}
            
            <NumberFormat class="numberft" key={guid} name="Deductible" value={dat.Deductible} 
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
            </td>
            <td key={guid}>
            <div key={guid} className="btn-box" style={{position: "static"}}>
        {Default1.length !== 1 && <Button  type="button"
        className="mr10" variant="contained"
        onClick={() => handleRemoveClick(index,dat.Id)}><RemoveRoundedIcon /></Button>}
        {Default1.length - 1 === index && <Button type="button" 
        className={(Default1.length > 4) && "hi"} variant="contained" onClick={e=>handleAddClick(e,"Scheduled Tools")} ><AddRoundedIcon /></Button>}
    </div>
            </td>
            
    </tr>
        
    
); 

})}
<tr>
 <td style={tableStyle}>EXTENSIONS</td>
  <td></td>
  <td ></td>
  <td></td>
  </tr>

  {itemList && itemList.map((x, i) => {
        if (x.GroupName === "Scheduled Extensions") {
          if(x.Limit == 0){
            x.Limit = '';
          }
          if(x.Deductible == 0){
            x.Deductible = '';
          }
        return (
            
               <tr>
                  <td>
                    {x.LineInputType == 1 && <label> {x.LineItem} </label>}
                                       
                    </td>
                    <td>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    
                    // <TextField  name="Limit" value={x.Limit}
                    // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
                    
                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    }
                      {x.IsLimit && x.LimitInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="Limit"  
                        value={x.Limit}
                        onChange={e => handleInputChange(e, i)}
                        label="Limit"
                        >

                        { limitAmount && limitAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                     
                    
                        
                    {/* <TextField  name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                    <td>
                     {x.IsDeductible && x.DeductibleInputType == 2 &&
                     
                    //  <TextField  name="Deductible" value={x.Deductible}
                    // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    
                    }
                      {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlinedDeductible"
                        name="Deductible"  
                        value={x.Deductible}
                        onChange={e => handleInputChange(e, i)}
                        label="Deductible"
                        >

                        { DeAmount && DeAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                      <td></td>
                      </tr>

                        );
    
                    }

      })}

{ E2Default && E2Default.map((dat, index) => {
if (dat.GroupName === "Scheduled Extensions") {
return (
    
    <tr >
    <td>
            <TextField  name="LineItem" value={dat.LineItem}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Extension" variant="outlined" />
            </td>
            
            <td>
            {/* <TextField  name="Limit" value={dat.Limit}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Limit" variant="outlined" /> */}

            <NumberFormat class="numberft" name="Limit" value={dat.Limit} 
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Limit" variant="outlined" />
            </td>
            <td>
            {/* <TextField  name="Deductible" value={dat.Deductible}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Deductible" variant="outlined" /> */}
            
            <NumberFormat class="numberft" name="Deductible" value={dat.Deductible} 
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
            </td>
            <td>
            <div className="btn-box" style={{position: "static"}}>
        {E2Default.length !== 1 && <Button  type="button"
        className="mr10" variant="contained"
        onClick={() => handleRemoveClick(index,dat.Id)}><RemoveRoundedIcon /></Button>}
        {E2Default.length - 1 === index && <Button type="button" 
        className={(E2Default.length > 4) && "hi"} variant="contained" onClick={e=>handleAddClick(e,"Scheduled Extensions")}><AddRoundedIcon /></Button>}
    </div>
            </td>
            
    </tr>
        
    
); }

})}
<tr>
<td style={tableStyle}>INSTALLATION FLOATER</td>
  <td></td>
  <td ></td>
  <td></td>
  </tr>
  {itemList && itemList.map((x, i) => {
        if (x.GroupName === "Installation Floater") {
          if(x.Limit == 0){
            x.Limit = '';
          }
          if(x.Deductible == 0){
            x.Deductible = '';
          }
        return (
            
               <tr>
                  <td>
                    {x.LineInputType == 1 && <label> {x.LineItem} </label>}
                                       
                    </td>
                    <td>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    
                    
                    // <TextField  name="Limit" value={x.Limit}
                    // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    }
                      {x.IsLimit && x.LimitInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="Limit"  
                        value={x.Limit}
                        onChange={e => handleInputChange(e, i)}
                        label="Limit"
                        >

                        { limitAmount && limitAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                     
                    
                        
                    {/* <TextField  name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                    <td>
                     {x.IsDeductible && x.DeductibleInputType == 2 &&
                     
                    //  <TextField  name="Deductible" value={x.Deductible}
                    // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />
                    
                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    }
                      {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlinedDeductible"
                        name="Deductible"  
                        value={x.Deductible}
                        onChange={e => handleInputChange(e, i)}
                        label="Deductible"
                        >

                        { DeAmount && DeAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                      <td></td>
                      </tr>

                        );
    
                    }

      })}
<tr>
<td style={tableStyle}>EXTENSIONS</td>
  <td></td>
  <td ></td>
  <td></td>
  </tr>
  {itemList && itemList.map((x, i) => {
        if (x.GroupName === "Installation Extensions") {
          if(x.Limit == 0){
            x.Limit = '';
          }
          if(x.Deductible == 0){
            x.Deductible = '';
          }
        return (
            
               <tr>
                  <td>
                    {x.LineInputType == 1 && <label> {x.LineItem} </label>}
                                       
                    </td>
                    <td>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    
                    // <TextField  name="Limit" value={x.Limit}
                    // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    
                    }
                      {x.IsLimit && x.LimitInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="Limit"  
                        value={x.Limit}
                        onChange={e => handleInputChange(e, i)}
                        label="Limit"
                        >

                        { limitAmount && limitAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                     
                    
                        
                    {/* <TextField  name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                    <td>
                     {x.IsDeductible && x.DeductibleInputType == 2 &&
                     
                    //  <TextField  name="Deductible" value={x.Deductible}
                    // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    
                    }
                      {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlinedDeductible"
                        name="Deductible"  
                        value={x.Deductible}
                        onChange={e => handleInputChange(e, i)}
                        label="Deductible"
                        >

                        { DeAmount && DeAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                      <td></td>
                      </tr>

                        );
    
                    }

      })}

{ E3Default && E3Default.map((dat1, index) => {
if (dat1.GroupName === "Installation Extensions") {
return (
    
    <tr >
    <td>
            <TextField  name="LineItem" value={dat1.LineItem}
            onChange={e => handleInputChangeother(e, index,dat1.Id)} label="Extension" variant="outlined" />
            </td>
            
            <td>
            {/* <TextField  name="Limit" value={dat1.Limit}
            onChange={e => handleInputChangeother(e, index,dat1.Id)} label="Limit" variant="outlined" /> */}

            <NumberFormat class="numberft" name="Limit" value={dat1.Limit} 
            onChange={e => handleInputChangeother(e, index,dat1.Id)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
            </td>
            <td>
            {/* <TextField  name="Deductible" value={dat1.Deductible}
            onChange={e => handleInputChangeother(e, index,dat1.Id)} label="Deductible" variant="outlined" /> */}
            
            <NumberFormat class="numberft" name="Deductible" value={dat1.Deductible} 
            onChange={e => handleInputChangeother(e, index,dat1.Id)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'}/>
            </td>
            <td>
            <div className="btn-box" style={{position: "static"}}>
        {E3Default.length !== 1 && <Button  type="button"
        className="mr10" variant="contained"
        onClick={() => handleRemoveClick(index,dat1.Id)}><RemoveRoundedIcon /></Button>}
        {E3Default.length - 1 === index && <Button type="button" 
        className={(E3Default.length > 4) && "hi"} variant="contained" onClick={e=>handleAddClick(e,"Installation Extensions")}><AddRoundedIcon /></Button>}
    </div>
            </td>
            
    </tr>
        
    
); }

})}

<tr>
<td style={tableStyle}>CONTRACTOR'S E&O</td>
  <td></td>
  <td ></td>
  <td></td>
  </tr>
  {itemList && itemList.map((x, i) => {
      if (x.GroupName === "CONTRACTOR'S E&O" && x.ItemId === "56592701-143b-4cda-8bed-e3afcc9b3687") 
      {
        if(x.Limit == 0){
          x.Limit = '';
        }
        if(x.Deductible == 0){
          x.Deductible = '';
        }
          return(
              <tr>
              <td>
              {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                           if (ch.LineItem === "Retro") {
                              
                          return(
                          
                      <div>
                      <MuiPickersUtilsProvider  utils={DateFnsUtils}>
                          <div className="datePicker">
                          <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          format="MM/dd/yyyy"
                          margin="normal"
                          name="ValData"
                          id="date-picker-inline"
                          label="Retro Date"
                          value={(ch.ValData === "") ? new Date() : ch.ValData}
                          onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                          KeyboardButtonProps={{
                          'aria-label': 'change date',
                          }}
                          />
                          </div>
                          </MuiPickersUtilsProvider>
                      </div> )}
                          })}
              </td>
              <td>
      {x.IsLimit && x.LimitInputType == 2 &&
      
      // <TextField  name="Limit" value={x.Limit}
      // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

      <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'}/>
      
      }
          {x.IsLimit && x.LimitInputType == 5 && <FormControl variant="outlined" className="select">
          <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
          <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          name="Limit"  
          value={x.Limit}
          onChange={e => handleInputChange(e, i)}
          label="Limit"
          >

          { limitAmount && limitAmount.map((item, index)=>{
          return(
          <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

          })} 

          </Select>
          </FormControl> }
          </td>
          
      
          
      {/* <TextField  name="Deductible" value={x.Deductible} 
      onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
      <td>
          {x.IsDeductible && x.DeductibleInputType == 2 &&
          
      //     <TextField  name="Deductible" value={x.Deductible}
      // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

      <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
      
      }
          {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
          <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
          <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          name="Deductible"  
          value={x.Deductible}
          onChange={e => handleInputChange(e, i)}
          label="Deductible"
          >

          { DeAmount && DeAmount.map((item, index)=>{
          return(
          <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

          })} 

          </Select>
          </FormControl> }
          </td>
              <td></td>
              </tr>
          )
          
      }
        else if (x.GroupName === "CONTRACTOR'S E&O" && x.ItemId !== "56592701-143b-4cda-8bed-e3afcc9b3687") {
          if(x.Limit == 0){
            x.Limit = '';
          }
          if(x.Deductible == 0){
            x.Deductible = '';
          }
          return (
            
               <tr>
                  <td>
                    {x.LineInputType == 1 && <label> {x.LineItem} </label>}
                                       
                    </td>
                    <td>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    
                    // <TextField  name="Limit" value={x.Limit}
                    // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
                    
                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
                    }
                      {x.IsLimit && x.LimitInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="Limit"  
                        value={x.Limit}
                        onChange={e => handleInputChange(e, i)}
                        label="Limit"
                        >

                        { limitAmount && limitAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                     
                    
                        
                    {/* <TextField  name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                    <td>
                     {x.IsDeductible && x.DeductibleInputType == 2 &&
                     
                    //  <TextField  name="Deductible" value={x.Deductible}
                    // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    
                    }
                      {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlinedDeductible"
                        name="Deductible"  
                        value={x.Deductible}
                        onChange={e => handleInputChange(e, i)}
                        label="Deductible"
                        >

                        { DeAmount && DeAmount.map((item, index)=>{
                        return(
                        <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                      <td></td>
                      </tr>

                        );
    
                    }

      })}

      </React.Fragment>
             
        </tbody>
      </table>
    </div>
        </React.Fragment>
    )
}

export default ContractorFloater