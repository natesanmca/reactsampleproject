import React,{ useReducer,useEffect,useState} from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import { useSelector, useDispatch } from "react-redux";
import NumberFormat from 'react-number-format';
import axios from 'axios';
import {
  fetchTimeLine,
  fetchSelectedPolicy,
  fetchCheckState,
} from "../../../../../../Redux/Actions/actions";
const API_URL = process.env.REACT_APP_BASE_API_URL;
const BussinessInteruption =(props)=>{
  const [itemList, setitemList] = useState([]);
  const List = useSelector((state) => state.TimeLine);
  const[timeline,settimeline] = React.useState(List.timeline);
  const submission = useSelector((state) => state.SubMission);
  const dispatch = useDispatch();
    const [userInput, setUserInput] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {
        radio: '',
        building: '',
        BuildingDeductible: '',
        BuildingLimit:''
        }
        );
        const Months = ([
          {Month:"12 Months"},{Month:"18 Months"},{Month:"24 Months"}
          ]);
        const handleChange = evt => {
          const name = evt.target.name;
          let  newValue = evt.target.value;
          setUserInput({[name]: newValue});
        }
        const handleInputChangeSub = (event, index,Id) => {
  
          console.log('OperationChages');
          let tempTitleList = [...itemList];
          let isitemAvailable = tempTitleList.find(
              (tempTitle) => tempTitle.ItemId === Id
            );
          //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
            tempTitleList.map((tempTitle) => {
              if (tempTitle.ItemId === Id && isitemAvailable) {
                  
                tempTitle.ChildItems[index][event.target.name] = event.target.value;
                  // if (tempTitle.ChildItems[index].ChildItemId === "3c0b51b1-d91e-423a-b241-4fdd1eb2d07f") {
                  //     setselected(event.target.value);
                  // }
                setitemList(tempTitleList);
                console.log(tempTitleList);
              }
          });
        };

        const handleInputChange = (e, index) => {
          const { name, value } = e.target;
          const list = [...itemList];
          let rval = value;

            if(e.target.className == 'numberft'){
              rval=value.replace(/\D/g,'');
            }
          list[index][name] = rval;
          setitemList(list);
        };
        const DataLoad = () => {

          const subVal = {
              "ID": props.coverageId
            };
          console.log(subVal);
          axios.post(`${API_URL}Quote/get-coverageItem`, subVal)
            .then(res => {
              if(res.data.Success){ 
                     let itemLists = res.data.Data;
                     //let list = JSON.stringify(classDetails)
                     //let listItem = JSON.parse(list)
                     if (res.data.Data.length > 0) {
                      setitemList(res.data.Data[0].CoverageItems);
                      
                     }
                     console.log(res.data.Data);
             }
            // console.log(operationList);
             console.log(itemList);
          
          });
        }
        useEffect(()=>{
          console.log("Props building",props);
          if (submission.SubmissionNo === "0") {
            DataLoad();
          }
          else{
            if (props.ItemData) {
              setitemList(props.ItemData.CoverageItems);
            }
          }
        },[props.ItemData]);


        useEffect(() => {
   

          dispatch(fetchTimeLine(timeline));
      
      
          console.log("TimeLine dynamic",timeline);
       
        
      }, [timeline]);

      useEffect(()=>{
        let tempTitleList = [...timeline];
      
        let isPolicyTypeAvailable = tempTitleList.find(
          (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
        );
        tempTitleList.map((tempTitle) => {
          if (
            tempTitle.policyTypeId === props.policyTypeId &&
            isPolicyTypeAvailable
          ) {
    
            tempTitle.coverages.map((coverage, coverageIndex) => {
              if (coverage.coverageId == props.coverageId) {
                coverage.coveragedata.map((dat,i)=> {
                    if (dat.locationId === props.LocationId) {
                      tempTitle.coverages[coverageIndex].coveragedata[i].CoverageItems = itemList;
                    }
                });
              }
            });
    
          }
    
        });
        settimeline(tempTitleList);

       },[itemList]);

        var tableStyle = {
          "background": "#2e3f50",
          "color": "#fff",
          "text-transform": "capitalize",
       };
       var BorderStyle = {
          "border-right":"1px solid #333"
        };
    return(
        <React.Fragment>
            <div className="table buildingTable">
                <table>
                    <thead>
                         <th style={{width: '35%'}}></th>
                        <th style={{width: '25%'}}>COVERAGE INFO</th>
                <th style={{width: '20%'}}>LIMIT</th>
                <th style={{width: '20%'}}> DEDUCTIBLE</th>
               
                        
                    </thead>
                    <tbody>
                    {itemList && itemList.map((x, i) => {
 if(x.Limit == 0){
  x.Limit = '';
}
if(x.Deductible == 0){
  x.Deductible = '';
}
if (x.GroupName === "") {
return (
    
       <tr>
          
                <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                <td>
                <div className="radioField">
                    {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                        
                        if (ch.LineInputType === 2) {
                          return(
                       
                            <TextField  name="ValData" value={ch.ValData}
                             onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="Revenue" variant="outlined" /> );
                          }
                        else if (ch.LineInputType === 5) {
                        return(
                     
                          <div className="field2"> <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> Months </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id="demo-simple-select-outlined"
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label=""
                   >

                   { Months && Months.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.Month}>{item.Month}</MenuItem>)

                   })} 

                   </Select>
                </FormControl> </div> );
                        }
                    })}
                    </div>
                </td>

            <td style={BorderStyle}>
            {x.IsLimit && x.LimitInputType == 2 &&
            
            // <TextField  name="Limit" value={x.Limit}
            //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
             
            <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$ '} />
             }
              </td>
            <td >
            {x.IsDeductible && x.DeductibleInputType == 2 &&
//             <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$ '}/>
 }

              </td>

              </tr>

        );
}
 

})}
                    </tbody>
                </table>
            </div>
        </React.Fragment>
    )
}

export default BussinessInteruption