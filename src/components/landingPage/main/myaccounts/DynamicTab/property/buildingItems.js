import React,{ useReducer,useState,useEffect} from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import { useSelector, useDispatch } from "react-redux";
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import RemoveRoundedIcon from '@material-ui/icons/RemoveRounded';
import Button from "@material-ui/core/Button";
import NumberFormat from 'react-number-format';
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';
import {
  fetchTimeLine,
  fetchSelectedPolicy,
  fetchCheckState,
} from "../../../../../../Redux/Actions/actions";
const API_URL = process.env.REACT_APP_BASE_API_URL;

const BuildingItems =(props)=>{
    const [itemList, setitemList] = useState([]);
    const [] = useState([]);
    const [] = useState([]);
    const List = useSelector((state) => state.TimeLine);
  const[timeline,settimeline] = React.useState(List.timeline);
    const [Default, setDefault] = useState([{ LineItem: "", Limit: "", Deductible:"" }]);
    const dispatch = useDispatch();
    const submission = useSelector((state) => state.SubMission);
    const [userInput, setUserInput] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {
        radio: '',
        building: '',
        BuildingDeductible: '',
        BuildingLimit:''
        }
        );
        const CoInsurance = ([
          {Percent:"0%"},{Percent:"80%"},{Percent:"90%"},{Percent:"100%"}
          ]);
        const handleChange = evt => {
          const name = evt.target.name;
          let  newValue = evt.target.value;
          setUserInput({[name]: newValue});
        }
        const handleInputChangeSub = (event, index,Id) => {
  
          console.log('OperationChages');
          let tempTitleList = [...itemList];
          let isitemAvailable = tempTitleList.find(
              (tempTitle) => tempTitle.ItemId === Id
            );
          //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
            tempTitleList.map((tempTitle) => {
              if (tempTitle.ItemId === Id && isitemAvailable) {
                  
                tempTitle.ChildItems[index][event.target.name] = event.target.value;
                  // if (tempTitle.ChildItems[index].ChildItemId === "3c0b51b1-d91e-423a-b241-4fdd1eb2d07f") {
                  //     setselected(event.target.value);
                  // }
                setitemList(tempTitleList);
                console.log(tempTitleList);
              }
          });
        };

        const handleInputChange = (e, index) => {
          const { name, value } = e.target;
          const list = [...itemList];
          let rval = value;

          if(e.target.className == 'numberft'){
            rval=value.replace(/\D/g,'');
          }
          list[index][name] = rval;
          setitemList(list);

                  };

     
        const handleInputChangeother = (e, index) => {
          const { name, value } = e.target;
          let rval = value;

            if(e.target.className == 'numberft'){
              rval=value.replace(/\D/g,'');
            }
          const list = [...Default];
          list[index][name] = rval;
          setDefault(list);
 
          //dispatch(fetchOwnername(Default))
        };
        const handleRemoveClick = index => {
          const list = [...Default];
          list.splice(index, 1);
          setDefault(list);
        };
      
        // handle click event of the Add button
        const handleAddClick = () => {
          setDefault([...Default, { Id:uuidv4(), LineItem: "", Limit: "", Deductible:"", LineItem2:"",GroupName: "" }]);    
        };

        const DataLoad = () => {

          const subVal = {
              "ID": props.coverageId
            };
          console.log(subVal);
          axios.post(`${API_URL}Quote/get-coverageItem`, subVal)
            .then(res => {
              if(res.data.Success){ 
                     let itemLists = res.data.Data;
                     //let list = JSON.stringify(classDetails)
                     //let listItem = JSON.parse(list)
                     if (res.data.Data.length > 0) {
                      setitemList(res.data.Data[0].CoverageItems);
                      setDefault(res.data.Data[0].CoverageOtherItem);
                     }
                     console.log(res.data.Data);
             }
            // console.log(operationList);
             console.log(itemList);
          
          });
        }
        // useEffect(()=>{
        //   if (submission.SubmissionNo === "0") {
        //     DataLoad();
        //   }

        // },[]);
        useEffect(()=>{
          console.log("Props building",props);
          // console.log("Coverages",props.location.state.id.coverages);
          // props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{
          //   console.log(data);
          if (submission.SubmissionNo === "0") {
            DataLoad();
          }
          else{
          if (props.ItemData) {
            setitemList(props.ItemData.CoverageItems);
            setDefault(props.ItemData.CoverageOtherItem);
          }
        }
          // //   data.coveragedata.CoverageItems && data.coveragedata.CoverageItems.map((item)=>{
          // //     if (item.ItemId === "d4edafea-5b80-483a-8d3e-9e3f59a3e07c") {
          // //         console.log("selectdata",item.ChildItems[0].ValData);
          // //         setselected(item.ChildItems[0].ValData);
          // //     }
      
          // // });
          // });
      
          
         // DataLoad();
        },[props.ItemData]);

        useEffect(() => {
   

          dispatch(fetchTimeLine(timeline));
      
      
          console.log("TimeLine dynamic",timeline);
       
        
      }, [timeline]);
       useEffect(()=>{
        let tempTitleList = [...timeline];
      
        let isPolicyTypeAvailable = tempTitleList.find(
          (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
        );
        tempTitleList.map((tempTitle) => {
          if (
            tempTitle.policyTypeId === props.policyTypeId &&
            isPolicyTypeAvailable
          ) {
    
            tempTitle.coverages.map((coverage, coverageIndex) => {
              if (coverage.coverageId == props.coverageId) {
                coverage.coveragedata.map((dat,i)=> {
                    if (dat.locationId === props.LocationId) {
                      tempTitle.coverages[coverageIndex].coveragedata[i].CoverageOtherItem = Default;
                    }
                });
              }
            });
    
          }
    
        });
        settimeline(tempTitleList);
       },[Default]);
       useEffect(()=>{
        let tempTitleList = [...timeline];
      
        let isPolicyTypeAvailable = tempTitleList.find(
          (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
        );
        tempTitleList.map((tempTitle) => {
          if (
            tempTitle.policyTypeId === props.policyTypeId &&
            isPolicyTypeAvailable
          ) {
    
            tempTitle.coverages.map((coverage, coverageIndex) => {
              if (coverage.coverageId == props.coverageId) {
                coverage.coveragedata.map((dat,i)=> {
                    if (dat.locationId === props.LocationId) {
                      tempTitle.coverages[coverageIndex].coveragedata[i].CoverageItems = itemList;
                    }
                });
              }
            });
    
          }
    
        });
        settimeline(tempTitleList);

       },[itemList]);
        var tableStyle = {
          "background": "#2e3f50",
          "color": "#fff",
          "text-transform": "capitalize",
       };
       var BorderStyle = {
          "border-right":"1px solid #333"
        };
    return(
        <React.Fragment>
            <div className="table buildingTable">
                <table>
                    <thead>
                        
                      <th style={{width: '30%'}}>BUILDING & CONTENTS  INFORMATION</th>
                      <th style={{width: '30%'}}>Coverage Info</th>
                      <th style={{width: '15%'}}>LIMIT</th>
                      <th style={{width: '15%'}}>DEDUCTIBLE</th>
                      <th style={{width: '10%'}}></th>
                    </thead>
                    <tbody>

  {itemList && itemList.map((x, i) => {
    if(x.Limit == 0){
      x.Limit = '';
    }
    if(x.Deductible == 0){
      x.Deductible = '';
    }

if (x.GroupName === "") {
return (
    
       <tr>
          
                <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                <td>
                <div className="radioField">
                    {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                        
                        if (ch.LineInputType === 4) {
                          return(
                       
                            <div className="field1"> <FormControl component="fieldset" className="row-radio">
                            <FormLabel component="legend"></FormLabel>
                            <RadioGroup aria-label="radio" name="ValData" value={ch.ValData} onChange={e =>handleInputChangeSub(e,ind,x.ItemId)}>
                              <FormControlLabel value="RC" control={<Radio />} label="RC" />
                              <FormControlLabel value="ACV" control={<Radio />} label="ACV" />
                              <FormControlLabel value="POED" control={<Radio />} label="POED" />
                            </RadioGroup>
                          </FormControl></div> );
                          }
                        else if (ch.LineInputType === 5) {
                        return(
                     
                          <div className="field2"> <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> Co-Insurance (%) </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id="demo-simple-select-outlined"
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label=""
                   >

                   { CoInsurance && CoInsurance.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.Percent}>{item.Percent}</MenuItem>)

                   })} 

                   </Select>
                </FormControl> </div> );
                        }
                    })}
                    </div>
                </td>

            <td style={BorderStyle}>
            {x.IsLimit && x.LimitInputType == 2 &&
            // <TextField  name="Limit" value={x.Limit}
            //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$ '} />
              }
              </td>
            <td >
            {x.IsDeductible && x.DeductibleInputType == 2 &&
//             <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$ '} />
 }

              </td>
                    <td></td>
              </tr>

        );
}
 

})}

<tr>
  <td style={tableStyle}>ELECTRONIC DATA PROCESSING SYSTEMS</td>
  <td></td>
  <td style={BorderStyle}></td>
  <td></td>
  <td></td>
</tr>

{itemList && itemList.map((x, i) => {

if (x.GroupName === "ELECTRONIC") {
return (
    
       <tr>
          
                <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                <td></td>

            <td style={BorderStyle}>
            {x.IsLimit && x.LimitInputType == 2 &&
            // <TextField  name="Limit" value={x.Limit}
            //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$ '} />
              }
              </td>
            <td >
            {x.IsDeductible && x.DeductibleInputType == 2 &&
//             <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$ '} />
 }

              </td>
<td></td>
              </tr>

        );
}
 

})}

{ Default && Default.map((dat, index) => {

return (
    
    <tr >
    <td>
            <TextField  name="LineItem" value={dat.LineItem}
            onChange={e => handleInputChangeother(e, index)} label="" variant="outlined" />
            </td>
        <td></td>    
            <td style={BorderStyle}>
            {/* <TextField  name="Limit" value={dat.Limit}
            onChange={e => handleInputChangeother(e, index)} label="Limit" variant="outlined" /> */}

            <NumberFormat class="numberft" name="Limit" value={dat.Limit} 
            onChange={e => handleInputChangeother(e, index)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$ '} />
            </td>
            <td >
            {/* <TextField  name="Deductible" value={dat.Deductible}
            onChange={e => handleInputChangeother(e, index)} label="Deductible" variant="outlined" /> */}

            <NumberFormat class="numberft" name="Deductible" value={dat.Deductible} 
            onChange={e => handleInputChangeother(e, index)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$ '} />
             
            </td>
            <td>
            <div className="btn-box" style={{position: "static"}}>
        {Default.length !== 1 && <Button  type="button"
        className="mr10" variant="contained"
        onClick={() => handleRemoveClick(index)}><RemoveRoundedIcon /></Button>}
        {Default.length - 1 === index && <Button type="button" 
        className={(Default.length > 4) && "hi"} variant="contained" onClick={handleAddClick}><AddRoundedIcon /></Button>}
    </div>
            </td>
            
    </tr>
        
    
);
})}

<tr>
  <td style={tableStyle}>Contingent Condominium</td>
  <td></td>
  <td style={BorderStyle}></td>
  <td></td>
  <td></td>
</tr>

{itemList && itemList.map((x, i) => {

if (x.GroupName === "Contingent Condominium") {
return (
    
       <tr>
          
                <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                <td></td>

            <td style={BorderStyle}>
            {x.IsLimit && x.LimitInputType == 2 &&
            // <TextField  name="Limit" value={x.Limit}
            //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

            <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$ '} />
            }
              </td>
            <td >
            {x.IsDeductible && x.DeductibleInputType == 2 &&
//             <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

            <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
            onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$ '} />
            }

              </td>
<td></td>
              </tr>

        );
}
 

})}
                       
                    </tbody>
                </table>
            </div>
        </React.Fragment>
    )
}

export default BuildingItems