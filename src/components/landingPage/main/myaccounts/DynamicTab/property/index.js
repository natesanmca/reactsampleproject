import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import {useAlert} from 'react-alert'
import axios from 'axios';
import {
  fetchTimeLine,
  fetchSelectedPolicy,
  fetchCheckState,
} from "../../../../../../Redux/Actions/actions";
import LocationList from "./locationList";
import Crime from "./crime";
import ContractorFloater from "./contractorFloater";
const API_URL = process.env.REACT_APP_BASE_API_URL;
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

const Property = (props) => {
  const classes = useStyles();
  const List = useSelector((state) => state.TimeLine);
  const Account = useSelector(state=>state.Account);
const submission = useSelector((state) => state.SubMission);
const[timeline,settimeline] = useState(List.timeline);
  console.log("list", List);
  const policyData = props.location.state.id;
  const PolicyReducer = useSelector(state => state.PolicyQuote);
  let history = useHistory();
  const alert = useAlert();
  // const policyData = List.timeline.find(
  //   (data) => data.policyType === "Property"
  // );

  const renderFirstAccordian = () => {
    const hasBusinessOrBuilding = policyData.coverages.find(
      (coverageData) =>
        coverageData.coverageId === "3b964469-136a-4c5f-8c46-e53eb0d4e3ac" ||
        coverageData.coverageId === "c51de115-1cbe-4b58-bf9e-7fd35b98a372" ||
        coverageData.coverageId === "7fb24475-cc3d-4999-af11-ade7f2cf022a"
    );
     
    if (hasBusinessOrBuilding) {
      return (
        <Accordion className="accordian">
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>
              Property Coverage
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              <LocationList coverages={policyData}></LocationList>
            </Typography>
          </AccordionDetails>
        </Accordion>
      );
    }
  };
const renderAccordianContent = (coverageId) =>{
 
   switch (coverageId) {
      case "1865c12d-f4fd-4c93-83fd-855b916b5f89":
        let coveragedatafloater = policyData.coverages.find(
          (coverageData) =>
            coverageData.coverageId === "1865c12d-f4fd-4c93-83fd-855b916b5f89");
       return  <ContractorFloater coverages={coveragedatafloater} policyTypeId = {policyData.policyTypeId}></ContractorFloater>
        break;
        case "b66a34e0-4a95-4f90-8c55-2d98613f079f":
          return  <p>Cargo Floater</p>
          break;
          case "c8adf2f6-6219-45cb-b32e-60b54fe102a0":
            let coveragedata = policyData.coverages.find(
              (coverageData) =>
                coverageData.coverageId === "c8adf2f6-6219-45cb-b32e-60b54fe102a0");
            return   <Crime coverages={coveragedata} policyTypeId = {policyData.policyTypeId}></Crime>
            break;
            case "384f622e-6c1a-4f38-a564-60560b7169aa":
              return   <p>Equipment</p>
              break;
    
      default:
        break;
    }
 
}
  const renderAccordian = () => {
    let hasBusinessOrBuilding = false;

    return policyData.coverages.map((coverageData, index) => {
      if (
        !(
          coverageData.coverageId === "3b964469-136a-4c5f-8c46-e53eb0d4e3ac" ||
          coverageData.coverageId === "c51de115-1cbe-4b58-bf9e-7fd35b98a372" ||
          coverageData.coverageId === "7fb24475-cc3d-4999-af11-ade7f2cf022a"
        )
      ) {
        return (
          <Accordion key={index} className="accordian">
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>
                {coverageData.coverageName}
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
             {renderAccordianContent(coverageData.coverageId)}
              </Typography>
            </AccordionDetails>
          </Accordion>
        );
      }
    });
  };

  useEffect(()=>{
    console.log("Props Property",props);
    //console.log("Coverages",props.location.state.id.coverages);
   

    
   // DataLoad();
  },[]);
  const onClear = () =>{
    //DataLoad();
}


const onSubmission = (event) => {
  if (PolicyReducer.SubMissionType === "Submission") {
  const Request = {
      AccountNumber : Account.AcNo,
      SubMissionNo : submission.SubmissionNo,
      policylist : timeline
     // otherItem : Default

  }

  
  console.log("Request Coverage Test");
  console.log("Request Coverage ",Request);
  console.log("Request String",JSON.stringify(Request));
  let url = API_URL + 'Quote/new-policy';
  //https://localhost:5000/api/Quote/new-policy
  axios.post(url, Request)
  .then(res => {
    console.log("red",res);
      if(res.data.Success){    
          //let bdmList = res.data.Data;
         
          alert.success(res.data.Message,{
            timeout:5000,
            onClose: () => {
             history.push("/myaccounts/accountDetails/Submissions");
            }
          });
          //alert(res.data.Message);
       //  console.log(res.data.Message);
       //  history.push("/UserComp");
        }
        else {
         alert.info(res.data.Message);
        }

  // console.log(operationList);
   //console.log(itemList);

});
  }
  else if (PolicyReducer.SubMissionType === "InsurerQuote") {

    const Request = {
      AccountNumber : Account.AcNo,
      SubMissionNo : submission.SubmissionNo,
      InsurerQuotationNo : PolicyReducer.QuotationNo,
      policylist : timeline,
      InsurerInfoview : PolicyReducer.InsurerInfo,
      BindingInfoview : PolicyReducer.BindingInfo,
      InforceInfoview : PolicyReducer.InforceInfo
     // otherItem : Default
  
  }
  console.log("Request Quote Coverage Test7777");
  console.log("Request Quote Coverage ",Request);
  console.log("Request Quote String",JSON.stringify(Request));
  console.log("PolicyReducer",PolicyReducer);
  let url = API_URL + 'Policy/new-policy-quote';
  
  axios.post(url, Request)
  .then(res => {
    
      if(res.data.Success){    
          //let bdmList = res.data.Data;
          fileupload(res.data);
          //alert(res.data.Message);
       //  console.log(res.data.Message);
       //  history.push("/UserComp");
        }
        else {
         alert.info(res.data.Message);
        }
  
  // console.log(operationList);
 //  console.log(itemList);
  
  });
    
  }
  
    }
  const fileupload = (ResData)=>{
    let formData = new FormData();
    formData.append('InsurerQuotationNo', ResData.Data.InsurerQuotationNo);
    formData.append('BindingId', ResData.Data.BindingId);
    formData.append('InsurerId', ResData.Data.InsurerId);
    formData.append('InForceId', ResData.Data.InForceId);
    PolicyReducer.InsurerInfofiles && PolicyReducer.InsurerInfofiles.map((file)=>{
      formData.append('InsurerFiles', file);
      console.log("file",file);
    })
    PolicyReducer.BindingInfofiles && PolicyReducer.BindingInfofiles.map((file)=>{
      formData.append('BindingFiles', file);
      console.log("file",file);
    })
    PolicyReducer.InforceInfofiles && PolicyReducer.InforceInfofiles.map((file)=>{
      formData.append('InForceFiles', file);
      console.log("file",file);
    })
  
    axios.post(`${API_URL}Policy/file-upload`, formData)
  .then(res => {
    
      if(res.data.Success){    
          //let bdmList = res.data.Data;
          alert.success(ResData.Message,{
            timeout:5000,
            onClose: () => {
             history.push("/myaccounts/accountDetails/policys");
            }
          });
        }
        else {
         alert.info(res.data.Message);
        }
  
  // console.log(operationList);
   //console.log(itemList);
  
  });
  
  }
  return (
    <div >
      {renderFirstAccordian()}
      {renderAccordian()}

            <div className="btn-group">

            {props.location.state.total === props.location.state.index + 1 && <Button variant="contained" name="cancel" type="button" onClick={onClear}> 
            CANCEL
            </Button>}

            {props.location.state.total === props.location.state.index + 1 &&<Button variant="contained" name="save" color="primary" type="button" onClick={onSubmission}>

            SAVE
            </Button>}
       </div>

    </div>
  );
};

export default Property;
