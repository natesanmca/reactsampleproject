
import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {useSelector, useDispatch} from "react-redux";
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import 'date-fns';
import {useAlert} from 'react-alert'
import { useHistory } from "react-router-dom";
import NumberFormat from 'react-number-format';
import {
    fetchTimeLine,
    fetchSelectedPolicy,
    fetchCheckState,
  } from "../../../../../Redux/Actions/actions";

//import "./dynamic.css"
const API_URL = process.env.REACT_APP_BASE_API_URL;
const CocCoverage=(props)=>{

console.log('Test Click')
console.log('Wrapup Cliock',props)


const [operationList, setoperationList] = useState([]);
const [itemList, setitemList] = useState([]);
const [] = useState([]);
const [] = useState([]);
const [Default, setDefault] = useState([{ LineItem: "", Limit: "", Deductible:"" }]);
const [building, setbuilding] = useState([{ buildingId: "", location: "" }]);
const Account = useSelector(state=>state.Account);
const submission = useSelector((state) => state.SubMission);
const stepperList = useSelector(state => state.TimeLine);
const[timeline,settimeline] = useState(stepperList.timeline);
const[selected,setselected] = useState("");
const policyId = props.location.state.id.policyTypeId;
const PolicyReducer = useSelector(state => state.PolicyQuote);
let history = useHistory();
const alert = useAlert();
const dispatch = useDispatch();

const applicant = ([
  {Applicant:"General Contractor"},{Applicant:"Project Owner"},{Applicant:"Project Manager"}
  ]);

  const projecttown = ([
    {Project:"Townhomes"},{Project:"Semi-Detached"},{Project:"Single Detached"},
    {Project:"Duplex"},{Project:"Triplex"},{Project:"Fourplex"},
    {Project:"Commercial Industrial"},{Project:"Commercial Plaza"}
    ]);

  const ifoptions = ([
    {section:"Blanket"},{section:"Scheduled"}
    ]);
    const DeAmount = ([
      {Amount:"$0"},{Amount:"$1,000"},{Amount:"$2,500"},
      {Amount:"$,5000"},{Amount:"$7,500"},{Amount:"$10,000"}
      
        ]);
   

  // handle input change
  
  const handleInputChange = (e, index) => {
    console.log(e.target.className);
    const { name, value } = e.target;
    const list = [...itemList];
    let rval = value;

    if(e.target.className == 'numberft'){
      rval=value.replace(/\D/g,'');
    }
    list[index][name] = rval;
    setitemList(list);
    //dispatch(fetchOwnername(operationList))
    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === policyId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === policyId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == "cf321ac3-87f5-42de-9ee2-d0229a0e6bcc") {
            tempTitle.coverages[coverageIndex].coveragedata[0].CoverageItems = itemList;
          }
        });

      }

    });
    settimeline(tempTitleList);
  };

  const handleInputChangeSub = (event, index,Id) => {
  
    console.log('OperationChages');
    let tempTitleList = [...itemList];
    let isitemAvailable = tempTitleList.find(
        (tempTitle) => tempTitle.ItemId === Id
      );
    //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
      tempTitleList.map((tempTitle) => {
        if (tempTitle.ItemId === Id && isitemAvailable) {
            
          tempTitle.ChildItems[index][event.target.name] = event.target.value;
            if (tempTitle.ChildItems[index].ChildItemId === "3c0b51b1-d91e-423a-b241-4fdd1eb2d07f") {
                setselected(event.target.value);
            }
          setitemList(tempTitleList);
          console.log(tempTitleList);
        }
    });
  };

  // handle click event of the Remove button

  // handle click event of the Add button
  const onSubmission = (event) => {
    
    console.log("submission Type",PolicyReducer);
    if (PolicyReducer.SubMissionType === "Submission") {
    const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        policylist : timeline
       // otherItem : Default

    }

    
    console.log("Request Coverage Test");
    console.log("Request Coverage ",Request);
    console.log("Request String",JSON.stringify(Request));
    let url = API_URL + 'Quote/new-policy';
    //https://localhost:5000/api/Quote/new-policy
    axios.post(url, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(res.data.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/Submissions");
              }
            });
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
 
    // console.log(operationList);
     console.log(itemList);
  
  });
    }
    else if (PolicyReducer.SubMissionType === "InsurerQuote") {

      const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        InsurerQuotationNo : PolicyReducer.QuotationNo,
        policylist : timeline,
        InsurerInfoview : PolicyReducer.InsurerInfo,
        BindingInfoview : PolicyReducer.BindingInfo,
        InforceInfoview : PolicyReducer.InforceInfo
       // otherItem : Default
    
    }
    console.log("Request Quote Coverage Test");
    console.log("Request Quote Coverage ",Request);
    console.log("Request Quote String",JSON.stringify(Request));
    console.log("PolicyReducer",PolicyReducer);
    let url = API_URL + 'Policy/new-policy-quote';
    
    axios.post(url, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            fileupload(res.data);
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
    // console.log(itemList);
    
    });
      
    }
    
      }
    const fileupload = (ResData)=>{
      let formData = new FormData();
      formData.append('InsurerQuotationNo', ResData.Data.InsurerQuotationNo);
      formData.append('BindingId', ResData.Data.BindingId);
      formData.append('InsurerId', ResData.Data.InsurerId);
      formData.append('InForceId', ResData.Data.InForceId);
      PolicyReducer.InsurerInfofiles && PolicyReducer.InsurerInfofiles.map((file)=>{
        formData.append('InsurerFiles', file);
        console.log("file",file);
      })
      PolicyReducer.BindingInfofiles && PolicyReducer.BindingInfofiles.map((file)=>{
        formData.append('BindingFiles', file);
        console.log("file",file);
      })
      PolicyReducer.InforceInfofiles && PolicyReducer.InforceInfofiles.map((file)=>{
        formData.append('InForceFiles', file);
        console.log("file",file);
      })
    
      axios.post(`${API_URL}Policy/file-upload`, formData)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(ResData.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/policys");
              }
            });
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
     //console.log(itemList);
    
    });
    
    }
    const onClear = () =>{
    //DataLoad();
}
  
  //   useEffect( ()=> {
  //       console.log("Props",props.location.state);
  //       DataLoad();
    
  // }, []);

//   useEffect(()=> {
    
//     if (submission.SubmissionNo !== "" && submission.SubmissionNo !== "0") 
//     {
//    let AcNo = submission.SubmissionNo;
//     const AcNo1 = {AcNo};
//     console.log(AcNo);
//     axios
//       .post("http://13.126.250.89/BWIAPI/api/Quote/get-submission-Details", AcNo1)
//       .then((res) => {
//         if (res.data.Success) {
//           let respdata = res.data.Data;
//           if (respdata.coveragedata) {
//           setitemList(respdata.coveragedata.CoverageItems);
//           setDefault(respdata.coveragedata.CoverageOtherItem);
//           }
//           //setisFirstloaded(false)
//         }
//       });
      
//     }
//     else {
//       //DataLoad();
//       console.log("Props",props.location.state.id);
//       console.log("Coverages",props.location.state.id.coverages);
//       props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{
//         console.log(data);
//         setitemList(data.coveragedata.CoverageItems);
//         setDefault(data.coveragedata.CoverageOtherItem);
//       });
//     }

//   },[]);

const BuildingLoad = () => {
    const urlstr = API_URL + 'Building/get-all-buildingInfos/' + Account.AcNo;
    console.log(urlstr);
    axios.get(urlstr)
      .then(res => {
        if(res.data.Success){ 
               let itemLists = res.data.Data;
               
               //let list = JSON.stringify(classDetails)
               //let listItem = JSON.parse(list)
            //    SetAddLocation(itemLists.ActiveLocation);
            //    SetdeleteLocation(itemLists.DeleteLocation);
               //alert.success(res.data.Message);
               //const [building, setbuilding] = useState([{ buildingId: "", location: "" }]);
               let buldingdata = [];
               res.data.Data.ActiveLocation && res.data.Data.ActiveLocation.map((data)=>{
                console.log("data",data);
                  //let Location =  data.Street + "," + data.City + "," + data.PostalCode;
                  buldingdata.push({buildingId: data.Id,location: data.Street + "," + data.City + "," + data.PostalCode});
                //setbuilding([ ...building,{buildingId: data.Id,location: data.Street + "," + data.City + "," + data.PostalCode}]);
               });
               setbuilding(buldingdata);
               console.log(res.data.Data);

       }

    });
  }

useEffect(() => {
   

    dispatch(fetchTimeLine(timeline));


    console.log("TimeLine dynamic",timeline);
 
  
}, [timeline]);

  useEffect(()=>{
    console.log("Props",props.location.state);
    console.log("Coverages",props.location.state.id.coverages);
    props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{
      console.log(data);
      setitemList(data.coveragedata[0].CoverageItems);
      setDefault(data.coveragedata[0].CoverageOtherItem);
      data.coveragedata[0].CoverageItems && data.coveragedata[0].CoverageItems.map((item)=>{
        if (item.ItemId === "d4edafea-5b80-483a-8d3e-9e3f59a3e07c") {
            console.log("selectdata",item.ChildItems[0].ValData);
            setselected(item.ChildItems[0].ValData);
        }

    });
    });

    BuildingLoad();
   // DataLoad();
  },[]);

  useEffect(()=>{
    console.log("SelectedItem",selected);
  },[selected])

  var tableStyle = {
    "background": "#2e3f50",
    "color": "#fff",
    "text-transform": "capitalize",
 };
 var BorderStyle = {
    "border-right":"1px solid #333"
  };
  return (
     <div>
    <h1>{props.location.state.id.policyType}</h1>
    <div class="table">
      <table>
        <thead>

          <th style={{width: '40%'}}>Course of Construction Insurance</th>
          <th style={{width: '20%'}}>Coverage Info</th>
          <th style={{width: '20%'}}>LIMIT</th>
          <th style={{width: '20%'}}>DEDUCTIBLE</th>
          
        </thead>
       <tbody>
     <React.Fragment>
         {/* <tr>
             <td>Coverage Questions</td>
             <td> </td>
             <td> </td>
             <td> </td>
          </tr> */}
    {itemList && itemList.map((x, i) => {

        if (x.GroupName === "") {
          if(x.Limit == 0){
            x.Limit = '';
          }
          if(x.Deductible == 0){
            x.Deductible = '';
          }
        return (
            
               <tr>
                  
                        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                        <td>
                            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                                if (ch.ChildItemId === "adaac325-ea36-4f0a-a27f-097424b47c31" && ch.LineInputType == 5) {
                                return(
                             
                            <FormControl variant="outlined" className="select">
                           <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                           <Select
                           labelId="demo-simple-select-outlined-label"
                           id="demo-simple-select-outlined"
                           name="ValData"  
                           value={ch.ValData}
                           onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                           label=""
                           >
   
                           { applicant && applicant.map((item, index)=>{
                           return(
                           <MenuItem key={index} value={item.Applicant}>{item.Applicant}</MenuItem>)
   
                           })} 
   
                           </Select>
                        </FormControl> );
                                }
                            else if (ch.ChildItemId === "3c0b51b1-d91e-423a-b241-4fdd1eb2d07f" && ch.LineInputType === 5) {
                             return (
                            <FormControl variant="outlined" className="select">
                            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                            <Select
                            labelId="demo-simple-select-outlined-label"
                            id={ch.ChildItemId}
                            name="ValData"  
                            value={ch.ValData}
                            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                            label=""
                            >

                            { ifoptions && ifoptions.map((item, index)=>{
                            return(
                            <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                            })} 

                            </Select>
                            </FormControl>
                        
                        );}
                        else if (ch.LineInputType == 2) {
                            return(
                                <TextField  name="ValData" value={ch.ValData}
                               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
                        );}
                             })}
                        </td>

                    <td style={BorderStyle}>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    
                    // <TextField  name="Limit" value={x.Limit}
                    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
                     
                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
                     }
                      </td>
                    <td >
                    {x.IsDeductible && x.DeductibleInputType == 2 &&
                    
        //             <TextField  name="Deductible" value={x.Deductible}
        // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

        <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />
        
        }
            {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            name="Deductible"  
            value={x.Deductible}
            onChange={e => handleInputChange(e, i)}
            label="Deductible"
            >

            { DeAmount && DeAmount.map((item, index)=>{
              let rval = item.Amount; 
              let val1;                       
              val1=rval.replace(/\D/g,'');
            return(
            <MenuItem key={index} value={val1}>{item.Amount}</MenuItem>)

            })} 

            </Select>
            </FormControl> }

                      </td>

                      </tr>

                );
        }
         

      })}
      {/* {itemList && itemList.map((x, i) => {
if ( selected === "Scheduled") 
        {
            return(
         <tr style="background: #2e3f50;color: #fff;text-transform: capitalize;">
                    <td>Scheduled</td>
                    <td></td>
                    <td></td>
                </tr>
            );
          
}
else if( selected === "Blanket") 
        {
         return(
         <tr style="background: #2e3f50;color: #fff;text-transform: capitalize;">
                    <td>Blanket</td>
                    <td></td>
                    <td></td>
                </tr>
         );
}
})} */}

{selected === "Blanket" && <tr style={tableStyle}>
                    <td>Blanket</td>
                    <td></td>
                    <td></td>
                    <td></td>
</tr> }
{selected === "Scheduled" && <tr style={tableStyle}>
                    <td>Scheduled</td>
                    <td></td>
                    <td></td>
                    <td></td>
</tr> }
{ itemList && itemList.map((x,i)=> {

        if (x.GroupName === "Scheduled" && selected === "Scheduled") 
        {
          if(x.Limit == 0){
            x.Limit = '';
          }
          if(x.Deductible == 0){
            x.Deductible = '';
          }
            
            return (

                
            
                <tr>

                <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                <td>
                    {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                        if (ch.ChildItemId === "7c1e688f-3c6f-4ef3-aa61-3ee6baf2d9b5" && ch.LineInputType == 5) {
                        return(
                     
                    <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id="demo-simple-select-outlined"
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label=""
                   >

                   { building && building.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.buildingId}>{item.location}</MenuItem>)

                   })} 

                   </Select>
                </FormControl> );
                        }
                    else if (ch.ChildItemId === "f0f31f7c-1ea7-494d-be1a-e6b4cf8df0bd" && ch.LineInputType === 5) {
                     return (
                    <FormControl variant="outlined" className="select">
                    <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                    <Select
                    labelId="demo-simple-select-outlined-label"
                    id={ch.ChildItemId}
                    name="ValData"  
                    value={ch.ValData}
                    onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                    label=""
                    >

                    { projecttown && projecttown.map((item, index)=>{
                    return(
                    <MenuItem key={index} value={item.Project}>{item.Project}</MenuItem>)

                    })} 

                    </Select>
                    </FormControl>
                
                );}
                else if (ch.LineInputType == 2) {
                    return(
                        <TextField  name="ValData" value={ch.ValData}
                       onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
                );}
                     })}
                </td>

            <td style={BorderStyle}>
            {x.IsLimit && x.LimitInputType == 2 &&
            
            // <TextField  name="Limit" value={x.Limit}
            //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
             
            <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
             }
              </td>
            <td >
            {x.IsDeductible && x.DeductibleInputType == 2 &&
            
//             <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

}
    {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
    <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
    <Select
    labelId="demo-simple-select-outlined-label"
    id="demo-simple-select-outlined"
    name="Deductible"  
    value={x.Deductible}
    onChange={e => handleInputChange(e, i)}
    label="Deductible"
    >

    { DeAmount && DeAmount.map((item, index)=>{
      let rval = item.Amount; 
      let val1;                       
      val1=rval.replace(/\D/g,'');
      
    return(
    <MenuItem key={index} value={val1}>{item.Amount}</MenuItem>)

    })} 

    </Select>
    </FormControl> }

              </td>

              </tr>
 
                 );
            
        }
    })
}
{ itemList && itemList.map((x,i)=> {

if (x.GroupName === "Blanket" && selected === "Blanket") 
{
  if(x.Limit == 0){
    x.Limit = '';
  }
  if(x.Deductible == 0){
    x.Deductible = '';
  }
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
             if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label=""
            >

            { projecttown && projecttown.map((item, index)=>{
            return(
            <MenuItem key={index} value={item.Project}>{item.Project}</MenuItem>)

            })} 

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
     
    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
  let rval = item.Amount; 
  let val1;                       
  val1=rval.replace(/\D/g,'');
return(
<MenuItem key={index} value={val1}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>

      </tr>

         );
    
}
})
}
      </React.Fragment>
             
        </tbody>
      </table>
    </div>
         <div className="btn-group">

         {props.location.state.total === props.location.state.index + 1 && <Button variant="contained" name="cancel" type="button" onClick={onClear}> 
        CANCEL
        </Button>}

        {props.location.state.total === props.location.state.index + 1 &&<Button variant="contained" name="save" color="primary" type="button" onClick={onSubmission}>

        SAVE
        </Button>}
                </div>
  </div>
  
  
  );


    // return(<h1>{props.location.state.id}</h1>)
}
export default CocCoverage