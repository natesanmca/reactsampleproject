//import React from "react";
import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import RemoveRoundedIcon from '@material-ui/icons/RemoveRounded';
import {useSelector, useDispatch} from "react-redux";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Autocomplete from "@material-ui/lab/Autocomplete"
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import NumberFormat from 'react-number-format';
import {useAlert,positions,Provider as AlertProvider} from 'react-alert'
import { useHistory } from "react-router-dom";
import {
  fetchTimeLine,
  fetchSelectedPolicy,
  fetchCheckState,
} from "../../../../../Redux/Actions/actions";

import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
const API_URL = process.env.REACT_APP_BASE_API_URL;
const CyberCoverage=(props)=>{

console.log('Test Click')

const [operationList, setoperationList] = useState([]);
const [itemList, setitemList] = useState([]);
const [childitemList, setchilditemList] = useState([]);
const [pushlist, setpushlist] = useState([]);
const [Default, setDefault] = useState([{ LineItem: "", Limit: "", Deductible:"" }]);
const Account = useSelector(state=>state.Account);
const submission = useSelector((state) => state.SubMission);
const stepperList = useSelector(state => state.TimeLine);
const[timeline,settimeline] = useState(stepperList.timeline);
const policyId = props.location.state.id.policyTypeId;
const PolicyReducer = useSelector(state => state.PolicyQuote);
let history = useHistory();
const alert = useAlert();


const limitAmount = ([
  {Amount:" "},{Amount:"$ 0"},{Amount:"$ 1,000,000"},{Amount:"$ 2,000,000"},{Amount:"$ 3,000,000"},
  {Amount:"$ 4,000,000"},{Amount:"$ 5,000,000"},{Amount:"$ 6,000,000"},
  {Amount:"$ 7,000,000"},{Amount:"$ 8,000,000"},{Amount:"$ 9,000,000"},
  {Amount:"$10,000,000"}
  ]);
  const DeAmount = ([
    {Amount:" "},  {Amount:"$ 0"},{Amount:"$ 500"},{Amount:"$ 1,000"},{Amount:"$ 2,500"},
  {Amount:"$ 5,000"},{Amount:"$ 10,000"},{Amount:"$ 15,000"}
  ,{Amount:"$ 20,000"},{Amount:"$ 30,000"},{Amount:"$ 40,000"},
  {Amount:"$ 50,000"}
    ]);
   
    const [input, SetInput] = useState({
    perClaimLimit: "",
    eachEmployeeBenifitesLimit: "",
    aggreateLimit: "",
    perClaimDecutable: "",
    eachEmployeeBenifitesDecutable: "",
    aggreateDecutable: "",
    checkedA: false,
    checkedB: false,
    gender1: "",
    });

  const OwnerName = useSelector(state=>state);
  const dispatch = useDispatch();
  // handle input change
  
  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...itemList];
    list[index][name] = value;
    setitemList(list);
    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === policyId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === policyId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == "749dd2ec-2aad-4d2f-babb-cb15bad60a68") {
            tempTitle.coverages[coverageIndex].coveragedata[0].CoverageItems = itemList;
          }
        });

      }

    });
    settimeline(tempTitleList);
    //dispatch(fetchOwnername(operationList))
  };

  const handleInputChangeSub = (event, index,Id) => {
  
    console.log('OperationChages');
    let tempTitleList = [...itemList];
    let isitemAvailable = tempTitleList.find(
        (tempTitle) => tempTitle.ItemId === Id
      );
    //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
      tempTitleList.map((tempTitle) => {
        if (tempTitle.ItemId === Id && isitemAvailable) {
            if (index === 2) {
                tempTitle.ChildItems[index]['ValData'] = event;
            }
            else{
          tempTitle.ChildItems[index][event.target.name] = event.target.value;
            }
          setitemList(tempTitleList);
          console.log(tempTitleList);
        }
    });
  };
  const onSubmission = (event) => {
    if (PolicyReducer.SubMissionType === "Submission") {
    const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        policylist : timeline
       // otherItem : Default

    }

    
    console.log("Request Coverage Test");
    console.log("Request Coverage ",Request);
    console.log("Request String",JSON.stringify(Request));
    //https://localhost:5000/api/Quote/new-policy
    let url = API_URL + 'Quote/new-policy';
    axios.post(url, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(res.data.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/Submissions");
              }
            });
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
 
    // console.log(operationList);
     console.log(itemList);
  
  });
    }
    else if (PolicyReducer.SubMissionType === "InsurerQuote") {

      const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        InsurerQuotationNo : PolicyReducer.QuotationNo,
        policylist : timeline,
        InsurerInfoview : PolicyReducer.InsurerInfo,
        BindingInfoview : PolicyReducer.BindingInfo,
        InforceInfoview : PolicyReducer.InforceInfo
       // otherItem : Default
    
    }
    console.log("Request Quote Coverage Test");
    console.log("Request Quote Coverage ",Request);
    console.log("Request Quote String",JSON.stringify(Request));
    console.log("PolicyReducer",PolicyReducer);
    let url = API_URL + 'Policy/new-policy-quote';
    
    axios.post(url, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            fileupload(res.data);
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
    // console.log(itemList);
    
    });
      
    }
    
      }
    const fileupload = (ResData)=>{
      let formData = new FormData();
      formData.append('InsurerQuotationNo', ResData.Data.InsurerQuotationNo);
      formData.append('BindingId', ResData.Data.BindingId);
      formData.append('InsurerId', ResData.Data.InsurerId);
      formData.append('InForceId', ResData.Data.InForceId);
      PolicyReducer.InsurerInfofiles && PolicyReducer.InsurerInfofiles.map((file)=>{
        formData.append('InsurerFiles', file);
        console.log("file",file);
      })
      PolicyReducer.BindingInfofiles && PolicyReducer.BindingInfofiles.map((file)=>{
        formData.append('BindingFiles', file);
        console.log("file",file);
      })
      PolicyReducer.InforceInfofiles && PolicyReducer.InforceInfofiles.map((file)=>{
        formData.append('InForceFiles', file);
        console.log("file",file);
      })
    
      axios.post(`${API_URL}Policy/file-upload`, formData)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(ResData.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/policys");
              }
            });
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
     //console.log(itemList);
    
    });
    
    }
  const onClear = (event) =>{
    DataLoad();
}
  const DataLoad = () => {
    const subVal = {
        "ID": "d5a73263-f027-42a6-81ea-ea9bfe9c9e1e"
      };
    console.log(subVal);
    axios.post(`${API_URL}Quote/get-coverageItem`, subVal)
      .then(res => {
        if(res.data.Success){ 
               let itemLists = res.data.Data;
               //let list = JSON.stringify(classDetails)
               //let listItem = JSON.parse(list)
               if (res.data.Data.length > 0) {
                setitemList(itemLists);
               }
               console.log(res.data.Data);
       }
      // console.log(operationList);
       console.log(itemList);
    
    });
  }
  //   useEffect( ()=> {
  //       console.log("Props",props.location.state);
  //       DataLoad();
    
  // }, []);
  useEffect(() => {
   

     dispatch(fetchTimeLine(timeline));


     console.log("TimeLine dynamic",timeline);
  
   
}, [timeline]);

  useEffect(()=> {
  
     console.log("Props",props.location.state);
     console.log("Coverages",props.location.state.id.coverages);
      props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{
        console.log(data);
        setitemList(data.coveragedata[0].CoverageItems);
        setDefault(data.coveragedata[0].CoverageOtherItem);
      });
      
    // }

  },[]);


  return (
     <div>
    <h1>{props.location.state.id.policyType}</h1>
    <div class="table">
      <table>
        <thead>
          <th>Cyber Coverage</th>

          <th>LIMIT</th>
          <th>DEDUCTIBLE</th>
          <th></th>
        </thead>
       <tbody>
     <React.Fragment>
    {itemList && itemList.map((x, i) => {
        return (
            
               <tr>
                  <td>
                    {x.LineInputType == 1 && <label> {x.LineItem} </label>}
                    {x.LineInputType == 3 && <FormControlLabel
                        control={<Checkbox  checked={x.IsChecked} name="IsUSR" />}
                        label = {x.LineItem}
                        />}
                    
                    </td>
                    <td>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    
                    // <TextField  name="Limit" value={x.Limit}
                    // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
                    
                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
                    }
                      {x.IsLimit && x.LimitInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Limit</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="Limit"  
                        value={x.Limit}
                        onChange={e => handleInputChange(e, i)}
                        label="Limit"
                        >

                        { limitAmount && limitAmount.map((item, index)=>{
                          let rval = item.Amount; 
                          let val1;                       
                          val1=rval.replace(/\D/g,'');
                        return(
                        <MenuItem key={index} value={val1}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                     
                    
                        
                    {/* <TextField  name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                    <td>
                     {x.IsDeductible && x.DeductibleInputType == 2 &&
                     
                    //  <TextField  name="Deductible" value={x.Deductible}
                    // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />
                    
                    }
                      {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
                        <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlinedDeductible"
                        name="Deductible"  
                        value={x.Deductible}
                        onChange={e => handleInputChange(e, i)}
                        label="Deductible"
                        >

                        { DeAmount && DeAmount.map((item, index)=>{
                           let rval = item.Amount; 
                           let val1;                       
                           val1=rval.replace(/\D/g,'');
                        return(
                        <MenuItem key={index} value={val1}>{item.Amount}</MenuItem>)

                        })} 

                        </Select>
                      </FormControl> }
                      </td>
                      <td></td>
                      </tr>

                        );
    
         

      })}


      </React.Fragment>
             
        </tbody>
      </table>
    </div>
         <div className="btn-group">

       {props.location.state.total === props.location.state.index + 1 && <Button variant="contained" name="cancel" type="button" onClick={onClear}> 
        CANCEL
        </Button>}

        {props.location.state.total === props.location.state.index + 1 &&<Button variant="contained" name="save" color="primary" type="button" onClick={onSubmission}>

        SAVE
        </Button>}
                </div>
  </div>
  
  
  );


    // return(<h1>{props.location.state.id}</h1>)
}
export default CyberCoverage