//import React from "react";
import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {useSelector, useDispatch} from "react-redux";
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import 'date-fns';
import {useAlert} from 'react-alert'
import { useHistory } from "react-router-dom";
import NumberFormat from 'react-number-format';
import {
    fetchTimeLine,
    fetchSelectedPolicy,
    fetchCheckState,
  } from "../../../../../Redux/Actions/actions";
  const API_URL = process.env.REACT_APP_BASE_API_URL;
const WrapUp=(props)=>{

console.log('Test Click')
console.log('Wrapup Cliock',props)


const [operationList, setoperationList] = useState([]);
const [itemList, setitemList] = useState([]);
const [] = useState([]);
const [] = useState([]);
const [Default, setDefault] = useState([{ LineItem: "", Limit: "", Deductible:"" }]);
const Account = useSelector(state=>state.Account);
const submission = useSelector((state) => state.SubMission);
const stepperList = useSelector(state => state.TimeLine);
const[timeline,settimeline] = useState(stepperList.timeline);
const policyId = props.location.state.id.policyTypeId;
const PolicyReducer = useSelector(state => state.PolicyQuote);
let history = useHistory();
const alert = useAlert();
const dispatch = useDispatch();

const period = ([
  {Period:"12 months"},{Period:"24 months"},{Period:"36 months"}
  ]);
   
   // handle input change
  
  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...itemList];
    let rval = value;

    if(e.target.className == 'numberft'){
      rval=value.replace(/\D/g,'');
    }
    list[index][name] = rval;
    setitemList(list);
    //dispatch(fetchOwnername(operationList))
    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === policyId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === policyId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == "d7ca123e-a0ed-4913-9dae-f69aec696e9b") {
            tempTitle.coverages[coverageIndex].coveragedata[0].CoverageItems = itemList;
          }
        });

      }

    });
    settimeline(tempTitleList);
  };

  const handleInputChangeSub = (event, index,Id) => {
  
    console.log('OperationChages');
    let tempTitleList = [...itemList];
    let isitemAvailable = tempTitleList.find(
        (tempTitle) => tempTitle.ItemId === Id
      );
    //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
      tempTitleList.map((tempTitle) => {
        if (tempTitle.ItemId === Id && isitemAvailable) {
            if (index === 2) {
                tempTitle.ChildItems[index]['ValData'] = event;
            }
            else{
          tempTitle.ChildItems[index][event.target.name] = event.target.value;
            }
          setitemList(tempTitleList);
          console.log(tempTitleList);
        }
    });
  };

  // handle click event of the Remove button

  // handle click event of the Add button
  const onSubmission = (event) => {
    if (PolicyReducer.SubMissionType === "Submission") {
    const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        policylist : timeline
       // otherItem : Default

    }

    
    console.log("Request Coverage Test");
    console.log("Request Coverage ",Request);
    console.log("Request String",JSON.stringify(Request));
    //https://localhost:5000/api/Quote/new-policy
    axios.post(`${API_URL}Quote/new-policy`, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(res.data.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/Submissions");
              }
            });
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
 
    // console.log(operationList);
     console.log(itemList);
  
  });
    }
    else if (PolicyReducer.SubMissionType === "InsurerQuote") {

      const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        InsurerQuotationNo : PolicyReducer.QuotationNo,
        policylist : timeline,
        InsurerInfoview : PolicyReducer.InsurerInfo,
        BindingInfoview : PolicyReducer.BindingInfo,
        InforceInfoview : PolicyReducer.InforceInfo
       // otherItem : Default
    
    }
    console.log("Request Quote Coverage Test777");
    console.log("Request Quote Coverage ",Request);
    console.log("Request Quote String",JSON.stringify(Request));
    console.log("PolicyReducer",PolicyReducer);
    let url = API_URL + 'Policy/new-policy-quote';
    
    axios.post(url, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            fileupload(res.data);
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
    // console.log(itemList);
    
    });
      
    }
    
      }
    const fileupload = (ResData)=>{
      let formData = new FormData();
      formData.append('InsurerQuotationNo', ResData.Data.InsurerQuotationNo);
      formData.append('BindingId', ResData.Data.BindingId);
      formData.append('InsurerId', ResData.Data.InsurerId);
      formData.append('InForceId', ResData.Data.InForceId);
      PolicyReducer.InsurerInfofiles && PolicyReducer.InsurerInfofiles.map((file)=>{
        formData.append('InsurerFiles', file);
        console.log("file",file);
      })
      PolicyReducer.BindingInfofiles && PolicyReducer.BindingInfofiles.map((file)=>{
        formData.append('BindingFiles', file);
        console.log("file",file);
      })
      PolicyReducer.InforceInfofiles && PolicyReducer.InforceInfofiles.map((file)=>{
        formData.append('InForceFiles', file);
        console.log("file",file);
      })
    
      axios.post(`${API_URL}Policy/file-upload`, formData)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(ResData.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/policys");
              }
            });
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
     //console.log(itemList);
    
    });
    
    }

    const onClear = () =>{
    DataLoad();
}
  const DataLoad = () => {
    const subVal = {
        "ID": "d7ca123e-a0ed-4913-9dae-f69aec696e9b"
      };
    console.log(subVal);
    axios.post(`${API_URL}Quote/get-coverageItem`, subVal)
      .then(res => {
        if(res.data.Success){ 
               let itemLists = res.data.Data;
               //let list = JSON.stringify(classDetails)
               //let listItem = JSON.parse(list)
               if (res.data.Data.length > 0) {
                setitemList(itemLists);
               }
               console.log(res.data.Data);
       }
      // console.log(operationList);
       console.log(itemList);
    
    });
  }
  //   useEffect( ()=> {
  //       console.log("Props",props.location.state);
  //       DataLoad();
    
  // }, []);

//   useEffect(()=> {
    
//     if (submission.SubmissionNo !== "" && submission.SubmissionNo !== "0") 
//     {
//    let AcNo = submission.SubmissionNo;
//     const AcNo1 = {AcNo};
//     console.log(AcNo);
//     axios
//       .post("http://13.126.250.89/BWIAPI/api/Quote/get-submission-Details", AcNo1)
//       .then((res) => {
//         if (res.data.Success) {
//           let respdata = res.data.Data;
//           if (respdata.coveragedata) {
//           setitemList(respdata.coveragedata.CoverageItems);
//           setDefault(respdata.coveragedata.CoverageOtherItem);
//           }
//           //setisFirstloaded(false)
//         }
//       });
      
//     }
//     else {
//       //DataLoad();
//       console.log("Props",props.location.state.id);
//       console.log("Coverages",props.location.state.id.coverages);
//       props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{
//         console.log(data);
//         setitemList(data.coveragedata.CoverageItems);
//         setDefault(data.coveragedata.CoverageOtherItem);
//       });
//     }

//   },[]);
useEffect(() => {
   

    dispatch(fetchTimeLine(timeline));


    console.log("TimeLine dynamic",timeline);
 
  
}, [timeline]);
  useEffect(()=>{
    console.log("Props",props.location.state);
    console.log("Coverages",props.location.state.id.coverages);
    props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{
      console.log(data);
      setitemList(data.coveragedata[0].CoverageItems);
      setDefault(data.coveragedata[0].CoverageOtherItem);
    });
   // DataLoad();
  },[]);

  var tableStyle = {
    "background": "#2e3f50",
    "color": "#fff",
    "text-transform": "capitalize",
 };
 var BorderStyle = {
    "border-right":"1px solid #333"
  };
  return (
     <div>
    <h1>{props.location.state.id.policyType}</h1>
    <div class="table">
      <table>
        <thead>
          <th style={{width: '40%'}}>Wrap-Up Liability Insurance</th>
          <th style={{width: '20%'}}>Coverage Info</th>
          <th style={{width: '20%'}}>LIMIT</th>
          <th style={{width: '20%'}}>DEDUCTIBLE</th>
        </thead>
       <tbody>
     <React.Fragment>
         <tr>
             <td>Coverage Questions</td>
             <td> </td>
             <td style={BorderStyle}> </td>
             <td> </td>
          </tr>
    {itemList && itemList.map((x, i) => {

        if (x.ItemId === "ad5991fa-943f-4f73-8452-98439187931c" || x.ItemId === "fb00e72d-c1d3-4f8a-a8f7-853c46334603") {
          if(x.Limit == 0){
            x.Limit = '';
          }
          if(x.Deductible == 0){
            x.Deductible = '';
          }
        return (
            
               <tr>
                  
                        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                        <td>
                            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                                return(
                            ch.LineInputType == 5 && <FormControl variant="outlined" className="select">
                           <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                           <Select
                           labelId="demo-simple-select-outlined-label"
                           id="demo-simple-select-outlined"
                           name="ValData"  
                           value={ch.ValData}
                           onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                           label=""
                           >
   
                           { period && period.map((item, index)=>{
                           return(
                           <MenuItem key={index} value={item.Period}>{item.Period}</MenuItem>)
   
                           })} 
   
                           </Select>
                        </FormControl> );  
                             })}
                        </td>

                    <td style={BorderStyle}>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    
                    // <TextField  name="Limit" value={x.Limit}
                    // onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    
                    }

                      </td>
                     
                    
                        
                    {/* <TextField  name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                    <td>
                     {x.IsDeductible && x.DeductibleInputType == 2 &&
                     
                    //  <TextField  name="Deductible" value={x.Deductible}
                    // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
                    
                    }

                      </td>

                      </tr>

                );
        }
         

      })}

{ itemList && itemList.map((x,i)=> {

        if (x.ItemId !== "ad5991fa-943f-4f73-8452-98439187931c" && x.ItemId !== "fb00e72d-c1d3-4f8a-a8f7-853c46334603") 
        {
            return (
            
                <tr>
                   
                         <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                         <td>
                             {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                                 return(
                                     ch.LineInputType == 2 &&<TextField  name="ValData" value={ch.ValData}
                                    onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
                             );  
                              })}
                         </td>
                         
                     
                     <td style={BorderStyle}>
                     {x.IsLimit && x.LimitInputType == 2 &&
                     
                    //  <TextField  name="Limit" value={x.Limit}
                    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
                     
                     }
 
                       </td>
                      
                     
                         
                     {/* <TextField  name="Deductible" value={x.Deductible} 
                     onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" /> */}
                     <td>
                      {x.IsDeductible && x.DeductibleInputType == 2 &&
                      
                    //   <TextField  name="Deductible" value={x.Deductible}
                    //  onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

                    <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'}/>
                     
                     }
 
                       </td>
 
                       </tr>
 
                 );
            
        }
    })
}
      </React.Fragment>
             
        </tbody>
      </table>
    </div>
         <div className="btn-group">

         {props.location.state.total === props.location.state.index + 1 && <Button variant="contained" name="cancel" type="button" onClick={onClear}> 
        CANCEL
        </Button>}

        {props.location.state.total === props.location.state.index + 1 &&<Button variant="contained" name="save" color="primary" type="button" onClick={onSubmission}>

        SAVE
        </Button>}
                </div>
  </div>
  
  
  );


    // return(<h1>{props.location.state.id}</h1>)
}
export default WrapUp