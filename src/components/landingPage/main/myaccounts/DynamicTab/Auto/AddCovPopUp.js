import React,{ useReducer,useState,useEffect} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";


const AddCoveragePop=(props)=>{
    return(
       <React.Fragment>
        <Dialog
              open={openCoverage}
              onClose={handleClose}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">
                View coverage by Vehicle Type{" "}
                <CloseIcon
                  className="closeIcon"
                  onClick={handleClose}
                ></CloseIcon>
              </DialogTitle>
              <DialogContent className="addVehicle">
                <Grid container spacing={3}>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">BASIC DETAILS</h6>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <label>Vehicle Type</label>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      className={classes.formControl}
                    >
                      <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="CoverageVehicleType"
                        value={coverageList.CoverageVehicleType}
                        onChange={CoverageChange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">PHYSICAL DAMAGE:</h6>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <label>Physicall Damage Limit</label>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      className={classes.formControl}
                    >
                      <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="PhysicallDamageLimit"
                        value={coverageList.PhysicallDamageLimit}
                        onChange={CoverageChange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <label>Physicall Damage Deductable</label>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      className={classes.formControl}
                    >
                      <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="physicallDamageDeductable"
                        value={coverageList.physicallDamageDeductable}
                        onChange={CoverageChange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={12}>
                    <hr class="line" />
                  </Grid>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">TRANSPORTATION REPLACEMENT:</h6>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <label>Transportation Replacement</label>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      className={classes.formControl}
                    >
                      <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="transportationReplacement"
                        value={coverageList.transportationReplacement}
                        onChange={CoverageChange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={12}>
                    <hr class="line" />
                  </Grid>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">WAIVER OF DEPRECIATION:</h6>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <label>Waiver of Depreciation</label>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      className={classes.formControl}
                    >
                      <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="waiverOfDepreciation"
                        value={coverageList.waiverOfDepreciation}
                        onChange={CoverageChange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </DialogContent>

              <DialogActions>
                <div className="btn-group">
                  <Button
                    variant="contained"
                    name="cancel"
                    type="button"
                    onClick={handleClose}
                  >
                    CANCEL
                  </Button>

                  <Button
                    variant="contained"
                    name="save"
                    color="primary"
                    type="button"
                    onClick={addCoverageList}
                  >
                    SAVE
                  </Button>
                </div>
              </DialogActions>
            </Dialog>
       </React.Fragment> 
       
    )
}

export default AddCoveragePop