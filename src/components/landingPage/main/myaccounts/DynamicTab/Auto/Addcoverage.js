import React,{ useReducer,useState,useEffect} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const AddCoverage=(props)=>{
    return(
       <React.Fragment>
          
           <table>
               <thead>
                   <th>Type</th>
                   <th>Nick Name</th>
                   <th>Physical Damage Limit</th>
                   <th>Physical Damage Deductable</th>
                   <th>Actions</th>
               </thead>
               <tbody>
               {props.coveragelists && props.coveragelists.map((data, index) =>
               <tr key={data.CoverageVehicleId}>
                        <td>{data.CoverageVehicleType}</td>
                        <td>{data.NickName}</td>
                        <td>{data.PhysicallDamage}</td>
                        <td>{data.physicallDamageDeductable}</td>
                        <td className="actionItems">                        
                            <span><EditIcon onClick={() => props.editList(data)}></EditIcon></span>
                            <span><DeleteIcon onClick ={()=>props.deleteList(data)}></DeleteIcon></span>
                        </td>
                   </tr>)}
                 
               </tbody>
           </table>
       </React.Fragment> 
       
    )
}

export default AddCoverage