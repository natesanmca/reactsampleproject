import React,{ useReducer,useState,useEffect} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const AddIRCAVehicle=(props)=>{
    return(
       <React.Fragment>
          
           <table>
               <thead>
                 <th>Serial Number</th>
                   <th>Year</th>
                   <th>Vehicle Make</th>
                   <th>Model</th>
                  
                   <th>Vehicle Class</th>
                   <th>Type</th>
                   <th>VIN</th>
                   <th>Actions</th>
               </thead>
               <tbody>
               {props.vehiclelists && props.vehiclelists.map((data, index) =>
               <tr key={data.VehicleId}>
                         <td> {index + 1} </td>
                        <td>{data.VehicleYear}</td>
                        <td>{data.VehicleMake}</td>
                        <td>{data.VehicleModel}</td>
                       
                        <td>{data.VehicleClass}</td>
                        <td>{data.VehicleType}</td>
                        <td>{data.VehicleIdentificationNumber}</td>
                        <td className="actionItems">                        
                            <span><EditIcon onClick={() => props.editList(data)}></EditIcon></span>
                            <span><DeleteIcon onClick ={()=>props.deleteList(data)}></DeleteIcon></span>
                        </td>
                   </tr>)}
                 
               </tbody>
           </table>
       </React.Fragment> 
       
    )
}

export default AddIRCAVehicle