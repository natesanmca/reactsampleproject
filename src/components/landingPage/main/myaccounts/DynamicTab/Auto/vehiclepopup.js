import React,{ useReducer,useState,useEffect} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import CloseIcon from "@material-ui/icons/Close";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import { useSelector, useDispatch } from "react-redux";

const AddVehiclePop=(props)=>{

  const List = useSelector((state) => state.TimeLine);
  const[CoverList,setCoverList] = React.useState(List.coverlist);

  // const [VehicleType,setVehicleType] = useState([{Type:"private passenger"},{Type:"commercial light"},{Type:"commercial heavy"},
  // {Type:"Trailer"},{Type:"Recreational"},{Type:"Motorcycle"}]);

// useEffect(()=>{
//   console.log("Vehicle Cover List",CoverList);
//   let list = CoverList;
//   console.log("Vehicle Cover List2",list);
//   console.log("Vehicle Cover List length",list.length);
//   list.map((x)=>{
//     console.log("Type Name",x.NickName);
//     //setVehicleType([...VehicleType,{Type: x.NickName}]);
//     VehicleType.push({Type: x.NickName});
//   });

//   console.log("Vehicle Type List",VehicleType);
// },[List.coverlist]);
    return(
       <React.Fragment>
 <Dialog
              open={props.openState}
              onClose={props.close}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">
                Add Vehicle{" "}
                <CloseIcon
                  className="closeIcon"
                  onClick={props.close}
                ></CloseIcon>
              </DialogTitle>
              <DialogContent className="addVehicle">
                <Grid container spacing={3}>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">BASIC DETAILS</h6>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                        Vehicle Type
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="vehiclVehicleTypeeType"
                        name="VehicleType"
                        value={props.data.VehicleType}
                        onChange={props.onchange}
                        label="Vehicle Type"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { CoverList && CoverList.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.Type}>{item.Type}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleYear"
                      name="VehicleYear"
                      value={props.data.VehicleYear}
                      onChange={props.onchange}
                      label="Vehicle Year"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleMake"
                      name="VehicleMake"
                      value={props.data.VehicleMake}
                      onChange={props.onchange}
                      label="Vehicle Make"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleModel"
                      name="VehicleModel"
                      value={props.data.VehicleModel}
                      onChange={props.onchange}
                      label="Vehicle Model"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleIdentificationNumber"
                      name="VehicleIdentificationNumber"
                      value={props.data.VehicleIdentificationNumber}
                      onChange={props.onchange}
                      label="Vehicle Serial Number"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleClass"
                      name="VehicleClass"
                      value={props.data.VehicleClass}
                      onChange={props.onchange}
                      label="Vehicle Class"
                      variant="outlined"
                    />
                  </Grid>
                  
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="ListPriceNew"
                      name="ListPriceNew"
                      value={props.data.ListPriceNew}
                      onChange={props.onchange}
                      label="List Price New"
                      variant="outlined"
                    />
                  </Grid>
                  
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="BodyType"
                      name="BodyType"
                      value={props.data.BodyType}
                      onChange={props.onchange}
                      label="Body Type"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="Weight"
                      name="Weight"
                      value={props.data.Weight}
                      onChange={props.onchange}
                      label="Weight"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">MACHINERY DETAILS</h6>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                        Attachment Machinery
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="Attached_Machinery"
                        name="Attached_Machinery"
                        value={props.data.Attached_Machinery}
                        onChange={props.onchange}
                        label="Attachment Machinery"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value="Yes">Yes</MenuItem>
                        <MenuItem value="No">No</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="outlined-basic"
                      name="Attached_Machinery_Value"
                      value={props.data.Attached_Machinery_Value}
                      onChange={props.onchange}
                      label="value"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={12}>
                    <div class="description">
                      <textarea
                        id="Notes"
                        name="Attached_Machinery_Description"
                        value={props.data.Attached_Machinery_Description}
                        onChange={props.onchange}
                        placeholder="Decription"
                      ></textarea>
                    </div>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                        Owned,Leased or Finaced
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="Owned_Leased_Financed"
                        name="Owned_Leased_Financed"
                        value={props.data.Owned_Leased_Financed}
                        onChange={props.onchange}
                        label="Owned,Leased or Finaced"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value="Owned">Owned</MenuItem>
                        <MenuItem value="Leased">Leased</MenuItem>
                        <MenuItem value="Finaced">Finaced</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </DialogContent>
              <DialogActions>
                <div className="btn-group">
                  <Button
                    variant="contained"
                    name="cancel"
                    type="button"
                    onClick={props.close}
                  >
                    CANCEL
                  </Button>

                  <Button
                    variant="contained"
                    name="save"
                    color="primary"
                    type="button"
                    onClick={props.addList}
                  >
                    SAVE
                  </Button>
                </div>
              </DialogActions>
            </Dialog>       
            
       </React.Fragment> 
       
    )
}

export default AddVehiclePop