import React, { useReducer, useState,useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import CloseIcon from "@material-ui/icons/Close";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import AddCoverage from "./Addcoverage";
import AddCoveragePop from "./coveragepopup";
import AddVehiclePop from "./vehiclepopup";
import AddVehicle from "./AddVehicle";
import AddDriverPop from "./driverpopup"
import AddDriver from "./Adddriver"
import { useSelector, useDispatch } from "react-redux";
import { DataGrid, Column, Editing, Scrolling, Lookup, Summary, TotalItem,SearchPanel } from 'devextreme-react/data-grid';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { v4 as uuidv4 } from 'uuid';
import { useHistory } from "react-router-dom";
import {useAlert} from 'react-alert'
import axios from 'axios';
import {
    fetchTimeLine,fetchCoverList
  } from "../../../../../../Redux/Actions/actions";

  const API_URL = process.env.REACT_APP_BASE_API_URL;

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

const AutoFleet = (props) => {
  const classes = useStyles();
  const [BirthDate, setBirthDate] = React.useState(new Date());
  const [open, setOpen] = React.useState(false);
  const [openDriver, setOpenDriver] = React.useState(false);
  const [openCoverage, setOpenCoverage] = React.useState(false);
  const List = useSelector((state) => state.TimeLine);
  const[timeline,settimeline] = React.useState(List.timeline);
  const PolicyReducer = useSelector(state => state.PolicyQuote);
  const[selectedBenifits,setselectedBenifits] = useState("");
const Account = useSelector(state=>state.Account);
const submission = useSelector((state) => state.SubMission);
  const [itemList, setitemList] = useState([]);

  let history = useHistory();
  const alert = useAlert();
  const dispatch = useDispatch();

  const handleBirthDate = (date) => {
    setBirthDate(date);
    SetDriverList(driverList => ({
      ...driverList, 
      ['BirthDate']: date
  }));
    console.log("efate",BirthDate);
  };

  const AutoLiabilityLimit = ([
    {Limit:"$ 1,000,000"},{Limit:"$ 2,000,000"},{Limit:"$ 3,000,000"},
    {Limit:"$ 4,000,000"},{Limit:"$ 5,000,000"},{Limit:"$ 6,000,000"},
    {Limit:"$ 7,000,000"},{Limit:"$ 8,000,000"},{Limit:"$ 9,000,000"},
    {Limit:"$ 10,000,000"}
    ]);
    const FamilyProtection = ([
      {Limit:"$ 1,000,000"},{Limit:"$ 2,000,000"},{Limit:"$ 3,000,000"},
      {Limit:"$ 4,000,000"},{Limit:"$ 5,000,000"}
      ]);
  
    const AccidentBenefits = ([
      {ABen:"Standard"},{ABen:"Enhanced"}
      ]);

      const IAmounts = ([
        {IAmount:"$600"},{IAmount:"$800"},{IAmount:"$1000"}
   ]);
       const MAmounts = ([
        {MAmount:"$130000"},{MAmount:"$1000000"}
   ]);
   const ifoptions = ([
    {section:"Yes"},{section:"No"}
    ]);

    

  const addVechile = (event) => {
    event.stopPropagation();
    setOpen(true);
  };
  const addDriver = (event) => {
    event.stopPropagation();
    setOpenDriver(true);
  };
  const addCoverage = (event) => {
    event.stopPropagation();
    setOpenCoverage(true);
    SetCoverageList(emptyState);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenDriver(false);
    setOpenCoverage(false);
  };
  const handleChange = (evt) => {
    const name = evt.target.name;
    let newValue = evt.target.value;
    setVehicleInput({ [name]: newValue });
  };

  const handleInputChangeSub = (event, index,Id) => {
  
    console.log('OperationChages');
    let tempTitleList = [...itemList];
    let isitemAvailable = tempTitleList.find(
        (tempTitle) => tempTitle.ItemId === Id
      );
    //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
      tempTitleList.map((tempTitle) => {
        if (tempTitle.ItemId === Id && isitemAvailable) {
            
          tempTitle.ChildItems[index][event.target.name] = event.target.value;
            if (tempTitle.ChildItems[index].ChildItemId === "b6aee73a-504e-4f02-9a95-c1238bfd6ba8") {
                setselectedBenifits(event.target.value);
            }
           
          setitemList(tempTitleList);
          console.log(tempTitleList);
        }
    });

    let tempTitleList1 = [...timeline];

    let isPolicyTypeAvailable = tempTitleList1.find(
      (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
    );
    tempTitleList1.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === props.policyTypeId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == props.coverages.coverageId) {
            tempTitle.coverages[coverageIndex].coveragedata[0].CoverageItems = itemList;
          }
        });

      }

    });

    settimeline(tempTitleList1);
   

  };

  const [vehicleInput, setVehicleInput] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
     VehicleDetails: [],
     DriverDetails:[],
      autoLiabilityLimit: "",
      familyProtection: "",
      accidentBenifits: "",
      incomeReplacementBenifit: "",
      medicalRehabilitation: "",
      attendantCare: "",
      deathFuneral: "",
      indexation: "",
      depedantCare: "",
      CovByvehicleTypeList: [],
    }
  );
  const VehicleChange = (evt) => {
    const name = evt.target.name;
    let newValue = evt.target.value;
    SetVehicleList({ [name]: newValue });
  };
  const DriverChange = (evt) => {
    const name = evt.target.name;
    let newValue = evt.target.value;
    SetDriverList({ [name]: newValue });
  };
  const CoverageChange = (evt) => {
    const name = evt.target.name;
    let newValue = evt.target.value;
    SetCoverageList({ [name]: newValue });
  };

  
  const [Vehicles,setVehicles] = useState([]);
  const [VehicleList, SetVehicleList] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      // id: "",
      VehicleId: uuidv4(),
      RegisteredOwner: "",
      VehicleType: "",
      VehicleYear: "",
      VehicleMake: "",
      VehicleModel: "",
      VehicleIdentificationNumber: "",
      VehicleClass: "",
      ListPriceNew: "",
      BodyType: "",
      Weight:"",
      Radius_Operation: "",
      Principal_Driver_Name: "",
      Principal_Driver_Description: "",
      Occational_Driver_Name: "",
      Occational_Driver_Description: "",
      Physical_Damage: "",
      Physical_Damage_Deductible: "",
      Transportation_Replacement: "",
      Waiver_Depreciation: "",
      Owned_Leased_Financed: "",
      Lienholder_Lessor: "",
      Attached_Machinery: "",
      Attached_Machinery_Description: "",
      Attached_Machinery_Value: ""
    }
  );
  const [CovByvehicleTypeList,setCovByvehicleTypeList] = useState([]);
  const [coverageList, SetCoverageList] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      CoverageVehicleId:uuidv4(),
      CoverageVehicleType: "",
      NickName: "",
      PhysicallDamage: "",
      waiverOfDepreciation: "",
      physicallDamageDeductable: "",
      transportationReplacement: "",
    }
  );
  const [DriversList,setDriversList] = useState([]);
  const [driverList, SetDriverList] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      DriverId:uuidv4(),
      driverName: "",
      driverLicenseNo: "",
      BirthDate: BirthDate,
      LicenseYear: "",
      DriverTrain: "",
      Conviction: ""
    
    }
  );
  const emptyDriverState = {
      DriverId:uuidv4(),
      driverName: "",
      driverLicenseNo: "",
      BirthDate: BirthDate,
      LicenseYear: "",
      DriverTrain: "",
      Conviction: ""
  };
  const emptyState = {
    CoverageVehicleId:uuidv4(),
    CoverageVehicleType: "",
    NickName: "",
    PhysicallDamage: "",
    waiverOfDepreciation: "",
    physicallDamageDeductable: "",
    transportationReplacement: "",
  };
  const emptyVehicleState = {
      // id: "",
      VehicleId: uuidv4(),
      RegisteredOwner: "",
      VehicleType: "",
      VehicleYear: "",
      VehicleMake: "",
      VehicleModel: "",
      VehicleIdentificationNumber: "",
      VehicleClass: "",
      ListPriceNew: "",
      BodyType: "",
      Weight:"",
      Radius_Operation: "",
      Principal_Driver_Name: "",
      Principal_Driver_Description: "",
      Occational_Driver_Name: "",
      Occational_Driver_Description: "",
      Physical_Damage: "",
      Physical_Damage_Deductible: "",
      Transportation_Replacement: "",
      Waiver_Depreciation: "",
      Owned_Leased_Financed: "",
      Lienholder_Lessor: "",
      Attached_Machinery: "",
      Attached_Machinery_Description: "",
      Attached_Machinery_Value: ""
      //vehicleOwnedBy: ""
  };

  const [addedCoverageList, setAddedCoverageList] = useState([]);

  const addDriverList = () => {
    setOpenDriver(false);
    // let addedDriverListData = vehicleInput;
    // if (driverList.id) {
    //   addedDriverListData.DriverDetails.map((data) => {
    //     if (data.id === driverList.id) {
    //       Object.assign(data, driverList);
    //     }
    //   });
    // } else {
    //   addedDriverListData.DriverDetails.push({
    //     ...driverList,
    //     id: addedDriverListData.DriverDetails.length + 1,
    //   });
    // }
    // setVehicleInput(addedDriverListData);
    // SetDriverList({ ...emptyDriverState });
    // console.log(addedDriverListData);


    let addedDriversList = DriversList;
    let IsAvailable = addedDriversList.find(
      (coverageData) =>
        coverageData.DriverId === driverList.DriverId);
 
      if (IsAvailable) {
        addedDriversList.map((data) => {
          if (data.DriverId === driverList.DriverId) {
            Object.assign(data, driverList);
          }
        });
        
        
      } else {
        if (driverList.driverName !== "") {
        addedDriversList.push(driverList);
        }
      }  
   
    setDriversList(addedDriversList);
    SetDriverList({ ...emptyDriverState });
    console.log("add Driver List1",addedDriversList);
    console.log("add Driver List2",DriversList);

    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === props.policyTypeId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == props.coverages.coverageId) {
            tempTitle.coverages[coverageIndex].coveragedata[0].Driver = DriversList;
          }
        });

      }

    });

    settimeline(tempTitleList);


  };

  const addCoverageList = () => {
    setOpenCoverage(false);
    // let addedCoverageListData = vehicleInput;
    // if (coverageList.id) {
    //   addedCoverageListData.CovByvehicleTypeList.map((data) => {
    //     if (data.id === coverageList.id) {
    //       Object.assign(data, coverageList);
    //     }
    //   });
    // } else {
    //   addedCoverageListData.CovByvehicleTypeList.push({
    //     ...coverageList,
    //     id: addedCoverageListData.CovByvehicleTypeList.length + 1,
    //   });
    // }
    let addedCovByvehicleTypeList = CovByvehicleTypeList;
    let IsAvailable = addedCovByvehicleTypeList.find(
      (coverageData) =>
        coverageData.CoverageVehicleId === coverageList.CoverageVehicleId);
 
      if (IsAvailable) {
        addedCovByvehicleTypeList.map((data) => {
          if (data.CoverageVehicleId === coverageList.CoverageVehicleId) {
            Object.assign(data, coverageList);
          }
        });
        
        
      } else {
        if (coverageList.CoverageVehicleType !== "") {
        addedCovByvehicleTypeList.push(coverageList);
        }
      }  
   
    setCovByvehicleTypeList(addedCovByvehicleTypeList);
    
    console.log("add coverage List1",addedCovByvehicleTypeList);
    console.log("add coverage List2",CovByvehicleTypeList);

    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === props.policyTypeId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == props.coverages.coverageId) {
            tempTitle.coverages[coverageIndex].coveragedata[0].CoverageVehicleType = CovByvehicleTypeList;
          }
        });

      }

    });

    settimeline(tempTitleList);
    SetCoverageList({ ...emptyState });

    CovByvehicleTypeList && CovByvehicleTypeList.map((data)=>{
      let isAvailable = VehicleType.find(
        (tempTitle) => tempTitle.Type === data.CoverageVehicleType + " + " + data.NickName
      );
  
      if (!isAvailable) {
        setVehicleType([...VehicleType,{Type:data.CoverageVehicleType + " + " + data.NickName}]);
      }
      
    });
  
    dispatch(fetchCoverList(VehicleType));



  };
  const addVehicleList = () => {
    setOpen(false);
    // let addedVehicleListData = vehicleInput;
    // if (VehicleList.id) {
    //   addedVehicleListData.VehicleDetails.map((data) => {
    //     if (data.id === VehicleList.id) {
    //       Object.assign(data, VehicleList);
    //     }
    //   });
    // } else {
    //   addedVehicleListData.VehicleDetails.push({
    //     ...VehicleList,
    //     id: addedVehicleListData.VehicleDetails.length + 1,
    //   });
    // }
    // setVehicleInput(addedVehicleListData);
    // SetVehicleList({ ...emptyVehicleState });
    // console.log(addedVehicleListData);

    let addedvehicleList = Vehicles;
    let IsAvailable = addedvehicleList.find(
      (coverageData) =>
        coverageData.VehicleId === VehicleList.VehicleId);
 
      if (IsAvailable) {
        addedvehicleList.map((data) => {
          if (data.VehicleId === VehicleList.VehicleId) {
            Object.assign(data, VehicleList);
          }
        });
        
        
      } else {
        
        if (VehicleList.VehicleType !== "") {

          addedvehicleList.push(VehicleList);

        }
       
        
      }  
   
    setVehicles(addedvehicleList);
    SetVehicleList({ ...emptyVehicleState });
    console.log("add Vehicle List1",addedvehicleList);
    console.log("add Vehicle List2",Vehicles);

    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === props.policyTypeId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == props.coverages.coverageId) {
            tempTitle.coverages[coverageIndex].coveragedata[0].Vehicle = Vehicles;
          }
        });

      }

    });

    settimeline(tempTitleList);

  };
  const DeleteVehicleList = (data) => {
    let tempTitleList = vehicleInput;
   vehicleInput.VehicleDetails.map((tempTitle, index) => {
       if (tempTitle.id === data.id) {
        tempTitleList.VehicleDetails.splice(index,1);
       }
   });
   SetVehicleList(tempTitleList);
 };
 const DeleteCoverageList = (e) => {
  console.log("event",e);
    
 let tempTitleList = CovByvehicleTypeList;
  tempTitleList.map((data, index) => {
   if (data.CoverageVehicleId === e.row.data.CoverageVehicleId) {
    tempTitleList.splice(index,1);
   }
 });

 setCovByvehicleTypeList(tempTitleList);
};
const DeleteDriverList = (data) => {
  let tempTitleList = vehicleInput;
 vehicleInput.DriverDetails.map((tempTitle, index) => {
     if (tempTitle.id === data.id) {
      tempTitleList.DriverDetails.splice(index,1);
     }
 });
 SetDriverList(tempTitleList);
};
 
  
  const EditList = (data) => {
    setOpenCoverage(true);
    SetCoverageList({ ...data });
  };
  const EditVehicleList = (data) => {
    setOpen(true);
    SetVehicleList({ ...data });
  };
  const EditdriverList = (data) => {
    setOpenDriver(true);
    SetDriverList({ ...data });
  };
  const editCoveragedata=(e)=>{
    console.log("event",e);
    
  let addedCovByvehicleTypeList = CovByvehicleTypeList;
  addedCovByvehicleTypeList.map((data) => {
    if (data.CoverageVehicleId === e.row.data.CoverageVehicleId) {
      setOpenCoverage(true);
SetCoverageList({ ...data });
    }
  });
    
    

  };

  useEffect(() => {
   

    dispatch(fetchTimeLine(timeline));


    console.log("TimeLine dynamic",timeline);
 
  
}, [timeline]);

const TypeData = ([{Type:"private passenger"},{Type:"commercial light"},{Type:"commercial heavy"},
{Type:"Trailer"},{Type:"Recreational"},{Type:"Motorcycle"}]);

// const [VehicleType,setVehicleType] = useState([{Type:"private passenger"},{Type:"commercial light"},{Type:"commercial heavy"},
//     {Type:"Trailer"},{Type:"Recreational"},{Type:"Motorcycle"}]);
    const [VehicleType,setVehicleType] = useState([]);

useEffect(() => {
   

  // dispatch(fetchCoverList(CovByvehicleTypeList));
  //  setVehicleType([...VehicleType,{Type:"private passenger"}]);

  //  TypeData && TypeData.map((data)=>{
  //   setVehicleType([...VehicleType,{Type:data.Type}]);
  // });
  setVehicleType([]);
  CovByvehicleTypeList && CovByvehicleTypeList.map((data)=>{
    let isAvailable = VehicleType.find(
      (tempTitle) => tempTitle.Type === data.CoverageVehicleType + " + " + data.NickName
    );

    if (!isAvailable) {
      setVehicleType([...VehicleType,{Type: data.CoverageVehicleType + " + " + data.NickName}]);
    }
    
  });

  dispatch(fetchCoverList(VehicleType));
  console.log("CovByvehicleTypeList dynamic",VehicleType);


}, [CovByvehicleTypeList]);

  useEffect(()=>{
    console.log("Props Auto",props);
    // console.log("Coverages",props.location.state.id.coverages);
    // props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{
    //   console.log(data);
    if (props.coverages) {
      setitemList(props.coverages.coveragedata[0].CoverageItems);
      setCovByvehicleTypeList(props.coverages.coveragedata[0].CoverageVehicleType)
      setVehicles(props.coverages.coveragedata[0].Vehicle)
      setDriversList(props.coverages.coveragedata[0].Driver)
      //setDefault(props.coverages.coveragedata[0].CoverageOtherItem);
      
      props.coverages.coveragedata[0].CoverageItems && props.coverages.coveragedata[0].CoverageItems.map((x,i)=>{

        if (x.ChildItems[0].ChildItemId === "b6aee73a-504e-4f02-9a95-c1238bfd6ba8") {
          console.log("Val Data",x.ChildItems[0].ValData);
          setselectedBenifits(x.ChildItems[0].ValData);
        }
      });

    }
    dispatch(fetchCoverList(VehicleType));

    // //   data.coveragedata.CoverageItems && data.coveragedata.CoverageItems.map((item)=>{
    // //     if (item.ItemId === "d4edafea-5b80-483a-8d3e-9e3f59a3e07c") {
    // //         console.log("selectdata",item.ChildItems[0].ValData);
    // //         setselected(item.ChildItems[0].ValData);
    // //     }

    // // });
    // });

    
   // DataLoad();
  },[props.coverages]);


   // handle click event of the Remove button

  // handle click event of the Add button
  const onSubmission = (event) => {
    
    console.log("submission Type",PolicyReducer);
    if (PolicyReducer.SubMissionType === "Submission") {
    const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        policylist : timeline
       // otherItem : Default

    }

    
    console.log("Request Coverage Test");
    console.log("Request Coverage ",Request);
    console.log("Request String",JSON.stringify(Request));
    let url = API_URL + 'Quote/new-policy';
    //https://localhost:5000/api/Quote/new-policy
    axios.post(url, Request)
    .then(res => {
         console.log("gold",res)
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(res.data.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/Submissions");
              }
            });
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
 
    // console.log(operationList);
     console.log(itemList);
  
  });
    }
    else if (PolicyReducer.SubMissionType === "InsurerQuote") {

      const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        InsurerQuotationNo : PolicyReducer.QuotationNo,
        policylist : timeline,
        InsurerInfoview : PolicyReducer.InsurerInfo,
        BindingInfoview : PolicyReducer.BindingInfo,
        InforceInfoview : PolicyReducer.InforceInfo
       // otherItem : Default
    
    }
    console.log("gold");
    console.log("Request Quote Coverage Test12221");
    console.log("Request Quote Coverage ",Request);
    console.log("Request Quote String",JSON.stringify(Request));
    console.log("PolicyReducer",PolicyReducer);
    
    let url = API_URL + 'Policy/new-policy-quote';
    
    axios.post(url, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            fileupload(res.data);
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
    // console.log(itemList);
    
    });
      
    }
    
      }
    const fileupload = (ResData)=>{
      let formData = new FormData();
      formData.append('InsurerQuotationNo', ResData.Data.InsurerQuotationNo);
      formData.append('BindingId', ResData.Data.BindingId);
      formData.append('InsurerId', ResData.Data.InsurerId);
      formData.append('InForceId', ResData.Data.InForceId);
      PolicyReducer.InsurerInfofiles && PolicyReducer.InsurerInfofiles.map((file)=>{
        formData.append('InsurerFiles', file);
        console.log("file",file);
      })
      PolicyReducer.BindingInfofiles && PolicyReducer.BindingInfofiles.map((file)=>{
        formData.append('BindingFiles', file);
        console.log("file",file);
      })
      PolicyReducer.InforceInfofiles && PolicyReducer.InforceInfofiles.map((file)=>{
        formData.append('InForceFiles', file);
        console.log("file",file);
      })
    
      axios.post(`${API_URL}Policy/file-upload`, formData)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(ResData.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/policys");
              }
            });
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
     //console.log(itemList);
    
    });
    
    }
    const onClear = () =>{
    //DataLoad();
}


  return (
    <Container maxWidth="">
      <Grid container spacing={3}>
        <Grid item md={12}>
          <div className="mainContent">
           {/* <AddVehiclePop close={handleClose}
              open={addVechile}
              addList={addVehicleList}
              openState={open}
              data={VehicleList}
              onchange={VehicleChange}
              coverlist={VehicleType}>
              </AddVehiclePop> */}
            <AddCoveragePop
              close={handleClose}
              open={addCoverage}
              addList={addCoverageList}
              openState={openCoverage}
              data={coverageList}
              onchange={CoverageChange}
             
            ></AddCoveragePop>
            <AddDriverPop
              close={handleClose}
              open={addDriver}
              addList={addDriverList}
              openState={openDriver}
              data={driverList}
              onchange={DriverChange}
              onDatechange={handleBirthDate}
            ></AddDriverPop>

        

            <div className="accordian">
            
            <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel2a-content"
                  id="panel2a-header"
                >
                  <Typography className={classes.heading}>
                    BLANKET COVERAGE DETAILS
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                    <Grid container spacing={3}>
                      <Grid item md={6}>
                        {/* <Grid container spacing={3}> */}
                        { itemList && itemList.map((x,i)=> {
                         
                         if (x.GroupName === "Blanket Coverage") {
                          return (
                         <Grid container spacing={3} >
                          <Grid item md={6}>
                          <label> {x.LineItem} </label>
                          </Grid>

                          <Grid item md={6}>
                          {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.ChildItemId === "15dc78ff-b8ff-42f0-bc14-92c4bf1f9d89") {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label=""
            >

                            <MenuItem value="">
                                <em>None</em>
                              </MenuItem>
                              { AutoLiabilityLimit && AutoLiabilityLimit.map((item, index)=>{
                          return(
                          <MenuItem key={index} value={item.Limit}>{item.Limit}</MenuItem>)

                          })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.ChildItemId === "75257e69-5d81-452a-a465-c3afe3e617e6") {
          return (
         <FormControl variant="outlined" className="select">
         <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
         <Select
         labelId="demo-simple-select-outlined-label"
         id={ch.ChildItemId}
         name="ValData"  
         value={ch.ValData}
         onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
         label=""
         >

                         <MenuItem value="">
                             <em>None</em>
                           </MenuItem>
                           { FamilyProtection && FamilyProtection.map((item, index)=>{
                           return(
                           <MenuItem key={index} value={item.Limit}>{item.Limit}</MenuItem>)
   
                           })}

         </Select>
         </FormControl>
     
     );}

     else if (ch.ChildItemId === "b6aee73a-504e-4f02-9a95-c1238bfd6ba8") {
      return (
     <FormControl variant="outlined" className="select">
     <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
     <Select
     labelId="demo-simple-select-outlined-label"
     id={ch.ChildItemId}
     name="ValData"  
     value={ch.ValData}
     onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
     label=""
     >

                     <MenuItem value="">
                         <em>None</em>
                       </MenuItem>
                       { AccidentBenefits && AccidentBenefits.map((item, index)=>{
                           return(
                           <MenuItem key={index} value={item.ABen}>{item.ABen}</MenuItem>)
   
                           })}

     </Select>
     </FormControl>
 
 );}
        
             })}
                          </Grid>
                          </Grid>
                          )
                         }                       
                        }) }
                         { selectedBenifits === "Enhanced" && <Grid container spacing={3}> <Grid item md={12}>
                            <h6 class="clrred mb20">Additional Questions</h6>
                      </Grid> </Grid>  }
                      { itemList && itemList.map((x,i)=> {
                         
                         if (x.GroupName === "Accident Benefits" && selectedBenifits === "Enhanced") {
                          return (
                         <Grid container spacing={3}>
                          <Grid item md={6}>
                          <label> {x.LineItem} </label>
                          </Grid>

                          <Grid item md={6}>
                          
                    {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                        if ((ch.ChildItemId === "4d76c42b-6465-4002-8b7e-55a5e7bc8a93" ) && ch.LineInputType == 5) {
                        return(
                     
                    <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id="demo-simple-select-outlined"
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Amount"
                   >

                   { IAmounts && IAmounts.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.IAmount}>{item.IAmount}</MenuItem>)

                   })} 

                   </Select>
                </FormControl> );
                        }
                    else if ((ch.ChildItemId === "36de1a8a-fccf-47ea-a589-6c775bb8d3d5" ) && ch.LineInputType === 5) {
                     return (
                    <FormControl variant="outlined" className="select">
                    <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                    <Select
                    labelId="demo-simple-select-outlined-label"
                    id={ch.ChildItemId}
                    name="ValData"  
                    value={ch.ValData}
                    onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                    label="Amount"
                    >

                    { MAmounts && MAmounts.map((item, index)=>{
                    return(
                    <MenuItem key={index} value={item.MAmount}>{item.MAmount}</MenuItem>)

                    })} 

                    </Select>
                    </FormControl>
                
                );}
                else if (ch.LineInputType === 5) {
                    return (
                   <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id={ch.ChildItemId}
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Select"
                   >

                   { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })} 

                   </Select>
                   </FormControl>
               
               );}
                else if (ch.LineInputType == 2) {
                    return(
                        <TextField  name="ValData" value={ch.ValData}
                       onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
                );}
                     })}
               
                          </Grid>
                          </Grid>

                          )}                       
                        }) }



                       
                        {/* </Grid> */}
                      </Grid>
                    </Grid>
                  </Typography>
                </AccordionDetails>
              </Accordion>

              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel3a-content"
                  id="panel3a-header"
                >
                  <Typography className={classes.heading}>
                    <span>COVERAGE DETAILS BY VEHICLE TYPE</span>{" "}
                    <Button
                      variant="contained"
                      name="ADD VEHICLE"
                      color="primary"
                      type="button"
                      onClick={addCoverage}
                    >
                      ADD COVERAGE <AddIcon></AddIcon>
                    </Button>
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography className="auto-table">

                  {/* <DataGrid
                        id="gridContainer"
                        dataSource={CovByvehicleTypeList}
                        
                        showBorders={true}
                       >

                            <Column dataField="CoverageVehicleType" caption="Type" />
                            <Column dataField="NickName" caption="Nick Name" />
                            <Column dataField="PhysicallDamage"  caption="Physical Damage" />
                            <Column dataField="physicallDamageDeductable" caption="Physical Damage Deductible" />
                            
                            <Column caption="Actions" type="buttons" 
                            
                              buttons={[
                                {
                                hint: 'Click to Edit',
                                icon: 'edit',
                                onClick: (e) => { editCoveragedata(e) }
                               },
                               {
                                hint: 'Click to Delete',
                                icon: 'delete',
                                onClick: (e) => { DeleteCoverageList(e) }
                               }
                               ]} />
                           
                    </DataGrid> */}

                    <AddCoverage
                      coveragelists={CovByvehicleTypeList}
                      editList={(editData) => EditList(editData)}
                      deleteList={(deleteDate)=>DeleteCoverageList(deleteDate)}
                    ></AddCoverage>
                  </Typography>
                </AccordionDetails>
              </Accordion>
              
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography className={classes.heading}>
                    <span>FLEET DETAILS</span>{" "}
                    <Button
                      variant="contained"
                      name="ADD VEHICLE"
                      color="primary"
                      type="button"
                      onClick={addVechile}
                    >
                      ADD VEHICLE <AddIcon></AddIcon>
                    </Button>
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography className="auto-table">
                  <AddVehicle
                      vehiclelists={Vehicles}
                      editList={(editData) => EditVehicleList(editData)}
                      deleteList ={(deleteData) => DeleteVehicleList(deleteData)}
                    ></AddVehicle>
                  </Typography>
                </AccordionDetails>
              </Accordion>
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel2a-content"
                  id="panel2a-header"
                >
                  <Typography className={classes.heading}>
                    <span>DRIVER DETAILS</span>{" "}
                    <Button
                      variant="contained"
                      name="ADD VEHICLE"
                      color="primary"
                      type="button"
                      onClick={addDriver}
                    >
                      ADD DRIVER <AddIcon></AddIcon>
                    </Button>
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography className="auto-table">
                   <AddDriver driverlists={DriversList}
                      editList={(editData) => EditdriverList(editData)}
                      deleteList={(driverData)=>DeleteDriverList(driverData)}></AddDriver>
                  </Typography>
                </AccordionDetails>
              </Accordion>
            </div>

            <Dialog
              open={open}
              onClose={handleClose}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">
                Add Vehicle{" "}
                <CloseIcon
                  className="closeIcon"
                  onClick={handleClose}
                ></CloseIcon>
              </DialogTitle>
              <DialogContent className="addVehicle">
                <Grid container spacing={3}>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">BASIC DETAILS</h6>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                        Vehicle Type
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="vehiclVehicleTypeeType"
                        name="VehicleType"
                        value={VehicleList.VehicleType}
                        onChange={VehicleChange}
                        label="Vehicle Type"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { VehicleType && VehicleType.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.Type}>{item.Type}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleYear"
                      name="VehicleYear"
                      value={VehicleList.VehicleYear}
                      onChange={VehicleChange}
                      label="Vehicle Year"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleMake"
                      name="VehicleMake"
                      value={VehicleList.VehicleMake}
                      onChange={VehicleChange}
                      label="Vehicle Make"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleModel"
                      name="VehicleModel"
                      value={VehicleList.VehicleModel}
                      onChange={VehicleChange}
                      label="Vehicle Model"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleIdentificationNumber"
                      name="VehicleIdentificationNumber"
                      value={VehicleList.VehicleIdentificationNumber}
                      onChange={VehicleChange}
                      label="Vehicle Serial Number"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleClass"
                      name="VehicleClass"
                      value={VehicleList.VehicleClass}
                      onChange={VehicleChange}
                      label="Vehicle Class"
                      variant="outlined"
                    />
                  </Grid>
                  
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="ListPriceNew"
                      name="ListPriceNew"
                      value={VehicleList.ListPriceNew}
                      onChange={VehicleChange}
                      label="List Price New"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="BodyType"
                      name="BodyType"
                      value={VehicleList.BodyType}
                      onChange={VehicleChange}
                      label="Body Type"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="Weight"
                      name="Weight"
                      value={VehicleList.Weight}
                      onChange={VehicleChange}
                      label="Weight"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">MACHINERY DETAILS</h6>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                        Attachment Machinery
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="Attached_Machinery"
                        name="Attached_Machinery"
                        value={VehicleList.Attached_Machinery}
                        onChange={VehicleChange}
                        label="Attachment Machinery"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value="Yes">Yes</MenuItem>
                        <MenuItem value="No">No</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="outlined-basic"
                      name="Attached_Machinery_Value"
                      value={VehicleList.Attached_Machinery_Value}
                      onChange={VehicleChange}
                      label="value"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={12}>
                    <div class="description">
                      <textarea
                        id="Notes"
                        name="Attached_Machinery_Description"
                        value={VehicleList.Attached_Machinery_Description}
                        onChange={VehicleChange}
                        placeholder="Decription"
                      ></textarea>
                    </div>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                        Owned,Leased or Finaced
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="Owned_Leased_Financed"
                        name="Owned_Leased_Financed"
                        value={VehicleList.Owned_Leased_Financed}
                        onChange={VehicleChange}
                        label="Owned,Leased or Finaced"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value="Owned">Owned</MenuItem>
                        <MenuItem value="Leased">Leased</MenuItem>
                        <MenuItem value="Finaced">Finaced</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </DialogContent>
              <DialogActions>
                <div className="btn-group">
                  <Button
                    variant="contained"
                    name="cancel"
                    type="button"
                    onClick={handleClose}
                  >
                    CANCEL
                  </Button>

                  <Button
                    variant="contained"
                    name="save"
                    color="primary"
                    type="button"
                    onClick={addVehicleList}
                  >
                    SAVE
                  </Button>
                </div>
              </DialogActions>
            </Dialog>       

            {/* <div className="btn-group">
                  <Button
                    variant="contained"
                    name="cancel"
                    type="button"
                    
                    
                  >
                    CANCEL
                  </Button>

                  <Button
                    variant="contained"
                    name="save"
                    color="primary"
                    type="button"
                   
                  >
                    SAVE
                  </Button>
                </div> */}
          </div>
        </Grid>
      </Grid>
    </Container>
  );
};

export default AutoFleet;
