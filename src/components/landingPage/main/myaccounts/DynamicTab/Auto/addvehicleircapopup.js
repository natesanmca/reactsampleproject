import React,{ useReducer,useState,useEffect} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import CloseIcon from "@material-ui/icons/Close";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

const AddIRCAVehiclePop=(props)=>{
  const CHARACTER_LIMIT = 17;
  const VehicleType = ([
    {Type:"private passenger"},{Type:"commercial light"},{Type:"commercial heavy"},
    {Type:"Trailer"},{Type:"Recreational"},{Type:"Motorcycle"}
]);

const IAmounts = ([
    {IAmount:"$500"},{IAmount:"$1000"},{IAmount:"$2500"},{IAmount:"$5000"},
    {IAmount:"$7500"},{IAmount:"$10000"}
  ]);
  
  const MAmounts = ([
    {MAmount:"$50/$1000"},{MAmount:"$60/$1200"},{MAmount:"$75/$1500"},
    {MAmount:"$100/$2000"}
  ]);

  const PhysicalDamage = ([
    {Type:"Specified Perils"},{Type:"Comprehensive"},{Type:"Collision"},
    {Type:"All Perils"},{Type:"Comprehensive + Collision Physical Damage"}
  ]);

  const ifoptions = ([
    {section:"Yes"},{section:"No"}
    ]);
    const handleInputChange = (e) => {
     // props.onchange
     let ns;
     ns = e.target.value;
     ns = ns.replace(/([,.€])+/g, '');
    };
    return(
       <React.Fragment>
 <Dialog
              open={props.openState}
              onClose={props.close}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">
                Add Vehicle{" "}
                <CloseIcon
                  className="closeIcon"
                  onClick={props.close}
                ></CloseIcon>
              </DialogTitle>
              <DialogContent className="addVehicle">
                <Grid container spacing={3}>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">BASIC DETAILS</h6>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="RegisteredOwner"
                      name="RegisteredOwner"
                      value={props.data.RegisteredOwner}
                      onChange={props.onchange}
                      label="Registered Owner"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="Radius_Operation"
                      name="Radius_Operation"
                      value={props.data.Radius_Operation}
                      onChange={props.onchange}
                      label="Radius Operation"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                        Vehicle Type
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="vehiclVehicleTypeeType"
                        name="VehicleType"
                        value={props.data.VehicleType}
                        onChange={props.onchange}
                        label="Vehicle Type"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { VehicleType && VehicleType.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.Type}>{item.Type}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleYear"
                      name="VehicleYear"
                      value={props.data.VehicleYear}
                      onChange={props.onchange}
                      label="Vehicle Year"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleMake"
                      name="VehicleMake"
                      value={props.data.VehicleMake}
                      onChange={props.onchange}
                      label="Vehicle Make"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleModel"
                      name="VehicleModel"
                      value={props.data.VehicleModel}
                      onChange={props.onchange}
                      label="Vehicle Model"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleIdentificationNumber"
                      name="VehicleIdentificationNumber"
                      
                      inputProps={{
                        maxlength: CHARACTER_LIMIT
                      }}
                      
                      value={props.data.VehicleIdentificationNumber}
                      onChange={props.onchange}
                      label="Vehicle Serial Number"
                      variant="outlined"
                      
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="VehicleClass"
                      name="VehicleClass"
                      value={props.data.VehicleClass}
                      onChange={props.onchange}
                      label="Vehicle Class"
                      variant="outlined"
                    />
                  </Grid>
                  
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="ListPriceNew"
                      name="ListPriceNew"
                      value={props.data.ListPriceNew}
                      onChange={props.onchange}
                      label="List Price New"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="BodyType"
                      name="BodyType"
                      value={props.data.BodyType}
                      onChange={props.onchange}
                      label="Body Type"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="Weight"
                      name="Weight"
                      value={props.data.Weight}
                     // onChange={props.onchange}
                     onChange={e => handleInputChange(e)}
                      label="Weight"
                      variant="outlined"
                    />
                  </Grid>
                  
                  <Grid item md={12}>
                    <h6 class="clrred mb20">DRIVER DETAILS</h6>
                  </Grid>

                  <Grid item md={12}>
                    <h6 class="clrred mb20">PRINCIPAL DRIVER</h6>
                  </Grid>
                  
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="Principal_Driver_Name"
                      name="Principal_Driver_Name"
                      value={props.data.Principal_Driver_Name}
                      onChange={props.onchange}
                      label="Name"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="Principal_Driver_Description"
                      name="Principal_Driver_Description"
                      value={props.data.Principal_Driver_Description}
                      onChange={props.onchange}
                      label="Drivers Licence Number"
                      variant="outlined"
                    />
                  </Grid>

                  <Grid item md={6}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container justify="space-around" className="datePicker">
        <KeyboardDatePicker
          disableToolbar
          name="Principal_BirthDate"
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="Principal_date-picker-inline"
          label="Birth Date"
          value={props.data.Principal_BirthDate}
          onChange={props.onPdatechange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        </Grid>
        </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={6}>
                    {" "}
                    <TextField
                      id="Principal_LicenseYear"
                      name="Principal_LicenseYear"
                      value={props.data.Principal_LicenseYear}
                      onChange={props.onchange}
                      label="License Year"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                     
                    >
                      <InputLabel id="demo-simple-select-outlined-label"> Driver Training </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="Principal_demo-simple-select-outlined"
                        name="Principal_DriverTrain"
                        value={props.data.Principal_DriverTrain}
                        onChange={props.onchange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={12}>
                    <div class="description">
                      <textarea
                        id="Principal_Attached_Machinery_Description"
                        name="Principal_Conviction"
                        value={props.data.Principal_Conviction}
                        onChange={props.onchange}
                        placeholder="Conviction"
                      ></textarea>
                    </div>
                  </Grid>

                  <Grid item md={12}>
                    <h6 class="clrred mb20">OCCASIONAL DRIVER</h6>
                  </Grid>
                  
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="Occational_Driver_Name"
                      name="Occational_Driver_Name"
                      value={props.data.Occational_Driver_Name}
                      onChange={props.onchange}
                      label="Name"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="Occational_Driver_Description"
                      name="Occational_Driver_Description"
                      value={props.data.Occational_Driver_Description}
                      onChange={props.onchange}
                      label="Drivers Licence Number"
                      variant="outlined"
                    />
                  </Grid>

                  <Grid item md={6}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container justify="space-around" className="datePicker">
        <KeyboardDatePicker
          disableToolbar
          name="Occational_BirthDate"
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="date-picker-inline"
          label="Birth Date"
          value={props.data.Occational_BirthDate}
          onChange={props.onOdatechange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        </Grid>
        </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={6}>
                    {" "}
                    <TextField
                      id="LicenseYear"
                      name="Occational_LicenseYear"
                      value={props.data.Occational_LicenseYear}
                      onChange={props.onchange}
                      label="License Year"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                     
                    >
                      <InputLabel id="demo-simple-select-outlined-label"> Driver Training </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="Occational_DriverTrain"
                        value={props.data.Occational_DriverTrain}
                        onChange={props.onchange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={12}>
                    <div class="description">
                      <textarea
                        id="Attached_Machinery_Description"
                        name="Occational_Conviction"
                        value={props.data.Occational_Conviction}
                        onChange={props.onchange}
                        placeholder="Conviction"
                      ></textarea>
                    </div>
                  </Grid>
                  <Grid item md={12}>
                    <h6 class="clrred mb20"> COVERAGE DETAILS </h6>
                  </Grid>

                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                      Physical Damage
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="Physical_Damage"
                        name="Physical_Damage"
                        value={props.data.Physical_Damage}
                        onChange={props.onchange}
                        label="Physical Damage"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { PhysicalDamage && PhysicalDamage.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.Type}>{item.Type}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>

                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                      Physical Damage Deductible
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="Physical_Damage_Deductible"
                        name="Physical_Damage_Deductible"
                        value={props.data.Physical_Damage_Deductible}
                        onChange={props.onchange}
                        label="Physical Damage Deductible"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { IAmounts && IAmounts.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.IAmount}>{item.IAmount}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>


                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                     
                    >
                      <InputLabel id="demo-simple-select-outlined-label"> Transportation Replacement </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-Transportation_Replacement-outlined"
                        name="Transportation_Replacement"
                        value={props.data.Transportation_Replacement}
                        onChange={props.onchange}
                        label="Transportation Replacement"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { MAmounts && MAmounts.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.MAmount}>{item.MAmount}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>

                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                     
                    >
                      <InputLabel id="demo-simple-select-outlined-label"> Waiver of Depreciation </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="Waiver_Depreciation"
                        name="Waiver_Depreciation"
                        value={props.data.Waiver_Depreciation}
                        onChange={props.onchange}
                        label="Waiver of Depreciation"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>

                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                        Owned,Leased or Finaced
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="Owned_Leased_Financed"
                        name="Owned_Leased_Financed"
                        value={props.data.Owned_Leased_Financed}
                        onChange={props.onchange}
                        label="Owned,Leased or Finaced"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value="Owned">Owned</MenuItem>
                        <MenuItem value="Leased">Leased</MenuItem>
                        <MenuItem value="Finaced">Finaced</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>

                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="outlined-basic"
                      name="Lienholder_Lessor"
                      value={props.data.Lienholder_Lessor}
                      onChange={props.onchange}
                      label="Lienholder or Lessor"
                      variant="outlined"
                    />
                  </Grid>


                  <Grid item md={12}>
                    <h6 class="clrred mb20">MACHINERY DETAILS</h6>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      
                    >
                      <InputLabel id="demo-simple-select-outlined-label">
                        Attachment Machinery
                      </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="Attached_Machinery"
                        name="Attached_Machinery"
                        value={props.data.Attached_Machinery}
                        onChange={props.onchange}
                        label="Attachment Machinery"
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        <MenuItem value="Yes">Yes</MenuItem>
                        <MenuItem value="No">No</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="outlined-basic"
                      name="Attached_Machinery_Value"
                      value={props.data.Attached_Machinery_Value}
                      onChange={props.onchange}
                      label="value"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={12}>
                    <div class="description">
                      <textarea
                        id="Attached_Machinery_Description"
                        name="Attached_Machinery_Description"
                        value={props.data.Attached_Machinery_Description}
                        onChange={props.onchange}
                        placeholder="Decription"
                      ></textarea>
                    </div>
                  </Grid>
                  
                </Grid>
              </DialogContent>
              <DialogActions>
                <div className="btn-group">
                  <Button
                    variant="contained"
                    name="cancel"
                    type="button"
                    onClick={props.close}
                  >
                    CANCEL
                  </Button>

                  <Button
                    variant="contained"
                    name="save"
                    color="primary"
                    type="button"
                    onClick={props.addList}
                  >
                    SAVE
                  </Button>
                </div>
              </DialogActions>
            </Dialog>       
            
       </React.Fragment> 
       
    )
}

export default AddIRCAVehiclePop