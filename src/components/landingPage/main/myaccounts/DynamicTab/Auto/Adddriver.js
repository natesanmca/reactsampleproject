import React,{ useReducer,useState,useEffect} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

const AddDriver=(props)=>{
    return(
       <React.Fragment>
          
           <table>
               <thead>
                   <th>Driver Full Name</th>
                   <th>Driver License Number</th>
                   <th>Actions</th>
               </thead>
               <tbody>
               {props.driverlists && props.driverlists.map((data, index) =>
               <tr key={data.DriverId}>
                        <td>{data.driverName}</td>
                        <td>{data.driverLicenseNo}</td>
                        <td className="actionItems">                        
                            <span><EditIcon onClick={() => props.editList(data)}></EditIcon></span>
                            <span><DeleteIcon onClick ={()=>props.deleteList(data)}></DeleteIcon></span>
                        </td>
                   </tr>)}
                 
               </tbody>
           </table>
       </React.Fragment> 
       
    )
}

export default AddDriver