import React,{ useReducer,useState,useEffect} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import CloseIcon from "@material-ui/icons/Close";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";

const AddCoveragePop=(props)=>{

  const [VehicleType,setVehicleType] = useState([{Type:"private passenger"},{Type:"commercial light"},{Type:"commercial heavy"},
  {Type:"Trailer"},{Type:"Recreational"},{Type:"Motorcycle"}]);

const VehicleTypeTest = ([
  {Type:"private passenger"},{Type:"commercial light"},{Type:"commercial heavy"},
    {Type:"Trailer"},{Type:"Recreational"},{Type:"Motorcycle"}
]);

const PhysicalDamage = ([
  {Type:"Specified Perils"},{Type:"Comprehensive"},{Type:"Collision"},
  {Type:"All Perils"}
]);

const IAmounts = ([
  {IAmount:"$1000"},{IAmount:"$2500"},{IAmount:"$5000"},
  {IAmount:"$7500"},{IAmount:"$10000"}
]);

const ifoptions = ([
  {section:"Yes"},{section:"No"}
  ]);
  
  // useEffect(()=>{
  //   console.log("Cover List",props.coverlist);
  //   setVehicleType([]);
  //   setVehicleType([...VehicleType,{Type: "private passenger"}]);
  //   setVehicleType([...VehicleType,{Type: "commercial light"}]);
  //   setVehicleType([...VehicleType,{Type: "commercial heavy"}]);
  //   setVehicleType([...VehicleType,{Type: "Trailer"}]);
  //   setVehicleType([...VehicleType,{Type: "Recreational"}]);
  //   setVehicleType([...VehicleType,{Type: "Motorcycle"}]);
  //   props.coverlist.map((x,i)=>{
  //     console.log("Type Name",x.NickName);
  //     setVehicleType([...VehicleType,{Type: x.NickName}]);
  //     //VehicleType.push({Type: x.NickName});
  //   });

  //   console.log("Type List",VehicleType);
  // },[]);
    return(
       <React.Fragment>
        <Dialog
              open={props.openState}
              onClose={props.close}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">
                View coverage by Vehicle Type{" "}
                <CloseIcon
                  className="closeIcon"
                  onClick={props.close}
                ></CloseIcon>
              </DialogTitle>
              <DialogContent className="addVehicle">
                <Grid container spacing={3}>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">BASIC DETAILS</h6>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <label>Vehicle Type</label>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                     
                    >
                      <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="CoverageVehicleType"
                        value={props.data.CoverageVehicleType}
                        onChange={props.onchange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { VehicleType && VehicleType.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.Type}>{item.Type}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>
                                    
                  <Grid item md={6}>
                    {" "}
                    <label>Nick Name</label>
                  </Grid>
                  <Grid item md={6}>
                    
                    <TextField  name="NickName" value={props.data.NickName}
                               onChange={props.onchange} label="" variant="outlined" /> 

                  </Grid>
                  

                  <Grid item md={12}>
                    <h6 class="clrred mb20">PHYSICAL DAMAGE:</h6>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <label>Physicall Damage</label>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                      
                    >
                      <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="PhysicallDamage"
                        value={props.data.PhysicallDamageLimit}
                        onChange={props.onchange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { PhysicalDamage && PhysicalDamage.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.Type}>{item.Type}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <label>Physicall Damage Deductable</label>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                    
                    >
                      <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="physicallDamageDeductable"
                        value={props.data.physicallDamageDeductable}
                        onChange={props.onchange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>

                        { IAmounts && IAmounts.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.IAmount}>{item.IAmount}</MenuItem>)

                   })}

                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={12}>
                    <hr class="line" />
                  </Grid>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">TRANSPORTATION REPLACEMENT:</h6>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <label>Transportation Replacement</label>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                     
                    >
                      <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="transportationReplacement"
                        value={props.data.transportationReplacement}
                        onChange={props.onchange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={12}>
                    <hr class="line" />
                  </Grid>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">WAIVER OF DEPRECIATION:</h6>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <label>Waiver of Depreciation</label>
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                     
                    >
                      <InputLabel id="demo-simple-select-outlined-label"></InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="waiverOfDepreciation"
                        value={props.data.waiverOfDepreciation}
                        onChange={props.onchange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </DialogContent>

              <DialogActions>
                <div className="btn-group">
                  <Button
                    variant="contained"
                    name="cancel"
                    type="button"
                    onClick={props.close}
                  >
                    CANCEL
                  </Button>

                  <Button
                    variant="contained"
                    name="save"
                    color="primary"
                    type="button"
                    onClick={props.addList}
                  >
                    SAVE
                  </Button>
                </div>
              </DialogActions>
            </Dialog>
       </React.Fragment> 
       
    )
}

export default AddCoveragePop