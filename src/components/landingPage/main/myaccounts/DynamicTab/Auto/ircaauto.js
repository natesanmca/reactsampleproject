import React, { useReducer, useState,useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import CloseIcon from "@material-ui/icons/Close";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import AddCoverage from "./Addcoverage";
import AddCoveragePop from "./coveragepopup";
import AddVehiclePop from "./vehiclepopup";
import AddVehicle from "./AddVehicle";
import AddDriverPop from "./driverpopup"
import AddDriver from "./Adddriver"
import { useSelector, useDispatch } from "react-redux";
import { DataGrid, Column, Editing, Scrolling, Lookup, Summary, TotalItem,SearchPanel } from 'devextreme-react/data-grid';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { v4 as uuidv4 } from 'uuid';
import { useHistory } from "react-router-dom";
import {useAlert} from 'react-alert'
import axios from 'axios';
import {
    fetchTimeLine
  } from "../../../../../../Redux/Actions/actions";
import AddIRCAVehiclePop from "./addvehicleircapopup";
import AddIRCAVehicle from "./addircavehicle";

  const API_URL = process.env.REACT_APP_BASE_API_URL;

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

const AutoIRCA = (props) => {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const [openDriver, setOpenDriver] = React.useState(false);
  const [openCoverage, setOpenCoverage] = React.useState(false);
  const List = useSelector((state) => state.TimeLine);
  const[timeline,settimeline] = React.useState(List.timeline);
  const PolicyReducer = useSelector(state => state.PolicyQuote);
  const[selectedBenifits,setselectedBenifits] = useState("");
const Account = useSelector(state=>state.Account);
const submission = useSelector((state) => state.SubMission);
  const [itemList, setitemList] = useState([]);

  const [PBirthDate, setPBirthDate] = React.useState(new Date());
  const [OBirthDate, setOBirthDate] = React.useState(new Date());

  let history = useHistory();
  const alert = useAlert();
  const dispatch = useDispatch();


  const handlePBirthDate = (date) => {
    setPBirthDate(date);
    SetVehicleList(VehicleList => ({
      ...VehicleList, 
      ['Principal_BirthDate']: date
  }));
    console.log("PBirthDate",PBirthDate);
  };

  const handleOBirthDate = (date) => {
    setOBirthDate(date);
    SetVehicleList(VehicleList => ({
      ...VehicleList, 
      ['Occational_BirthDate']: date
  }));
    console.log("Occational_BirthDate",OBirthDate);
  };

  const AutoLiabilityLimit = ([ 
  {Limit:"$ 1,000,000"},{Limit:"$ 2,000,000"},{Limit:"$ 3,000,000"},
  {Limit:"$ 4,000,000"},{Limit:"$ 5,000,000"},{Limit:"$ 6,000,000"},
  {Limit:"$ 7,000,000"},{Limit:"$ 8,000,000"},{Limit:"$ 9,000,000"},
  {Limit:"$ 10,000,000"}

        ]);
    const FamilyProtection = ([
      {Limit:"$ 1,000,000"},{Limit:"$ 2,000,000"},{Limit:"$ 3,000,000"},
      {Limit:"$ 4,000,000"},{Limit:"$ 5,000,000"}
      ]);
  
    const AccidentBenefits = ([
      {ABen:"Standard"},{ABen:"Enhanced"}
      ]);

      const IAmounts = ([
        {IAmount:"$600"},{IAmount:"$800"},{IAmount:"$1000"}
   ]);
       const MAmounts = ([
        {MAmount:"$130000"},{MAmount:"$1000000"}
   ]);
   const ifoptions = ([
    {section:"Yes"},{section:"No"}
    ]);

  const addVechile = (event) => {
    event.stopPropagation();
    setOpen(true);
  };
  const addDriver = (event) => {
    event.stopPropagation();
    setOpenDriver(true);
  };
  const addCoverage = (event) => {
    event.stopPropagation();
    setOpenCoverage(true);
    SetCoverageList(emptyState);
  };

  const handleClose = () => {
    setOpen(false);
    setOpenDriver(false);
    setOpenCoverage(false);
  };
  const handleChange = (evt) => {
    const name = evt.target.name;
    let newValue = evt.target.value;
    setVehicleInput({ [name]: newValue });
  };

  const handleInputChangeSub = (event, index,Id) => {
  
    console.log('OperationChages');
    let tempTitleList = [...itemList];
    let isitemAvailable = tempTitleList.find(
        (tempTitle) => tempTitle.ItemId === Id
      );
    //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
      tempTitleList.map((tempTitle) => {
        if (tempTitle.ItemId === Id && isitemAvailable) {
            
          tempTitle.ChildItems[index][event.target.name] = event.target.value;
            if (tempTitle.ChildItems[index].ChildItemId === "72861d16-6cf4-45a3-9654-a7026fcfec3c") {
                setselectedBenifits(event.target.value);
            }
           
          setitemList(tempTitleList);
          console.log(tempTitleList);
        }
    });

    let tempTitleList1 = [...timeline];

    let isPolicyTypeAvailable = tempTitleList1.find(
      (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
    );
    tempTitleList1.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === props.policyTypeId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == props.coverages.coverageId) {
            tempTitle.coverages[coverageIndex].coveragedata[0].CoverageItems = itemList;
          }
        });

      }

    });

    settimeline(tempTitleList1);
   

  };

  const [vehicleInput, setVehicleInput] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
     VehicleDetails: [],
     DriverDetails:[],
      autoLiabilityLimit: "",
      familyProtection: "",
      accidentBenifits: "",
      incomeReplacementBenifit: "",
      medicalRehabilitation: "",
      attendantCare: "",
      deathFuneral: "",
      indexation: "",
      depedantCare: "",
      CovByvehicleTypeList: [],
    }
  );
  const VehicleChange = (evt) => {
    const name = evt.target.name;
    let newValue = evt.target.value;
    SetVehicleList({ [name]: newValue });
  };
  const DriverChange = (evt) => {
    const name = evt.target.name;
    let newValue = evt.target.value;
    SetDriverList({ [name]: newValue });
  };
  const CoverageChange = (evt) => {
    const name = evt.target.name;
    let newValue = evt.target.value;
    SetCoverageList({ [name]: newValue });
  };

  
  const [Vehicles,setVehicles] = useState([]);
  const [VehicleList, SetVehicleList] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      // id: "",
      VehicleId: uuidv4(),
      RegisteredOwner: "",
      VehicleType: "",
      VehicleYear: "",
      VehicleMake: "",
      VehicleModel: "",
      VehicleIdentificationNumber: "",
      VehicleClass: "",
      ListPriceNew: "",
      BodyType: "",
      Weight:"",
      Radius_Operation: "",
      Principal_Driver_Name: "",
      Principal_Driver_Description: "",
      Principal_BirthDate: PBirthDate,
      Principal_LicenseYear: "",
      Principal_DriverTrain: "",
      Principal_Conviction: "",
      Occational_Driver_Name: "",
      Occational_Driver_Description: "",
      Occational_BirthDate: OBirthDate,
      Occational_LicenseYear: "",
      Occational_DriverTrain: "",
      Occational_Conviction: "",
      Physical_Damage: "",
      Physical_Damage_Deductible: "",
      Transportation_Replacement: "",
      Waiver_Depreciation: "",
      Owned_Leased_Financed: "",
      Lienholder_Lessor: "",
      Attached_Machinery: "",
      Attached_Machinery_Description: "",
      Attached_Machinery_Value: ""
    }
  );
  const [CovByvehicleTypeList,setCovByvehicleTypeList] = useState([]);
  const [coverageList, SetCoverageList] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      CoverageVehicleId:uuidv4(),
      CoverageVehicleType: "",
      NickName: "",
      PhysicallDamage: "",
      waiverOfDepreciation: "",
      physicallDamageDeductable: "",
      transportationReplacement: "",
    }
  );
  const [DriversList,setDriversList] = useState([]);
  const [driverList, SetDriverList] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      DriverId:uuidv4(),
      driverName: "",
      driverLicenseNo: ""
    
    }
  );
  const emptyDriverState = {
      DriverId:uuidv4(),
      driverName: "",
      driverLicenseNo: ""
  };
  const emptyState = {
    CoverageVehicleId:uuidv4(),
    CoverageVehicleType: "",
    NickName: "",
    PhysicallDamage: "",
    waiverOfDepreciation: "",
    physicallDamageDeductable: "",
    transportationReplacement: "",
  };
  const emptyVehicleState = {
      // id: "",
      VehicleId: uuidv4(),
      RegisteredOwner: "",
      VehicleType: "",
      VehicleYear: "",
      VehicleMake: "",
      VehicleModel: "",
      VehicleIdentificationNumber: "",
      VehicleClass: "",
      ListPriceNew: "",
      BodyType: "",
      Weight:"",
      Radius_Operation: "",
      Principal_Driver_Name: "",
      Principal_Driver_Description: "",
      Principal_BirthDate: PBirthDate,
      Principal_LicenseYear: "",
      Principal_DriverTrain: "",
      Principal_Conviction: "",
      Occational_Driver_Name: "",
      Occational_Driver_Description: "",
      Occational_BirthDate: OBirthDate,
      Occational_LicenseYear: "",
      Occational_DriverTrain: "",
      Occational_Conviction: "",
      Physical_Damage: "",
      Physical_Damage_Deductible: "",
      Transportation_Replacement: "",
      Waiver_Depreciation: "",
      Owned_Leased_Financed: "",
      Lienholder_Lessor: "",
      Attached_Machinery: "",
      Attached_Machinery_Description: "",
      Attached_Machinery_Value: ""
      //vehicleOwnedBy: ""
  };

  const [addedCoverageList, setAddedCoverageList] = useState([]);

  const addDriverList = () => {
    setOpenDriver(false);
    // let addedDriverListData = vehicleInput;
    // if (driverList.id) {
    //   addedDriverListData.DriverDetails.map((data) => {
    //     if (data.id === driverList.id) {
    //       Object.assign(data, driverList);
    //     }
    //   });
    // } else {
    //   addedDriverListData.DriverDetails.push({
    //     ...driverList,
    //     id: addedDriverListData.DriverDetails.length + 1,
    //   });
    // }
    // setVehicleInput(addedDriverListData);
    // SetDriverList({ ...emptyDriverState });
    // console.log(addedDriverListData);


    let addedDriversList = DriversList;
    let IsAvailable = addedDriversList.find(
      (coverageData) =>
        coverageData.DriverId === driverList.DriverId);
 
      if (IsAvailable) {
        addedDriversList.map((data) => {
          if (data.DriverId === driverList.DriverId) {
            Object.assign(data, driverList);
          }
        });
        
        
      } else {
        if (driverList.driverName !== "") {
        addedDriversList.push(driverList);
        }
      }  
   
    setDriversList(addedDriversList);
    SetDriverList({ ...emptyDriverState });
    console.log("add Driver List1",addedDriversList);
    console.log("add Driver List2",DriversList);

    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === props.policyTypeId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == props.coverages.coverageId) {
            tempTitle.coverages[coverageIndex].coveragedata[0].Driver = DriversList;
          }
        });

      }

    });

    settimeline(tempTitleList);


  };

  const addCoverageList = () => {
    setOpenCoverage(false);
    // let addedCoverageListData = vehicleInput;
    // if (coverageList.id) {
    //   addedCoverageListData.CovByvehicleTypeList.map((data) => {
    //     if (data.id === coverageList.id) {
    //       Object.assign(data, coverageList);
    //     }
    //   });
    // } else {
    //   addedCoverageListData.CovByvehicleTypeList.push({
    //     ...coverageList,
    //     id: addedCoverageListData.CovByvehicleTypeList.length + 1,
    //   });
    // }
    let addedCovByvehicleTypeList = CovByvehicleTypeList;
    let IsAvailable = addedCovByvehicleTypeList.find(
      (coverageData) =>
        coverageData.CoverageVehicleId === coverageList.CoverageVehicleId);
 
      if (IsAvailable) {
        addedCovByvehicleTypeList.map((data) => {
          if (data.CoverageVehicleId === coverageList.CoverageVehicleId) {
            Object.assign(data, coverageList);
          }
        });
        
        
      } else {
        if (coverageList.CoverageVehicleType !== "") {
        addedCovByvehicleTypeList.push(coverageList);
        }
      }  
   
    setCovByvehicleTypeList(addedCovByvehicleTypeList);
    
    console.log("add coverage List1",addedCovByvehicleTypeList);
    console.log("add coverage List2",CovByvehicleTypeList);

    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === props.policyTypeId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == props.coverages.coverageId) {
            tempTitle.coverages[coverageIndex].coveragedata[0].CoverageVehicleType = CovByvehicleTypeList;
          }
        });

      }

    });

    settimeline(tempTitleList);
    SetCoverageList({ ...emptyState });

  };
  const addVehicleList = () => {
    setOpen(false);
    // let addedVehicleListData = vehicleInput;
    // if (VehicleList.id) {
    //   addedVehicleListData.VehicleDetails.map((data) => {
    //     if (data.id === VehicleList.id) {
    //       Object.assign(data, VehicleList);
    //     }
    //   });
    // } else {
    //   addedVehicleListData.VehicleDetails.push({
    //     ...VehicleList,
    //     id: addedVehicleListData.VehicleDetails.length + 1,
    //   });
    // }
    // setVehicleInput(addedVehicleListData);
    // SetVehicleList({ ...emptyVehicleState });
    // console.log(addedVehicleListData);

    let addedvehicleList = Vehicles;
    let IsAvailable = addedvehicleList.find(
      (coverageData) =>
        coverageData.VehicleId === VehicleList.VehicleId);
 
      if (IsAvailable) {
        addedvehicleList.map((data) => {
          if (data.VehicleId === VehicleList.VehicleId) {
            Object.assign(data, VehicleList);
          }
        });
        
        
      } else {
        
        if (VehicleList.VehicleType !== "") {

          addedvehicleList.push(VehicleList);

        }
       
        
      }  
   
    setVehicles(addedvehicleList);
    SetVehicleList({ ...emptyVehicleState });
    console.log("add Vehicle List1",addedvehicleList);
    console.log("add Vehicle List2",Vehicles);

    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === props.policyTypeId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == props.coverages.coverageId) {
            tempTitle.coverages[coverageIndex].coveragedata[0].Vehicle = Vehicles;
          }
        });

      }

    });

    settimeline(tempTitleList);

  };
  const DeleteVehicleList = (data) => {
    let tempTitleList = vehicleInput;
   vehicleInput.VehicleDetails.map((tempTitle, index) => {
       if (tempTitle.id === data.id) {
        tempTitleList.VehicleDetails.splice(index,1);
       }
   });
   SetVehicleList(tempTitleList);
 };
 const DeleteCoverageList = (e) => {
  console.log("event",e);
    
 let tempTitleList = CovByvehicleTypeList;
  tempTitleList.map((data, index) => {
   if (data.CoverageVehicleId === e.row.data.CoverageVehicleId) {
    tempTitleList.splice(index,1);
   }
 });

 setCovByvehicleTypeList(tempTitleList);
};
const DeleteDriverList = (data) => {
  let tempTitleList = vehicleInput;
 vehicleInput.DriverDetails.map((tempTitle, index) => {
     if (tempTitle.id === data.id) {
      tempTitleList.DriverDetails.splice(index,1);
     }
 });
 SetDriverList(tempTitleList);
};
 
  
  const EditList = (data) => {
    setOpenCoverage(true);
    SetCoverageList({ ...data });
  };
  const EditVehicleList = (data) => {
    setOpen(true);
    SetVehicleList({ ...data });
  };
  const EditdriverList = (data) => {
    setOpenDriver(true);
    SetDriverList({ ...data });
  };
  const editCoveragedata=(e)=>{
    console.log("event",e);
    
  let addedCovByvehicleTypeList = CovByvehicleTypeList;
  addedCovByvehicleTypeList.map((data) => {
    if (data.CoverageVehicleId === e.row.data.CoverageVehicleId) {
      setOpenCoverage(true);
SetCoverageList({ ...data });
    }
  });
    
    

  };

  useEffect(() => {
   

    dispatch(fetchTimeLine(timeline));


    console.log("TimeLine dynamic",timeline);
 
  
}, [timeline]);
  useEffect(()=>{
    console.log("Props Auto",props);
    // console.log("Coverages",props.location.state.id.coverages);
    // props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{
    //   console.log(data);
    if (props.coverages) {
      setitemList(props.coverages.coveragedata[0].CoverageItems);
      setCovByvehicleTypeList(props.coverages.coveragedata[0].CoverageVehicleType)
      setVehicles(props.coverages.coveragedata[0].Vehicle)
      setDriversList(props.coverages.coveragedata[0].Driver)
      //setDefault(props.coverages.coveragedata[0].CoverageOtherItem);
      
      props.coverages.coveragedata[0].CoverageItems && props.coverages.coveragedata[0].CoverageItems.map((x,i)=>{

        if (x.ChildItems[0].ChildItemId === "72861d16-6cf4-45a3-9654-a7026fcfec3c") {
          console.log("Val Data",x.ChildItems[0].ValData);
          setselectedBenifits(x.ChildItems[0].ValData);
        }
      });

    }
    // //   data.coveragedata.CoverageItems && data.coveragedata.CoverageItems.map((item)=>{
    // //     if (item.ItemId === "d4edafea-5b80-483a-8d3e-9e3f59a3e07c") {
    // //         console.log("selectdata",item.ChildItems[0].ValData);
    // //         setselected(item.ChildItems[0].ValData);
    // //     }

    // // });
    // });

    
   // DataLoad();
  },[props.coverages]);


   // handle click event of the Remove button

  // handle click event of the Add button
  const onSubmission = (event) => {
    
    console.log("submission Type",PolicyReducer);
    if (PolicyReducer.SubMissionType === "Submission") {
    const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        policylist : timeline
       // otherItem : Default

    }

    
    console.log("Request Coverage Test");
    console.log("Request Coverage ",Request);
    console.log("Request String",JSON.stringify(Request));
    let url = API_URL + 'Quote/new-policy';
    //https://localhost:5000/api/Quote/new-policy
    axios.post(url, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(res.data.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/Submissions");
              }
            });
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
 
    // console.log(operationList);
     console.log(itemList);
  
  });
    }
    else if (PolicyReducer.SubMissionType === "InsurerQuote") {

      const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        InsurerQuotationNo : PolicyReducer.QuotationNo,
        policylist : timeline,
        InsurerInfoview : PolicyReducer.InsurerInfo,
        BindingInfoview : PolicyReducer.BindingInfo,
        InforceInfoview : PolicyReducer.InforceInfo
       // otherItem : Default
    
    }
    console.log("Request Quote Coverage Test89888");
    console.log("Request Quote Coverage ",Request);
    console.log("Request Quote String",JSON.stringify(Request));
    console.log("PolicyReducer",PolicyReducer);
    let url = API_URL + 'Policy/new-policy-quote';
    
    axios.post(url, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            fileupload(res.data);
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
    // console.log(itemList);
    
    });
      
    }
    
      }
    const fileupload = (ResData)=>{
      let formData = new FormData();
      formData.append('InsurerQuotationNo', ResData.Data.InsurerQuotationNo);
      formData.append('BindingId', ResData.Data.BindingId);
      formData.append('InsurerId', ResData.Data.InsurerId);
      formData.append('InForceId', ResData.Data.InForceId);
      PolicyReducer.InsurerInfofiles && PolicyReducer.InsurerInfofiles.map((file)=>{
        formData.append('InsurerFiles', file);
        console.log("file",file);
      })
      PolicyReducer.BindingInfofiles && PolicyReducer.BindingInfofiles.map((file)=>{
        formData.append('BindingFiles', file);
        console.log("file",file);
      })
      PolicyReducer.InforceInfofiles && PolicyReducer.InforceInfofiles.map((file)=>{
        formData.append('InForceFiles', file);
        console.log("file",file);
      })
    
      axios.post(`${API_URL}Policy/file-upload`, formData)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(ResData.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/policys");
              }
            });
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
     //console.log(itemList);
    
    });
    
    }
    const onClear = () =>{
    //DataLoad();
}


  return (
    <Container maxWidth="">
      <Grid container spacing={3}>
        <Grid item md={12}>
          <div className="mainContent">
           <AddIRCAVehiclePop close={handleClose}
              open={addVechile}
              addList={addVehicleList}
              openState={open}
              data={VehicleList}
              onchange={VehicleChange}
               onPdatechange={handlePBirthDate} 
               onOdatechange={handleOBirthDate}
               >
              </AddIRCAVehiclePop>
            

        

            <div className="accordian">
            
            <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel2a-content"
                  id="panel2a-header"
                >
                  <Typography className={classes.heading}>
                    BLANKET COVERAGE DETAILS
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                    <Grid container spacing={3}>
                      <Grid item md={6}>
                        {/* <Grid container spacing={3}> */}
                        { itemList && itemList.map((x,i)=> {
                         
                         if (x.GroupName === "Blanket Coverage") {
                          return (
                         <Grid container spacing={3} >
                          <Grid item md={6}>
                          <label> {x.LineItem} </label>
                          </Grid>

                          <Grid item md={6}>
                          {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.ChildItemId === "b0f51d2d-3f4d-4c1e-ae43-bf528c31b51f") {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label=""
            >

                            <MenuItem value="">
                                <em>None</em>
                              </MenuItem>
                              { AutoLiabilityLimit && AutoLiabilityLimit.map((item, index)=>{
                          return(
                          <MenuItem key={index} value={item.Limit}>{item.Limit}</MenuItem>)

                          })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.ChildItemId === "bc10f4c9-2646-4051-856a-4ec10140c3fd") {
          return (
         <FormControl variant="outlined" className="select">
         <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
         <Select
         labelId="demo-simple-select-outlined-label"
         id={ch.ChildItemId}
         name="ValData"  
         value={ch.ValData}
         onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
         label=""
         >

                         <MenuItem value="">
                             <em>None</em>
                           </MenuItem>
                           { FamilyProtection && FamilyProtection.map((item, index)=>{
                           return(
                           <MenuItem key={index} value={item.Limit}>{item.Limit}</MenuItem>)
   
                           })}

         </Select>
         </FormControl>
     
     );}

     else if (ch.ChildItemId === "72861d16-6cf4-45a3-9654-a7026fcfec3c") {
      return (
     <FormControl variant="outlined" className="select">
     <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
     <Select
     labelId="demo-simple-select-outlined-label"
     id={ch.ChildItemId}
     name="ValData"  
     value={ch.ValData}
     onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
     label=""
     >

                     <MenuItem value="">
                         <em>None</em>
                       </MenuItem>
                       { AccidentBenefits && AccidentBenefits.map((item, index)=>{
                           return(
                           <MenuItem key={index} value={item.ABen}>{item.ABen}</MenuItem>)
   
                           })}

     </Select>
     </FormControl>
 
 );}
        
             })}
                          </Grid>
                          </Grid>
                          )
                         }                       
                        }) }
                         { selectedBenifits === "Enhanced" && <Grid container spacing={3}> <Grid item md={12}>
                            <h6 class="clrred mb20">Additional Questions</h6>
                      </Grid> </Grid>  }
                      { itemList && itemList.map((x,i)=> {
                         
                         if (x.GroupName === "Accident Benefits" && selectedBenifits === "Enhanced") {
                          return (
                         <Grid container spacing={3}>
                          <Grid item md={6}>
                          <label> {x.LineItem} </label>
                          </Grid>

                          <Grid item md={6}>
                          
                    {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                        if ((ch.ChildItemId === "c6b42286-9cd8-4da8-a9bb-d1089091df4c" ) && ch.LineInputType == 5) {
                        return(
                     
                    <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id="demo-simple-select-outlined"
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Amount"
                   >

                   { IAmounts && IAmounts.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.IAmount}>{item.IAmount}</MenuItem>)

                   })} 

                   </Select>
                </FormControl> );
                        }
                    else if ((ch.ChildItemId === "7f0fe604-5408-4193-bae3-489480567288" ) && ch.LineInputType === 5) {
                     return (
                    <FormControl variant="outlined" className="select">
                    <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                    <Select
                    labelId="demo-simple-select-outlined-label"
                    id={ch.ChildItemId}
                    name="ValData"  
                    value={ch.ValData}
                    onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                    label="Amount"
                    >

                    { MAmounts && MAmounts.map((item, index)=>{
                    return(
                    <MenuItem key={index} value={item.MAmount}>{item.MAmount}</MenuItem>)

                    })} 

                    </Select>
                    </FormControl>
                
                );}
                else if (ch.LineInputType === 5) {
                    return (
                   <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id={ch.ChildItemId}
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Select"
                   >

                   { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })} 

                   </Select>
                   </FormControl>
               
               );}
                else if (ch.LineInputType == 2) {
                    return(
                        <TextField  name="ValData" value={ch.ValData}
                       onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
                );}
                     })}
               
                          </Grid>
                          </Grid>

                          )}                       
                        }) }



                       
                        {/* </Grid> */}
                      </Grid>
                    </Grid>
                  </Typography>
                </AccordionDetails>
              </Accordion>

              
              
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography className={classes.heading}>
                    <span>VEHICLE DETAILS</span>{" "}
                    <Button
                      variant="contained"
                      name="ADD VEHICLE"
                      color="primary"
                      type="button"
                      onClick={addVechile}
                    >
                      ADD VEHICLE <AddIcon></AddIcon>
                    </Button>
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography className="auto-table">
                  <AddIRCAVehicle
                      vehiclelists={Vehicles}
                      editList={(editData) => EditVehicleList(editData)}
                      deleteList ={(deleteData) => DeleteVehicleList(deleteData)}
                    ></AddIRCAVehicle>
                  </Typography>
                </AccordionDetails>
              </Accordion>

            </div>
            {/* <div className="btn-group">
                  <Button
                    variant="contained"
                    name="cancel"
                    type="button"
                    
                    
                  >
                    CANCEL
                  </Button>

                  <Button
                    variant="contained"
                    name="save"
                    color="primary"
                    type="button"
                   
                  >
                    SAVE
                  </Button>
                </div> */}
          </div>
        </Grid>
      </Grid>
    </Container>
  );
};

export default AutoIRCA;
