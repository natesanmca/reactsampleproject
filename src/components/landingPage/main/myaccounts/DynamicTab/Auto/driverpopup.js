import React,{ useReducer,useState,useEffect} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import CloseIcon from "@material-ui/icons/Close";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';


const AddDriverPop=(props)=>{

  const ifoptions = ([
    {section:"Yes"},{section:"No"}
    ]);
  

    return(
       <React.Fragment>
        <Dialog
              open={props.openState}
              onClose={props.close}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">
                Add Driver{" "}
                <CloseIcon
                  className="closeIcon"
                  onClick={props.close}
                ></CloseIcon>
              </DialogTitle>
              <DialogContent>
                <Grid container spacing={3}>
                  <Grid item md={12}>
                    <h6 class="clrred mb20">DRIVER DETAILS</h6>
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="driverName"
                      name="driverName"
                      value={props.data.driverName}
                      onChange={props.onchange}
                      label="Driver Name"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    {" "}
                    <TextField
                      id="driverLicenseNo"
                      name="driverLicenseNo"
                      value={props.data.driverLicenseNo}
                      onChange={props.onchange}
                      label="Driver License"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container justify="space-around" className="datePicker">
        <KeyboardDatePicker
          disableToolbar
          name="BirthDate"
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="date-picker-inline"
          label="Birth Date"
          value={props.data.BirthDate}
          onChange={props.onDatechange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        </Grid>
        </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={6}>
                    {" "}
                    <TextField
                      id="LicenseYear"
                      name="LicenseYear"
                      value={props.data.LicenseYear}
                      onChange={props.onchange}
                      label="License Year"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item md={6}>
                    <FormControl
                      variant="outlined"
                     
                    >
                      <InputLabel id="demo-simple-select-outlined-label"> Driver Training </InputLabel>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        name="DriverTrain"
                        value={props.data.DriverTrain}
                        onChange={props.onchange}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item md={12}>
                    <div class="description">
                      <textarea
                        id="Attached_Machinery_Description"
                        name="Conviction"
                        value={props.data.Conviction}
                        onChange={props.onchange}
                        placeholder="Conviction"
                      ></textarea>
                    </div>
                  </Grid>
                </Grid>
              </DialogContent>
              <DialogActions>
                <div className="btn-group">
                  <Button
                    variant="contained"
                    name="cancel"
                    type="button"
                    onClick={props.close}
                  >
                    CANCEL
                  </Button>

                  <Button
                    variant="contained"
                    name="save"
                    color="primary"
                    type="button"
                    onClick={props.addList}
                  >
                    SAVE
                  </Button>
                </div>
              </DialogActions>
            </Dialog>
       </React.Fragment> 
       
    )
}

export default AddDriverPop