import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import {useAlert} from 'react-alert'
import axios from 'axios';
import {
  fetchTimeLine,
  fetchSelectedPolicy,
  fetchCheckState,
} from "../../../../../../Redux/Actions/actions";
import AutoFleet from "./autoFleet";
import AutoIRCA from "./ircaauto";

const API_URL = process.env.REACT_APP_BASE_API_URL;
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

const AutoCoverage = (props) => {
  const classes = useStyles();
  const List = useSelector((state) => state.TimeLine);
  const Account = useSelector(state=>state.Account);
const submission = useSelector((state) => state.SubMission);
const[timeline,settimeline] = useState(List.timeline);
  console.log("list", List);
  const policyData = props.location.state.id;
  const PolicyReducer = useSelector(state => state.PolicyQuote);
  let history = useHistory();
  const alert = useAlert();
  // const policyData = List.timeline.find(
  //   (data) => data.policyType === "Property"
  // );

  // const renderFirstAccordian = () => {
  //   const hasBusinessOrBuilding = policyData.coverages.find(
  //     (coverageData) =>
  //       coverageData.coverageId === "3b964469-136a-4c5f-8c46-e53eb0d4e3ac" ||
  //       coverageData.coverageId === "c51de115-1cbe-4b58-bf9e-7fd35b98a372" ||
  //       coverageData.coverageId === "7fb24475-cc3d-4999-af11-ade7f2cf022a"
  //   );
     
  //   if (hasBusinessOrBuilding) {
  //     return (
  //       <Accordion className="accordian">
  //         <AccordionSummary
  //           expandIcon={<ExpandMoreIcon />}
  //           aria-controls="panel1a-content"
  //           id="panel1a-header"
  //         >
  //           <Typography className={classes.heading}>
  //             Property Coverage
  //           </Typography>
  //         </AccordionSummary>
  //         <AccordionDetails>
  //           <Typography>
  //             <LocationList coverages={policyData}></LocationList>
  //           </Typography>
  //         </AccordionDetails>
  //       </Accordion>
  //     );
  //   }
  // };
const renderAccordianContent = (coverageId) =>{
 
   switch (coverageId) {
      case "aad39017-f4c2-4736-976f-ff2da8571af0":
        let fleetdata = policyData.coverages.find(
          (coverageData) =>
            coverageData.coverageId === "aad39017-f4c2-4736-976f-ff2da8571af0");
       return  <AutoFleet coverages={fleetdata} policyTypeId = {policyData.policyTypeId}></AutoFleet>
        break;
        case "f2edcdb9-90d2-4558-9171-f9a345d8c4a2":
          let ircadata = policyData.coverages.find(
            (ircacoverageData) =>
            ircacoverageData.coverageId === "f2edcdb9-90d2-4558-9171-f9a345d8c4a2");
         return  <AutoIRCA coverages={ircadata} policyTypeId = {policyData.policyTypeId}></AutoIRCA>
          break;

      default:
        break;
    }
 
}
  const renderAccordian = () => {
    let hasBusinessOrBuilding = false;

    return policyData.coverages.map((coverageData, index) => {
      if (
        (
          coverageData.coverageId === "aad39017-f4c2-4736-976f-ff2da8571af0" ||
          coverageData.coverageId === "f2edcdb9-90d2-4558-9171-f9a345d8c4a2" 
        )
      ) {
        return (
          <Accordion key={index} className="accordian">
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>
                {coverageData.coverageName}
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
             {renderAccordianContent(coverageData.coverageId)}
              </Typography>
            </AccordionDetails>
          </Accordion>
        );
      }
    });
  };

  useEffect(()=>{
    console.log("Props AutoFleet",props);
    //console.log("Coverages",props.location.state.id.coverages);
   

    
   // DataLoad();
  },[]);
  const onClear = () =>{
    //DataLoad();
}


const onSubmission = (event) => {
  if (PolicyReducer.SubMissionType === "Submission") {
  const Request = {
      AccountNumber : Account.AcNo,
      SubMissionNo : submission.SubmissionNo,
      policylist : timeline
     // otherItem : Default

  }

  
  console.log("Request Coverage Test");
  console.log("Request Coverage ",Request);
  console.log("Request String",JSON.stringify(Request));
  let url = API_URL + 'Quote/new-policy';
  //https://localhost:5000/api/Quote/new-policy
  axios.post(url, Request)
  .then(res => {
    
      if(res.data.Success){    
          //let bdmList = res.data.Data;
          alert.success(res.data.Message,{
            timeout:5000,
            onClose: () => {
             history.push("/myaccounts/accountDetails/Submissions");
            }
          });
          //alert(res.data.Message);
       //  console.log(res.data.Message);
       //  history.push("/UserComp");
        }
        else {
         alert.info(res.data.Message);
        }

  // console.log(operationList);
   //console.log(itemList);

});
  }
else if (PolicyReducer.SubMissionType === "InsurerQuote") {

  const Request = {
    AccountNumber : Account.AcNo,
    SubMissionNo : submission.SubmissionNo,
    InsurerQuotationNo : PolicyReducer.QuotationNo,
    policylist : timeline,
    InsurerInfoview : PolicyReducer.InsurerInfo,
    BindingInfoview : PolicyReducer.BindingInfo
   // otherItem : Default

}
console.log("Request Quote Coverage Test1111",Request.BindingInfoview);
//console.log("Request Quote Coverage ",Request);
//console.log("Request Quote String",JSON.stringify(Request));

let url = API_URL + 'Policy/new-policy-quote';

axios.post(url, Request)

.then(res => {
    //  alert('test');
    if(res.data.Success){    
        //let bdmList = res.data.Data;
        fileupload(res.data);
        //alert(res.data.Message);
     //  console.log(res.data.Message);
     //  history.push("/UserComp");
      }
      else {
       alert.info(res.data.Message);
      }

// console.log(operationList);
 //console.log(itemList);

});
  
}

}

const fileupload = (ResData)=>{
  let formData = new FormData();
  formData.append('InsurerQuotationNo', ResData.Data.InsurerQuotationNo);
  formData.append('BindingId', ResData.Data.BindingId);
  formData.append('InsurerId', ResData.Data.InsurerId);
  PolicyReducer.InsurerInfofiles && PolicyReducer.InsurerInfofiles.map((file)=>{
    formData.append('InsurerFiles', file);
    console.log("file",file);
  })
  PolicyReducer.BindingInfofiles && PolicyReducer.BindingInfofiles.map((file)=>{
    formData.append('BindingFiles', file);
    console.log("file",file);
  })

  axios.post(`${API_URL}Policy/file-upload`, formData)
.then(res => {
  
    if(res.data.Success){    
        //let bdmList = res.data.Data;
        alert.success(ResData.Message,{
          timeout:5000,
          onClose: () => {
           history.push("/myaccounts/accountDetails/policys");
          }
        });
      }
      else {
       alert.info(res.data.Message);
      }

// console.log(operationList);
 //console.log(itemList);

});

}
  return (
    <div >
      {/* {renderFirstAccordian()} */}
      {renderAccordian()}

            <div className="btn-group">

            {props.location.state.total === props.location.state.index + 1 && <Button variant="contained" name="cancel" type="button" onClick={onClear}> 
            CANCEL
            </Button>}

            {props.location.state.total === props.location.state.index + 1 &&<Button variant="contained" name="save" color="primary" type="button" onClick={onSubmission}>

            SAVE
            </Button>}
       </div>

    </div>
  );
};

export default AutoCoverage;
