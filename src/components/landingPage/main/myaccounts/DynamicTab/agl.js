//import React from "react";
import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {useSelector, useDispatch} from "react-redux";
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import RemoveRoundedIcon from '@material-ui/icons/RemoveRounded';
import 'date-fns';
import NumberFormat from 'react-number-format';
import { v4 as uuidv4 } from 'uuid';
import {useAlert} from 'react-alert'
import { useHistory } from "react-router-dom";
import {
    fetchTimeLine,
    fetchSelectedPolicy,
    fetchCheckState,
  } from "../../../../../Redux/Actions/actions";

//import "./dynamic.css"
const API_URL = process.env.REACT_APP_BASE_API_URL;
const AGLCoverage=(props)=>{

console.log('Test Click')
console.log('Wrapup Cliock',props)


const [operationList, setoperationList] = useState([]);
const [itemList, setitemList] = useState([]);
const [] = useState([]);
const [] = useState([]);
const [building, setbuilding] = useState([{ buildingId: "", location: "" }]);
const Account = useSelector(state=>state.Account);
const submission = useSelector((state) => state.SubMission);
const stepperList = useSelector(state => state.TimeLine);
const[timeline,settimeline] = useState(stepperList.timeline);
const[selected,setselected] = useState("");
const[selectedBenifits,setselectedBenifits] = useState("");
const[OwnedAuto,setOwnedAuto] = useState("");
const[Comprehensive,setComprehensive] = useState("");
const[Hail,setHail] = useState("");
const[Lot ,setLot] = useState("");
const[AName ,setAName] = useState("");
const policyId = props.location.state.id.policyTypeId;
const PolicyReducer = useSelector(state => state.PolicyQuote);

const [Default, setDefault] = React.useState([{ LineItem: "", Limit: "", Deductible:"", LineItem2:"",GroupName: "" }]);
const [Default1, setDefault1] = React.useState([]);
const [E2Default, setE2Default] = React.useState([]);
const [customer, setcustomer] = React.useState(false);
const [own, setown] = React.useState(false);

let history = useHistory();
const alert = useAlert();
const dispatch = useDispatch();

const applicant = ([
  {Applicant:"General Contractor"},{Applicant:"Project Owner"},{Applicant:"Project Manager"}
  ]);

  const limitAmount = ([
    {Amount:"0"},{Amount:"1000000"},{Amount:"2000000"},{Amount:"3000000"},
  {Amount:"4000000"},{Amount:"5000000"}
    ]);

  const Operations = ([
    {Operation:"New Vehicles"},{Operation:"Used Vehicles"},{Operation:"Wholesale/Auction"},
    {Operation:"Repairs"},{Operation:"Service Station"},{Operation:"Storage Garage"},
    {Operation:"Parking Lot"},{Operation:"Towing"}
    ]);

    const Vehicles = ([
        {Vehicle:"Car & Light Trucks"},{Vehicle:"Heavy Trucks"},{Vehicle:"Motorcycles"},
        {Vehicle:"Snow Vehicles"},{Vehicle:"Recreational Vehicles"},{Vehicle:"Antique/Exotic"}
        ]);
  

  const ifoptions = ([
    {section:"Yes"},{section:"No"}
    ]);
    const Exclusions = ([
        {Exclusion:"Yes (OEF71)"},{Exclusion:"No (OEF76)"}
        ]);
    

    const DeAmount = ([
      {Amount:"0"},{Amount:"1000"},{Amount:"2500"},
      {Amount:"5000"},{Amount:"7500"},{Amount:"10000"}
      
    ]);

        const IAmounts = ([
            {IAmount:"$600"},{IAmount:"$800"},{IAmount:"$1000"}
       ]);
           const MAmounts = ([
            {MAmount:"$130000"},{MAmount:"$1000000"}
       ]);

  // handle input change
  const handleInputChangeother = (e, index,Id) => {
    const { name, value } = e.target;
    const list = [...Default];
    list.map((other, i) => {
      if (other.Id === Id) {
        list[i][name] = value;
      }
    });
    
    setDefault(list);
    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === props.policyTypeId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === props.policyTypeId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (coverage.coverageId == props.coverages.coverageId) {
            tempTitle.coverages[coverageIndex].coveragedata[0].CoverageOtherItem = Default;
          }
        });

      }

    });
    settimeline(tempTitleList);
    //dispatch(fetchOwnername(Default))
  };

  const handleRemoveClick = (index,Id) => {
    const list = [...Default];
   
    list.map((other, i) => {
if (other.Id === Id) {
  list.splice(i, 1);
}
});
    
    setDefault(list);
  };

  // handle click event of the Add button
  const handleAddClick = (e,Gname) => {
    setDefault([...Default, { Id:uuidv4(), LineItem: "", Limit: "", Deductible:"", LineItem2:"",GroupName: Gname }]);  
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...itemList];
    let rval = value;

    if(e.target.className == 'numberft'){
      rval=value.replace(/\D/g,'');
    }
    list[index][name] = rval;
    setitemList(list);
    //dispatch(fetchOwnername(operationList))
    let tempTitleList = [...timeline];

    let isPolicyTypeAvailable = tempTitleList.find(
      (tempTitle) => tempTitle.policyTypeId === policyId
    );
    tempTitleList.map((tempTitle) => {
      if (
        tempTitle.policyTypeId === policyId &&
        isPolicyTypeAvailable
      ) {

        tempTitle.coverages.map((coverage, coverageIndex) => {
          if (tempTitle.coverages.length === 2) {
            if (coverage.coverageId == "33f4e039-c083-4948-b9b5-f9f653e7d7d4") {
              tempTitle.coverages[coverageIndex].coveragedata[0].CoverageItems = itemList;
            }
          }
          else {
          if (coverage.coverageId == "33f4e039-c083-4948-b9b5-f9f653e7d7d4" || coverage.coverageId == "7ade4350-9afb-4b33-b1af-1ded47f9bb30") {
            tempTitle.coverages[coverageIndex].coveragedata[0].CoverageItems = itemList;
          }
          
        }
        });

      }

    });
    settimeline(tempTitleList);
  };

  const handleInputChangeSub = (event, index,Id) => {
  
    console.log('OperationChages');
    let tempTitleList = [...itemList];
    let isitemAvailable = tempTitleList.find(
        (tempTitle) => tempTitle.ItemId === Id
      );
    //  Operations:[{Id:"",Operation: "", Canadian: "", Ustates:"", IsUSR: false}]
      tempTitleList.map((tempTitle) => {
        if (tempTitle.ItemId === Id && isitemAvailable) {
            
          tempTitle.ChildItems[index][event.target.name] = event.target.value;
            if (tempTitle.ChildItems[index].ChildItemId === "9b94310c-b02b-4f48-bb80-2ccc5f5c47d2" || tempTitle.ChildItems[index].ChildItemId === "7ad5a709-ca25-46be-bf21-adf799a12ac7") {
                setselectedBenifits(event.target.value);
            }
            else if (tempTitle.ChildItems[index].ChildItemId === "cde0b426-d3ea-4165-a3e4-071493469362" || tempTitle.ChildItems[index].ChildItemId === "b60d3cca-6208-4d18-ab18-243f10a7a9c3") {
                setOwnedAuto(event.target.value);
            }
            else if (tempTitle.ChildItems[index].ChildItemId === "427269cd-d04e-4fd3-8e9d-332a9152b3e2" || tempTitle.ChildItems[index].ChildItemId === "ffe3ffaf-12a9-479d-9743-f7b88fb40000") {
                setComprehensive(event.target.value);
            }
            else if (tempTitle.ChildItems[index].ChildItemId === "61d5e52e-271b-4b26-b36d-cb05ad267106" || tempTitle.ChildItems[index].ChildItemId === "b5921002-3aa3-4be0-a772-2711cbda743d") {
                setLot(event.target.value);
            }
            else if (tempTitle.ChildItems[index].ChildItemId === "af8ae511-abb8-41ea-a67d-2be862e6ac7c" || tempTitle.ChildItems[index].ChildItemId === "b6bf9500-11c5-4ee4-9c5a-18145fd8861c") {
                setHail(event.target.value);
            }
            else if (tempTitle.ChildItems[index].ChildItemId === "3987e5a7-81c3-4553-9480-dc2168d1baaf" || tempTitle.ChildItems[index].ChildItemId === "737b2627-06c5-4600-8b11-46ee67907282") {
                setAName(event.target.value);
            }
          setitemList(tempTitleList);
          console.log(tempTitleList);
        }
    });
  };

  // handle click event of the Remove button

  // handle click event of the Add button
  const onSubmission = (event) => {
    
    console.log("submission Type",PolicyReducer);
    if (PolicyReducer.SubMissionType === "Submission") {
    const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        policylist : timeline
       // otherItem : Default

    }

    
    console.log("Request Coverage Test");
    console.log("Request Coverage ",Request);
    console.log("Request String",JSON.stringify(Request));
    let url = API_URL + 'Quote/new-policy';
    //https://localhost:5000/api/Quote/new-policy
    axios.post(url, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(res.data.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/Submissions");
              }
            });
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
 
    // console.log(operationList);
     console.log(itemList);
  
  });
    }
    else if (PolicyReducer.SubMissionType === "InsurerQuote") {

      const Request = {
        AccountNumber : Account.AcNo,
        SubMissionNo : submission.SubmissionNo,
        InsurerQuotationNo : PolicyReducer.QuotationNo,
        policylist : timeline,
        InsurerInfoview : PolicyReducer.InsurerInfo,
        BindingInfoview : PolicyReducer.BindingInfo,
        InforceInfoview : PolicyReducer.InforceInfo
       // otherItem : Default
    
    }
    console.log("Request Quote Coverage Test");
    console.log("Request Quote Coverage ",Request);
    console.log("Request Quote String",JSON.stringify(Request));
    console.log("PolicyReducer",PolicyReducer);
    let url = API_URL + 'Policy/new-policy-quote';
    
    axios.post(url, Request)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            fileupload(res.data);
            //alert(res.data.Message);
         //  console.log(res.data.Message);
         //  history.push("/UserComp");
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
    // console.log(itemList);
    
    });
      
    }
    
      }
    const fileupload = (ResData)=>{
      let formData = new FormData();
      formData.append('InsurerQuotationNo', ResData.Data.InsurerQuotationNo);
      formData.append('BindingId', ResData.Data.BindingId);
      formData.append('InsurerId', ResData.Data.InsurerId);
      formData.append('InForceId', ResData.Data.InForceId);
      PolicyReducer.InsurerInfofiles && PolicyReducer.InsurerInfofiles.map((file)=>{
        formData.append('InsurerFiles', file);
        console.log("file",file);
      })
      PolicyReducer.BindingInfofiles && PolicyReducer.BindingInfofiles.map((file)=>{
        formData.append('BindingFiles', file);
        console.log("file",file);
      })
      PolicyReducer.InforceInfofiles && PolicyReducer.InforceInfofiles.map((file)=>{
        formData.append('InForceFiles', file);
        console.log("file",file);
      })
    
      axios.post(`${API_URL}Policy/file-upload`, formData)
    .then(res => {
      
        if(res.data.Success){    
            //let bdmList = res.data.Data;
            alert.success(ResData.Message,{
              timeout:5000,
              onClose: () => {
               history.push("/myaccounts/accountDetails/policys");
              }
            });
          }
          else {
           alert.info(res.data.Message);
          }
    
    // console.log(operationList);
     //console.log(itemList);
    
    });
    
    }
    const onClear = () =>{
    //DataLoad();
}
  
  //   useEffect( ()=> {
  //       console.log("Props",props.location.state);
  //       DataLoad();
    
  // }, []);

//   useEffect(()=> {
    
//     if (submission.SubmissionNo !== "" && submission.SubmissionNo !== "0") 
//     {
//    let AcNo = submission.SubmissionNo;
//     const AcNo1 = {AcNo};
//     console.log(AcNo);
//     axios
//       .post("http://13.126.250.89/BWIAPI/api/Quote/get-submission-Details", AcNo1)
//       .then((res) => {
//         if (res.data.Success) {
//           let respdata = res.data.Data;
//           if (respdata.coveragedata) {
//           setitemList(respdata.coveragedata.CoverageItems);
//           setDefault(respdata.coveragedata.CoverageOtherItem);
//           }
//           //setisFirstloaded(false)
//         }
//       });
      
//     }
//     else {
//       //DataLoad();
//       console.log("Props",props.location.state.id);
//       console.log("Coverages",props.location.state.id.coverages);
//       props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{
//         console.log(data);
//         setitemList(data.coveragedata.CoverageItems);
//         setDefault(data.coveragedata.CoverageOtherItem);
//       });
//     }

//   },[]);

const BuildingLoad = () => {
    const urlstr = API_URL + 'Building/get-all-buildingInfos/' + Account.AcNo;
    console.log(urlstr);
    axios.get(urlstr)
      .then(res => {
        if(res.data.Success){ 
               let itemLists = res.data.Data;
               
               //let list = JSON.stringify(classDetails)
               //let listItem = JSON.parse(list)
            //    SetAddLocation(itemLists.ActiveLocation);
            //    SetdeleteLocation(itemLists.DeleteLocation);
               //alert.success(res.data.Message);
               //const [building, setbuilding] = useState([{ buildingId: "", location: "" }]);
               let buldingdata = [];
               res.data.Data.ActiveLocation && res.data.Data.ActiveLocation.map((data)=>{
                console.log("data",data);
                  //let Location =  data.Street + "," + data.City + "," + data.PostalCode;
                  buldingdata.push({buildingId: data.Id,location: data.Street + "," + data.City + "," + data.PostalCode});
                //setbuilding([ ...building,{buildingId: data.Id,location: data.Street + "," + data.City + "," + data.PostalCode}]);
               });
               setbuilding(buldingdata);
               console.log(res.data.Data);

       }

    });
  }

useEffect(() => {
   

    dispatch(fetchTimeLine(timeline));


    console.log("TimeLine dynamic",timeline);
 
  
}, [timeline]);

  useEffect(()=>{
    console.log("Props",props.location.state);
    console.log("Coverages",props.location.state.id.coverages);
    console.log("Coverages Count",props.location.state.id.coverages.length);
    setcustomer(false);
    setown(false);
    props.location.state.id.coverages && props.location.state.id.coverages.map((data)=>{

      if (props.location.state.id.coverages.length === 2) {
        setcustomer(true);
        setown(true);
        if (data.coverageId === "33f4e039-c083-4948-b9b5-f9f653e7d7d4") {

          console.log(data);
          setitemList(data.coveragedata[0].CoverageItems);
          setDefault(data.coveragedata[0].CoverageOtherItem);
          data.coveragedata[0].CoverageItems && data.coveragedata[0].CoverageItems.map((item)=>{
            // if (item.ItemId === "d4edafea-5b80-483a-8d3e-9e3f59a3e07c") {
            //     console.log("selectdata",item.ChildItems[0].ValData);
            //     setselected(item.ChildItems[0].ValData);
            // }
    
            if (item.ItemId === "7aabd447-5d30-4331-8c68-3379d7d4e50b") {
                setselectedBenifits(item.ChildItems[0].ValData);
            }
            else if (item.ItemId === "0f668a79-4f1f-4f53-977f-21311f2dbfe1") {
                setOwnedAuto(item.ChildItems[0].ValData);
            }
            else if (item.ItemId === "c1ac18b3-0f9b-43ab-a759-4d7f8b19fd55") {
                setComprehensive(item.ChildItems[0].ValData);
            }
            else if (item.ItemId === "711777d3-6c64-4a2b-b6cc-c9bd85f478a5") {
                setLot(item.ChildItems[0].ValData);
            }
            else if (item.ItemId === "f080205e-e107-4f23-badb-feae0a6e04df") {
                setHail(item.ChildItems[0].ValData);
            }
            else if (item.ItemId === "768e8084-2557-40d3-b66c-e95f7297dcb8") {
                setAName(item.ChildItems[0].ValData);
            }
    
        });
      }
      }
else {
      if (data.coverageId === "33f4e039-c083-4948-b9b5-f9f653e7d7d4") {
        setcustomer(true);
      console.log(data);
      setitemList(data.coveragedata[0].CoverageItems);
      setDefault(data.coveragedata[0].CoverageOtherItem);
      data.coveragedata[0].CoverageItems && data.coveragedata[0].CoverageItems.map((item)=>{
        // if (item.ItemId === "d4edafea-5b80-483a-8d3e-9e3f59a3e07c") {
        //     console.log("selectdata",item.ChildItems[0].ValData);
        //     setselected(item.ChildItems[0].ValData);
        // }

        if (item.ItemId === "7aabd447-5d30-4331-8c68-3379d7d4e50b") {
            setselectedBenifits(item.ChildItems[0].ValData);
        }
        else if (item.ItemId === "0f668a79-4f1f-4f53-977f-21311f2dbfe1") {
            setOwnedAuto(item.ChildItems[0].ValData);
        }
        else if (item.ItemId === "c1ac18b3-0f9b-43ab-a759-4d7f8b19fd55") {
            setComprehensive(item.ChildItems[0].ValData);
        }
        else if (item.ItemId === "711777d3-6c64-4a2b-b6cc-c9bd85f478a5") {
            setLot(item.ChildItems[0].ValData);
        }
        else if (item.ItemId === "f080205e-e107-4f23-badb-feae0a6e04df") {
            setHail(item.ChildItems[0].ValData);
        }
        else if (item.ItemId === "768e8084-2557-40d3-b66c-e95f7297dcb8") {
            setAName(item.ChildItems[0].ValData);
        }

    });
  }
  else if (data.coverageId === "7ade4350-9afb-4b33-b1af-1ded47f9bb30") {
    setown(true);
    console.log(data);
    setitemList(data.coveragedata[0].CoverageItems);
    setDefault(data.coveragedata[0].CoverageOtherItem);
    data.coveragedata[0].CoverageItems && data.coveragedata[0].CoverageItems.map((item)=>{
      // if (item.ItemId === "d4edafea-5b80-483a-8d3e-9e3f59a3e07c") {
      //     console.log("selectdata",item.ChildItems[0].ValData);
      //     setselected(item.ChildItems[0].ValData);
      // }

      if (item.ItemId === "e48c381f-a89f-472c-a36e-d86cae0968f2") {
          setselectedBenifits(item.ChildItems[0].ValData);
      }
      else if (item.ItemId === "927e5bc6-c3b4-4e83-b0b4-c7dfcfd7a814") {
          setOwnedAuto(item.ChildItems[0].ValData);
      }
      else if (item.ItemId === "782ba9e2-7561-4793-9f4c-d1596bb00886") {
          setComprehensive(item.ChildItems[0].ValData);
      }
      else if (item.ItemId === "1d8c06e4-a382-40fb-a1e0-71cbe3c855d6") {
          setLot(item.ChildItems[0].ValData);
      }
      else if (item.ItemId === "2ea210c9-d412-46df-8da1-ba0f46df1a81") {
          setHail(item.ChildItems[0].ValData);
      }
      else if (item.ItemId === "c3a2eef0-56b6-490a-9472-c74fac3f055c") {
          setAName(item.ChildItems[0].ValData);
      }

  });
}
}
    });

    BuildingLoad();
   // DataLoad();
  },[]);

  useEffect(()=>{
    console.log("SelectedItem",selected);
  },[selected])
  useEffect(()=>{
    setDefault1([]);
    setE2Default([]);
     let E1 = [];
    let E2 = [];
    let E3 = [];
    Default.map((dat)=>{
        if (dat.GroupName === "Customer Comprehensive") {
            //setE1Default([...E1Default, {dat}]); 
            E1.push(dat);
        }
        else if(dat.GroupName === "Other Details 2"){
            //setE2Default([...E2Default, {dat}]);  
            E2.push(dat);

        }


      });

      setE2Default(E2); 
      setDefault1(E1); 
      console.log("Default",Default);
      console.log("E1Default",Default1);
      console.log("E2Default",E2Default);

  },[Default]);
  var tableStyle = {
    "background": "#2e3f50",
    "color": "#fff",
    "text-transform": "capitalize",
 };
 var BorderStyle = {
    "border-right":"1px solid #333"
  };
  var withoutBorderStyle = {
    "border-right":"0px solid #fff"
  };
  return (
     <div>
    <h1>{props.location.state.id.policyType}</h1>

    <div class="table">
      <table>
        <thead>

          <th style={{width: '39%'}}>AUTO GARAGE LIABILITY</th>
          <th style={{width: '40%'}}>Coverage Info</th>
          <th></th>
          <th></th>
          
          
        </thead>
       <tbody>
     <React.Fragment>
         {/* <tr>
             <td>Coverage Questions</td>
             <td> </td>
             <td> </td>
             <td> </td>
          </tr> */}
    {itemList && itemList.map((x, i) => {

        if (x.GroupName === "Agl_Open") {
        return (
            
               <tr>
                  
                        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                        <td>
                            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                                if ((ch.ChildItemId === "2d32faac-dcb1-4f87-9cab-ddf4c8ac8155" || ch.ChildItemId === "4c6b29d5-e559-4b3e-aa6b-717d554c0056") && ch.LineInputType == 5) {
                                return(
                             
                            <FormControl variant="outlined" className="select">
                           <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                           <Select
                           labelId="demo-simple-select-outlined-label"
                           id="demo-simple-select-outlined"
                           name="ValData"  
                           value={ch.ValData}
                           onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                           label="Operations"
                           >
   
                           { Operations && Operations.map((item, index)=>{
                           return(
                           <MenuItem key={index} value={item.Operation}>{item.Operation}</MenuItem>)
   
                           })} 
   
                           </Select>
                        </FormControl> );
                                }
                            else if ((ch.ChildItemId === "6592c9c5-9f3b-4989-bdd0-fd08e01a5d58" || ch.ChildItemId === "5adfb018-1efc-4c84-a05a-f0f0e8c9d370" ) && ch.LineInputType === 5) {
                             return (
                            <FormControl variant="outlined" className="select">
                            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                            <Select
                            labelId="demo-simple-select-outlined-label"
                            id={ch.ChildItemId}
                            name="ValData"  
                            value={ch.ValData}
                            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                            label="Vehicle type"
                            >

                            { Vehicles && Vehicles.map((item, index)=>{
                            return(
                            <MenuItem key={index} value={item.Vehicle}>{item.Vehicle}</MenuItem>)

                            })} 

                            </Select>
                            </FormControl>
                        
                        );}
                        else if (ch.LineInputType === 5) {
                            return (
                           <FormControl variant="outlined" className="select">
                           <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                           <Select
                           labelId="demo-simple-select-outlined-label"
                           id={ch.ChildItemId}
                           name="ValData"  
                           value={ch.ValData}
                           onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                           label="Select"
                           >

                           { ifoptions && ifoptions.map((item, index)=>{
                           return(
                           <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                           })} 

                           </Select>
                           </FormControl>
                       
                       );}
                        else if (ch.LineInputType == 2) {
                            return(
                                <TextField  name="ValData" value={ch.ValData}
                               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
                        );}
                             })}
                        </td>
                        <td style={withoutBorderStyle}></td>
                        <td style={withoutBorderStyle}></td>
                      </tr>

                );
        }
         

      })}

      </React.Fragment>
             
        </tbody>
      </table>
    </div>

    <div class="table">
      <table>
        <thead>

          <th style={{width: '40%'}}>Coverage Details</th>
          <th style={{width: '20%'}}>Coverage Info</th>
          <th style={{width: '20%'}}>LIMIT</th>
          <th style={{width: '20%'}}>DEDUCTIBLE</th>
          <th></th>
          
        </thead>
       <tbody>
     <React.Fragment>
         {/* <tr>
             <td>Coverage Questions</td>
             <td> </td>
             <td> </td>
             <td> </td>
          </tr> */}
    {itemList && itemList.map((x, i) => {

        if (x.GroupName === "Coverage") {
        return (
            
               <tr>
                  
                        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                        <td>
                            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                            if (ch.LineInputType === 5) {
                             return (
                            <FormControl variant="outlined" className="select">
                            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                            <Select
                            labelId="demo-simple-select-outlined-label"
                            id={ch.ChildItemId}
                            name="ValData"  
                            value={ch.ValData}
                            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                            label="Select"
                            >

                            { ifoptions && ifoptions.map((item, index)=>{
                            return(
                            <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                            })} 

                            </Select>
                            </FormControl>
                        
                        );}
                        else if (ch.LineInputType == 2) {
                            return(
                                <TextField  name="ValData" value={ch.ValData}
                               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
                        );}
                             })}
                        </td>

                    <td style={BorderStyle}>
                    {x.IsLimit && x.LimitInputType == 2 &&
                    
                    // <TextField  name="Limit" value={x.Limit}
                    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

                    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
                     
                     }
                      </td>
                    <td >
                    {x.IsDeductible && x.DeductibleInputType == 2 &&
                    
        //             <TextField  name="Deductible" value={x.Deductible}
        // onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />
        
        <NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />
        }
            {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            name="Deductible"  
            value={x.Deductible}
            onChange={e => handleInputChange(e, i)}
            label="Deductible"
            >

            { DeAmount && DeAmount.map((item, index)=>{
            return(
            <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

            })} 

            </Select>
            </FormControl> }

                      </td>
                      <td></td>

                      </tr>

                );
        }
         

      })}

{itemList && itemList.map((x, i) => {

if (x.GroupName === "Coverage Accident Benefits") {
return (
    
       <tr>
          
                <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                <td>
                    {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                    if (ch.LineInputType === 5) {
                     return (
                    <FormControl variant="outlined" className="select">
                    <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                    <Select
                    labelId="demo-simple-select-outlined-label"
                    id={ch.ChildItemId}
                    name="ValData"  
                    value={ch.ValData}
                    onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                    label="Select"
                    >

                    { ifoptions && ifoptions.map((item, index)=>{
                    return(
                    <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                    })} 

                    </Select>
                    </FormControl>
                
                );}
                else if (ch.LineInputType == 2) {
                    return(
                        <TextField  name="ValData" value={ch.ValData}
                       onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
                );}
                     })}
                </td>

            <td style={BorderStyle}>
            {x.IsLimit && x.LimitInputType == 2 &&
            
            // <TextField  name="Limit" value={x.Limit}
            //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />
            
            <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} /> 
             }
              </td>
            <td >
            {x.IsDeductible && x.DeductibleInputType == 2 &&
            
//             <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

}
    {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
    <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
    <Select
    labelId="demo-simple-select-outlined-label"
    id="demo-simple-select-outlined"
    name="Deductible"  
    value={x.Deductible}
    onChange={e => handleInputChange(e, i)}
    label="Deductible"
    >

    { DeAmount && DeAmount.map((item, index)=>{
    return(
    <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

    })} 

    </Select>
    </FormControl> }

              </td>
              <td></td>

              </tr>

        );
}
 

})}

{selectedBenifits === "Yes" && <tr >
                    <td style={tableStyle}>Additional Questions</td>
                    <td></td>
                    <td style={BorderStyle}></td>
                    <td></td>
                    <td></td>
</tr> }

{ itemList && itemList.map((x,i)=> {

        if (x.GroupName === "Additional Questions" && selectedBenifits === "Yes") 
        {
            
            return (
                <tr>

                <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
                <td>
                    {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                        if ((ch.ChildItemId === "040a47a5-eff3-4dd4-814d-9f1207422f4d" || ch.ChildItemId === "a022edb6-5dc5-4e7d-9bb2-63bd77c59c64") && ch.LineInputType == 5) {
                        return(
                     
                    <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id="demo-simple-select-outlined"
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Amount"
                   >

                   { IAmounts && IAmounts.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.IAmount}>{item.IAmount}</MenuItem>)

                   })} 

                   </Select>
                </FormControl> );
                        }
                    else if ((ch.ChildItemId === "fd757fbf-a0a6-4023-a373-7b35740b8e6d" || ch.ChildItemId === "592d88bd-2024-4cd6-9e85-57a3bb1bd18d") && ch.LineInputType === 5) {
                     return (
                    <FormControl variant="outlined" className="select">
                    <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                    <Select
                    labelId="demo-simple-select-outlined-label"
                    id={ch.ChildItemId}
                    name="ValData"  
                    value={ch.ValData}
                    onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                    label="Amount"
                    >

                    { MAmounts && MAmounts.map((item, index)=>{
                    return(
                    <MenuItem key={index} value={item.MAmount}>{item.MAmount}</MenuItem>)

                    })} 

                    </Select>
                    </FormControl>
                
                );}
                else if (ch.LineInputType === 5) {
                    return (
                   <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id={ch.ChildItemId}
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Select"
                   >

                   { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })} 

                   </Select>
                   </FormControl>
               
               );}
                else if (ch.LineInputType == 2) {
                    return(
                        <TextField  name="ValData" value={ch.ValData}
                       onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
                );}
                     })}
                </td>

            <td style={BorderStyle}>
            {x.IsLimit && x.LimitInputType == 2 &&
            
            // <TextField  name="Limit" value={x.Limit}
            //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

            <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
             
             }
              </td>
            <td >
            {x.IsDeductible && x.DeductibleInputType == 2 &&
            
//             <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

}
    {x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
    <InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
    <Select
    labelId="demo-simple-select-outlined-label"
    id="demo-simple-select-outlined"
    name="Deductible"  
    value={x.Deductible}
    onChange={e => handleInputChange(e, i)}
    label="Deductible"
    >

    { DeAmount && DeAmount.map((item, index)=>{
    return(
    <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

    })} 

    </Select>
    </FormControl> }

              </td>
              <td></td>

              </tr>
 
                 );
            
        }
    })
}


<tr >
                    <td style={tableStyle}>Other Details</td>
                    <td></td>
                    <td style={BorderStyle}></td>
                    <td></td>
                    <td></td>
                    </tr>
{ itemList && itemList.map((x,i)=> {
if ((x.ItemId === "927e5bc6-c3b4-4e83-b0b4-c7dfcfd7a814" || x.ItemId === "0f668a79-4f1f-4f53-977f-21311f2dbfe1") && own) {
  if (x.GroupName === "Other Details") 
{
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if ((ch.ChildItemId === "cde0b426-d3ea-4165-a3e4-071493469362" || ch.ChildItemId === "b60d3cca-6208-4d18-ab18-243f10a7a9c3")  && ch.LineInputType === 5) {
                    return (
                   <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id={ch.ChildItemId}
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Select"
                   >

                    { Exclusions && Exclusions.map((item, index)=>{
                  return(
                        <MenuItem key={index} value={item.Exclusion}>{item.Exclusion}</MenuItem>)

                    })} 

                   </Select>
                   </FormControl>
               
               );}
               else if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label="Select"
            >

                { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>
      <td></td>

      </tr>

         );
    
}
}
else if (x.ItemId === "28f50b6c-25df-4658-9ae0-e52eb14d1e98" || x.ItemId === "305ce3a0-cac0-43fb-a373-1724890ce5a1") {
if (x.GroupName === "Other Details") 
{
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if ((ch.ChildItemId === "cde0b426-d3ea-4165-a3e4-071493469362" || ch.ChildItemId === "b60d3cca-6208-4d18-ab18-243f10a7a9c3")  && ch.LineInputType === 5) {
                    return (
                   <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id={ch.ChildItemId}
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Select"
                   >

                    { Exclusions && Exclusions.map((item, index)=>{
                  return(
                        <MenuItem key={index} value={item.Exclusion}>{item.Exclusion}</MenuItem>)

                    })} 

                   </Select>
                   </FormControl>
               
               );}
               else if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label="Select"
            >

                { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>
      <td></td>

      </tr>

         );
    
}
}
})
}


{ (own && OwnedAuto === "No (OEF76)") && <tr >
                    <td style={tableStyle}>Owned Auto Physical Damage</td>
                    <td></td>
                    <td style={BorderStyle}></td>
                    <td></td>
                    <td></td>
</tr> }

{ (own && itemList) && itemList.map((x,i)=> {

if ( OwnedAuto === "No (OEF76)" && x.GroupName === "Owned Auto Physical Damage") 
{
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.ChildItemId === "cde0b426-d3ea-4165-a3e4-071493469362" && ch.LineInputType === 5) {
                    return (
                   <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id={ch.ChildItemId}
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Select"
                   >

                    { ifoptions && ifoptions.map((item, index)=>{
                  return(
                        <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                    })} 

                   </Select>
                   </FormControl>
               
               );}
               else if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label=""
            >

                { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

<NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>
      <td></td>

      </tr>

         );
    
}
})
}

{ customer && <tr >
                    <td style={tableStyle} >Customer's Auto Phyical Damage</td>
                    <td></td>
                    <td style={BorderStyle}></td>
                    <td></td>
                    <td></td>
</tr> }


{ customer && itemList && itemList.map((x,i)=> {

if (x.GroupName === "Customer Auto Phyical Damage" ) 
{
  if(x.Limit == 0){
    x.Limit = '';
  }
  if(x.Deductible == 0){
    x.Deductible = '';
  }
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.ChildItemId === "cde0b426-d3ea-4165-a3e4-071493469362" && ch.LineInputType === 5) {
                    return (
                   <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id={ch.ChildItemId}
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Select"
                   >

                    { Exclusions && Exclusions.map((item, index)=>{
                  return(
                        <MenuItem key={index} value={item.Exclusion}>{item.Exclusion}</MenuItem>)

                    })} 

                   </Select>
                   </FormControl>
               
               );}
               else if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label="Select"
            >

                { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />

}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>
      <td></td>

      </tr>

         );
    
}
})
}

{Comprehensive === "Yes" && <tr >
                    <td style={tableStyle}>Customer Comprehensive</td>
                    <td></td>
                    <td style={BorderStyle}></td>
                    <td></td>
                    <td></td>
</tr> }

{ itemList && itemList.map((x,i)=> {

if ( Comprehensive === "Yes" && x.GroupName === "Customer Comprehensive") 
{
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.ChildItemId === "cde0b426-d3ea-4165-a3e4-071493469362" && ch.LineInputType === 5) {
                    return (
                   <FormControl variant="outlined" className="select">
                   <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
                   <Select
                   labelId="demo-simple-select-outlined-label"
                   id={ch.ChildItemId}
                   name="ValData"  
                   value={ch.ValData}
                   onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
                   label="Select"
                   >

                    { Exclusions && Exclusions.map((item, index)=>{
                  return(
                        <MenuItem key={index} value={item.Exclusion}>{item.Exclusion}</MenuItem>)

                    })} 

                   </Select>
                   </FormControl>
               
               );}
               else if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label="Select"
            >

                { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />

}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>
      <td></td>

      </tr>

         );
    
}
})
}

{ Comprehensive === "Yes" && Default1 && Default1.map((dat, index) => {
if (dat.GroupName === "Customer Comprehensive") {
return (
    
    <tr >
            {/* <td>
            <TextField  name="LineItem" value={dat.LineItem}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Select Location" variant="outlined" />
            </td> */}

            <td>

            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> Select Location </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={dat.Id}
            name="LineItem"  
            value={dat.LineItem}
            onChange={e => handleInputChangeother(e, index,dat.Id)}
            label=""
            >
                { building && building.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.buildingId}>{item.location}</MenuItem>)

                   })}

            </Select>
            </FormControl>

            </td>
            <td>
           
            </td>
            <td style={BorderStyle}>
            {/* <TextField  name="Limit" value={dat.Limit}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Limit" variant="outlined" /> */}

<NumberFormat class="numberft" name="Limit" value={dat.Limit} 
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
            </td>
            <td>
            {/* <TextField  name="Deductible" value={dat.Deductible}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Deductible" variant="outlined" /> */}
          
          <NumberFormat class="numberft" name="Deductible" value={dat.Deductible} 
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />
            </td>
            <td>
            <div className="btn-box" style={{position: "static"}}>
        {Default1.length !== 1 && <Button  type="button"
        className="mr10" variant="contained"
        onClick={() => handleRemoveClick(index,dat.Id)}><RemoveRoundedIcon /></Button>}
        {Default1.length - 1 === index && <Button type="button" 
        className={(Default1.length > 4) && "hi"} variant="contained" onClick={e=>handleAddClick(e,"Customer Comprehensive")}><AddRoundedIcon /></Button>}
    </div>
            </td>
            
    </tr>
        
    
); }

})}

<tr >
                    <td style={tableStyle}></td>
                    <td></td>
                    <td style={BorderStyle}></td>
                    <td></td>
                    <td></td>
</tr>

{ itemList && itemList.map((x,i)=> {

if (x.GroupName === "Other Details 2") 
{
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label="Select"
            >

                { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />

}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>
      <td></td>

      </tr>

         );
    
}
})
}

{ AName === "Yes" && E2Default && E2Default.map((dat, index) => {
if (dat.GroupName === "Other Details 2") {
return (
    
    <tr >
            <td>
            <TextField  name="LineItem" value={dat.LineItem}
            onChange={e => handleInputChangeother(e, index,dat.Id)} label="Name" variant="outlined" />
            </td>
            <td>
            <div className="btn-box" style={{position: "static"}}>
        {E2Default.length !== 1 && <Button  type="button"
        className="mr10" variant="contained"
        onClick={() => handleRemoveClick(index,dat.Id)}><RemoveRoundedIcon /></Button>}
        {E2Default.length - 1 === index && <Button type="button" 
        className={(E2Default.length > 4) && "hi"} variant="contained" onClick={e=>handleAddClick(e,"Other Details 2")}><AddRoundedIcon /></Button>}
        </div>
            </td>
            <td style={BorderStyle}>
            
            </td>
            <td>
           
            </td>
            <td></td>

            
    </tr>
        
    
); }

})}

<tr >
                    <td style={tableStyle}></td>
                    <td></td>
                    <td style={BorderStyle}></td>
                    <td></td>
                    <td></td>
</tr>

{ itemList && itemList.map((x,i)=> {

if (x.GroupName === "Other Details 3" && (x.ItemId === "b6446003-175c-40fa-a053-c6dd1f8a5005" || x.ItemId === "d0a82c11-ebef-47fc-b06c-03b368c255f3" 
|| x.ItemId === "f080205e-e107-4f23-badb-feae0a6e04df" || x.ItemId === "66e1b69a-806d-4634-a905-ea9d354a68c1" || x.ItemId === "51e4aeda-fbec-4482-a2ab-037768d5975d" 
|| x.ItemId === "2ea210c9-d412-46df-8da1-ba0f46df1a81"))
{
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label="Select"
            >

                { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>
      <td></td>

      </tr>

         );
    
}
})
}

{ itemList && itemList.map((x,i)=> {

if ( Hail === "Yes" && x.GroupName === "Hail" )
{
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label="Select"
            >

                { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>
      <td></td>

      </tr>

         );
    
}
})
}

{ itemList && itemList.map((x,i)=> {

if (x.GroupName === "Other Details 3" && (x.ItemId === "711777d3-6c64-4a2b-b6cc-c9bd85f478a5" || x.ItemId === "1d8c06e4-a382-40fb-a1e0-71cbe3c855d6" ))
{
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label="Select"
            >

                { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />
}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>

      <td></td>

      </tr>

         );
    
}
})
}

{ itemList && itemList.map((x,i)=> {

if ( Lot === "Yes" && x.GroupName === "Open Lot Pilferage" )
{
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label="Select"
            >

                { ifoptions && ifoptions.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.section}>{item.section}</MenuItem>)

                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
                    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />
}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>
      <td></td>

      </tr>

         );
    
}
})
}

{ itemList && itemList.map((x,i)=> {

if (x.GroupName === "Other Details 3" && (x.ItemId === "0375a889-6423-4f80-a203-f456e1b8bec1" || x.ItemId === "d3cab782-add8-4087-9082-a47d05b70d95" ))
{
    return (
    
        <tr>
          
        <td>{x.LineInputType == 1 && <label> {x.LineItem} </label>}</td>
        <td>
            {x.ChildItems && x.ChildItems.map((ch,ind)=> {
                if (ch.LineInputType === 5) {
             return (
            <FormControl variant="outlined" className="select">
            <InputLabel id="demo-simple-select-outlined-label"> </InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id={ch.ChildItemId}
            name="ValData"  
            value={ch.ValData}
            onChange={e => handleInputChangeSub(e,ind,x.ItemId)}
            label="Amount"
            >

                { limitAmount && limitAmount.map((item, index)=>{
                   return(
                   <MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)
                   })}

            </Select>
            </FormControl>
        
        );}
        else if (ch.LineInputType == 2) {
            return(
                <TextField  name="ValData" value={ch.ValData}
               onChange={e => handleInputChangeSub(e,ind,x.ItemId)} label="" variant="outlined" /> 
        );}
             })}
        </td>

    <td style={BorderStyle}>
    {x.IsLimit && x.LimitInputType == 2 &&
    
    // <TextField  name="Limit" value={x.Limit}
    //  onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" />

    <NumberFormat class="numberft" name="Limit" value={x.Limit} 
    onChange={e => handleInputChange(e, i)} label="Limit" variant="outlined" thousandSeparator={true} prefix={'$'} />
     
     }
      </td>
    <td >
    {x.IsDeductible && x.DeductibleInputType == 2 &&
    
//     <TextField  name="Deductible" value={x.Deductible}
// onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" />

<NumberFormat class="numberft" name="Deductible" value={x.Deductible} 
                    onChange={e => handleInputChange(e, i)} label="Deductible" variant="outlined" thousandSeparator={true} prefix={'$'} />

}
{x.IsDeductible && x.DeductibleInputType == 5 && <FormControl variant="outlined" className="select">
<InputLabel id="demo-simple-select-outlined-label">Deductible</InputLabel>
<Select
labelId="demo-simple-select-outlined-label"
id="demo-simple-select-outlined"
name="Deductible"  
value={x.Deductible}
onChange={e => handleInputChange(e, i)}
label="Deductible"
>

{ DeAmount && DeAmount.map((item, index)=>{
return(
<MenuItem key={index} value={item.Amount}>{item.Amount}</MenuItem>)

})} 

</Select>
</FormControl> }

      </td>
      <td></td>

      </tr>

         );
    
}
})
}
      </React.Fragment>
             
        </tbody>
      </table>
    </div>
         <div className="btn-group">

         {props.location.state.total === props.location.state.index + 1 && <Button variant="contained" name="cancel" type="button" onClick={onClear}> 
        CANCEL
        </Button>}

        {props.location.state.total === props.location.state.index + 1 &&<Button variant="contained" name="save" color="primary" type="button" onClick={onSubmission}>

        SAVE
        </Button>}
                </div>
  </div>
  
  
  );


    // return(<h1>{props.location.state.id}</h1>)
}
export default AGLCoverage