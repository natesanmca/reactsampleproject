import React from 'react';

import { DataGrid, Column, Editing, Scrolling, Lookup, Summary, TotalItem,SearchPanel } from 'devextreme-react/data-grid';
//import { Button } from 'devextreme-react/button';
import Button from '@material-ui/core/Button';

import CustomStore from 'devextreme/data/custom_store';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { formatDate } from 'devextreme/localization';
import 'whatwg-fetch';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import axios from 'axios';
import history from '../../../../history.js'
import { useHistory,Link } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit';
import { useSelector, useDispatch,connect } from "react-redux";
import Addclamis from './addclaims.js';

const URL = 'http://13.126.250.89:4430/api/Policy/get-notes-list-by-account/57204955';
const UpdateURL = "http://13.126.250.89:4430/api/Policy/";

const REFRESH_MODES = ['full', 'reshape', 'repaint'];

const API_URL = process.env.REACT_APP_BASE_API_URL;
//console.log("allstate",allstate.login.user.UserId);
class Claim extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ordersData: new CustomStore({
        key: 'NoteId',
        load: () => this.sendRequest(`${API_URL}Policy/get-notes-list-by-account/${this.props.Account.AcNo}`),

      }),
      requests: [],
      refreshMode: 'reshape'
    };

    this.clearRequests = this.clearRequests.bind(this);
    this.handleRefreshModeChange = this.handleRefreshModeChange.bind(this);
    this.state = {
        clicked: false
      };
      
      this.handleClick = this.handleClick.bind(this);
  }
  

  sendRequest(url, method, data) {
     // alert(url);
    method = method || 'GET';
    data = data || {};
    console.log("data");
    
    this.logRequest(method, url, data);

    if(method === 'GET') {
      return fetch(url, {
        method: method,
        headers: { 'Content-Type': 'application/json' }
        // credentials: 'include'
      })
      .then(this.handleResponse, this.handleError)
        .then(user => {
            // login successful if there's a jwt token in the response
            if (user && user.Success) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //localStorage.setItem('user', JSON.stringify(user));
            }
            return user.Data;
            
        });
      
    }
    console.log(data.values.UserType);
    const params = Object.keys(data).map((key) => {
      return `${encodeURIComponent(key) }=${ encodeURIComponent(data[key])}`;
    }).join('&');

    return fetch(url, {
      method: method,
      body: params,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      credentials: 'include'
    }).then(result => {
      if(result.ok) {
        return result.text().then(text => text && JSON.parse(text));
      } else {
        return result.json().then(json => {
          throw json.Message;
        });
      }
    });
  }
   handleResponse(response) {
    return new Promise((resolve, reject) => {
        if (response.ok) {
            // return json if it was returned in the response
            var contentType = response.headers.get("content-type");
            if (contentType && contentType.includes("application/json")) {
                response.json().then(json => resolve(json));
            } else {
                resolve();
            }
        } else {
            // return error message from response body
            response.text().then(text => reject(text));
        }
    });
}

 handleError(error) {
    return Promise.reject(error && error.message);
}
  logRequest(method, url, data) {
    const args = Object.keys(data || {}).map(function(key) {
      return `${key }=${ data[key]}`;
    }).join(' ');

    const time = formatDate(new Date(), 'HH:mm:ss');
    const request = [time, method, url.slice(URL.length), args].join(' ');

    this.setState((state) => {
      return { requests: [request].concat(state.requests) };
    });
  }

  clearRequests() {
    this.setState({ requests: [] });
  }

  handleRefreshModeChange(e) {
    this.setState({ refreshMode: e.value });
  }
  //const history1 = useHistory();
  adduser() {
    //const history1 = useHistory();
    history.push('/UserRegistration');
  }
  sayHello() {
    alert('Hello!');
    return <Addclamis />
  }
  handleClick() {
    this.setState({
      clicked: true
    });
  }
  render() {
    const { refreshMode, ordersData, TypessData, StatusData } = this.state;
    return (
      <React.Fragment>
        <Container maxWidth="">
            <Grid container >
        <Grid item md={12}>
          <div className="mainContent"><h1>All Notes</h1></div>
        </Grid>
        </Grid>
        <Paper elevation={3} className="paper">
        <Grid container>
        <Container maxWidth="xl">
            <Grid item md={12}>
                
                <div className="mainContent">
                {/* <Link to='/UserRegistration'>
                <Button id="add"
                        text="ADD USER"
                        height={34}
                        onClick={this.adduser}
                    />
                 </Link> */}
                 
                    <Button onClick={this.handleClick} variant="contained" color="primary">
                        + Create Claim
                    </Button>
                   
                   
   
      
                    <DataGrid
                        id="gridContainer"
                        dataSource={ordersData}
                        // keyExpr="UserId"
                        showBorders={true}
                        showRowLines={true}
                        showColumnLines={false}>
                    <SearchPanel visible={true} highlightCaseSensitive={true} />
                   
                            <Column dataField="Description" caption="Serial No" />
                            <Column dataField="Producer" caption="Type of Loss" />
                            <Column dataField="CreatedBy" caption="Loss Detaill" />
                            <Column dataField="CreatedBy" caption="Loss Amount" />
                            <Column dataField="NoteId" caption="Edit" cellRender={Editcell} />
                           
                    </DataGrid>
                    
                </div>
                {this.state.clicked ? <Addclamis /> : null}
            </Grid>
            </Container>
        </Grid>
   
    </Paper>
   </Container>
      </React.Fragment>
    );
  }
  

}
function Editcell(data) {
  // return <img src={data.value} />/users/${user.id} activeClassName="active"
  
  return <Link to={{pathname:`/myaccounts/accountDetails/Createnote`, state:{id:data.value}}} ><EditIcon /></Link>
}

const mapStateToProps = (state) => {
  return {
      Account: state.Account 
  };
}
export default connect(mapStateToProps) (Claim);


