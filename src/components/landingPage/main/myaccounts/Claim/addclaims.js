import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import DateFnsUtils from '@date-io/date-fns';
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';
import AddRoundedIcon from '@material-ui/icons/AddRounded';
import RemoveRoundedIcon from '@material-ui/icons/RemoveRounded';
import {useSelector, useDispatch} from "react-redux";
//import { fetchOwnername } from "../../../../Redux/Actions/actions";
import FormControl from '@material-ui/core/FormControl';

import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import { useHistory,Link} from "react-router-dom";
import MenuItem from '@material-ui/core/MenuItem';
import axios from 'axios';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
  } from '@material-ui/pickers';
  import {useAlert,positions,Provider as AlertProvider} from 'react-alert'
  import { v4 as uuidv4 } from 'uuid';
  const API_URL = process.env.REACT_APP_BASE_API_URL;


const Addclamis=(props)=> {
  const Account = useSelector(state=>state.Account);
  const [NoteDate, setNoteDate] = React.useState(new Date());
  const [ownerList, setOwnerList] = useState([{ ClaimID: uuidv4(),AcNo: Account.AcNo, LossType: "", claimDate: NoteDate, LossAmount:"",LossDetails:""}]);
  //const OwnerName = useSelector(state=>state);
  
  const [Typeoflosss, setTypeoflosss] = useState('');
  const alert = useAlert();
  const handleQuoteExpiryDate = (date) => {
  //  alert(i);
  console.log("event",date);
  setNoteDate(date);
  setOwnerList(userInput => ({
      ...ownerList, 
      ['NoteDate']: date
  }));
  };
  const [count, setCount] = useState(1);
  //const dispatch = useDispatch();
  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...ownerList];
    list[index][name] = value;
    setOwnerList(list);
    
  };
  const TypeofLoss = ([
    {code:"None"},{code:"Flood"},{code:"DOCS"},{code:"DECS"}
    ]); 
  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...ownerList];
    list.splice(index, 1);
    setOwnerList(list);
    setCount(prevCount => prevCount - 1);
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    setOwnerList([...ownerList, { ClaimID: uuidv4(),AcNo: Account.AcNo, LossType: "", claimDate: NoteDate, LossAmount:"",LossDetails:""}]);  
    setCount(prevCount => prevCount + 1);
  };
  const [userInput, setUserInput] = useState(
    {
      Typeoflosss: '',
      NoteDate:NoteDate,
      LossAmount:'',
      LossDetaill:''
    }
    );
const handleChange = evt => {
  
const name = evt.target.name;
console.log("data",name);
let newValue = evt.target.value;
setUserInput(userInput => ({
  ...userInput, 
  [name]: newValue
})
);
}
  let history = useHistory();
const createusersubmit = (evt) => {
  evt.preventDefault();
  axios.post(`${API_URL}Policy/create-claim`, ownerList)
  .then(res => {
    
      if(res.data.Success){    
          //let bdmList = res.data.Data;
          alert.success(res.data.Message,{
            timeout:5000,
            onClose: () => {
             history.push("/myaccounts/accountDetails/Claim");
            }
          });
          //alert(res.data.Message);
       //  console.log(res.data.Message);
       //  history.push("/UserComp");
        }
        else {
         alert.info(res.data.Message);
        }

  // console.log(operationList);
   

});
}
  useEffect(()=>{
    if (props.id !== "0" && props.id !== "") {
      
        
        axios.post(`${API_URL}Policy/get-claim/${props.id}`)
        .then(res => {
          
            if(res.data.Success){    
              setOwnerList(res.data.Data);
              }
              else {
              console.log(res.data);
              }
    
        // console.log(operationList);
        console.log(res.data);
      
      });
     }
   },[props])
  return (
    <div>
      <form className="field"
        onSubmit={createusersubmit}
        autoComplete="off">
      <Grid item md={12}>
        <p class="statusP"><u>Create New Claim</u></p>
            </Grid>
            <Grid item md={12}>
        <p class="statusP">No of claim {count}</p>
            </Grid>
    
      {ownerList.map((x, i) => {
        return (
            <div className="layoutsplit">
            <Grid container spacing={3} >
            <Grid item md={3}>
      <FormControl variant="outlined" className="select">
      <InputLabel id="demo-simple-select-outlined-label">Type of Loss</InputLabel>
      <Select
        labelId="demo-simple-select-outlined-label"
        id="LossType"
        name="LossType"
        value={x.LossType}
        onChange={e => handleInputChange(e, i)}
        label="Type of Loss"
      >
        
        { TypeofLoss && TypeofLoss.map((item, index)=>{
                    return(
                    <MenuItem key={index} value={item.code}>{item.code}</MenuItem>)

                    })}  
       
      </Select>
    </FormControl>
      </Grid>
      <Grid item md={3}>
      <MuiPickersUtilsProvider  utils={DateFnsUtils}>
    <Grid container justify="space-around" className="datePicker">
      <KeyboardDatePicker
        disableToolbar
        variant="inline"
        format="MM/dd/yyyy"
        margin="normal"
        name="claimDate"
        id="claimDate"
        label="Date"
        value={x.claimDate}
       onChange={e =>handleQuoteExpiryDate}
        KeyboardButtonProps={{
          'aria-label': 'change date',
        }}
      />
     </Grid>
  </MuiPickersUtilsProvider>
      </Grid>
   
                    <Grid item md={3}>
                    <TextField  name="LossAmount" id="LossAmount" value={x.LossAmount}
                    onChange={e => handleInputChange(e, i)} label="Loss Amount" variant="outlined" />
                    </Grid>
                    <Grid item md={3}>
                    <TextField   name="LossDetails" id="LossDetails" value={x.LossDetails}
                    onChange={e => handleInputChange(e, i)} label="Loss Detaill" variant="outlined" />
                    </Grid>
            </Grid>
             <div className="btn-box">
             {ownerList.length !== 1 && <Button  type="button"
               className="mr10" variant="contained"
               onClick={() => handleRemoveClick(i)}><RemoveRoundedIcon /></Button>}
             {ownerList.length - 1 === i && <Button type="button" 
             className={(ownerList.length > 4) && "hi"} variant="contained" onClick={handleAddClick}><AddRoundedIcon /></Button>}
           </div>
           </div>
        );
      })}

      {/* <div style={{ marginTop: 20 }}>{JSON.stringify(ownerList)}</div> */}
      <div className="btn-group">
      <Link to={{pathname:`/myaccounts/accountDetails/Createnote`, state:{id:'0'}}}> <Button variant="contained">CANCEL</Button> </Link>
<Button variant="contained" color="primary" type="submit">
  SAVE
</Button>
           </div>
           </form>
    </div>
   
  );
}

export default Addclamis;
