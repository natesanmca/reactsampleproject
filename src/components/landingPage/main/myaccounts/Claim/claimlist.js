import React, { useReducer, useState, useEffect } from 'react';

import { DataGrid, Column, Editing, Scrolling, Lookup, Summary, TotalItem,SearchPanel } from 'devextreme-react/data-grid';
//import { Button } from 'devextreme-react/button';
import Button from '@material-ui/core/Button';
import { SelectBox } from 'devextreme-react/select-box';

import CustomStore from 'devextreme/data/custom_store';
import DataSource from "devextreme/data/data_source";
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { formatDate } from 'devextreme/localization';
import 'whatwg-fetch';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import axios from 'axios';
import history from '../../../../../history'
import { useHistory,Link } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import {useSelector, useDispatch,connect} from "react-redux";
import { fetchSubmissionNumber, fetchCheckState, fetchSelectedPolicy, fetchTimeLine,fetchType, fetchQuotationNo, fetchInsurerInfo, fetchBindingInfo, 
    fetchInsurerInfofilesview, fetchBindingInfofilesview, fetchStatusType  } from '../../../../../Redux/Actions/actions';
import Addclamis from './addclaims';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
    } from '@material-ui/pickers';
    import {useAlert,positions,Provider as AlertProvider} from 'react-alert'
    import { v4 as uuidv4 } from 'uuid';
    import TextField from "@material-ui/core/TextField";
    import DateFnsUtils from '@date-io/date-fns';
    import AddRoundedIcon from '@material-ui/icons/AddRounded';
import RemoveRoundedIcon from '@material-ui/icons/RemoveRounded';

const API_URL = process.env.REACT_APP_BASE_API_URL;
            const styles = (theme) => ({
                root: {
                  margin: 0,
                  padding: theme.spacing(2),
                },
                closeButton: {
                  position: 'absolute',
                  right: theme.spacing(1),
                  top: theme.spacing(1),
                  color: theme.palette.grey[500],
                },
              });
              
const DialogTitle = withStyles(styles)((props) => {
const { children, classes, onClose, ...other } = props;
return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
    <Typography variant="h6">{children}</Typography>
    {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
        <CloseIcon />
        </IconButton>
    ) : null}
    </MuiDialogTitle>
);
});
              
              const DialogContent = withStyles((theme) => ({
                root: {
                  padding: theme.spacing(2),
                },
              }))(MuiDialogContent);
              
              const DialogActions = withStyles((theme) => ({
                root: {
                  margin: 0,
                  padding: theme.spacing(1),
                },
              }))(MuiDialogActions);
              const startDate = new Date();
              function disablePrevDates(startDate) {
                const startSeconds = Date.parse(startDate);
                return (date) => {
                  return Date.parse(date) > startSeconds;
                }
              }
const ClaimList = () => {
    //const users = useSelector(state => state.users.items);
    const Account = useSelector(state=>state.Account);
    const [hasError, setErrors] = useState(false);
    const [claimid, setclaimid] = useState("0");
    const [users, setusers] = useState({});
    const [clicked,setclicked]=useState(false);
    const [NoteDate, setNoteDate] = React.useState(new Date());
    const [ownerList, setOwnerList] = useState([{ ClaimID: uuidv4(),AcNo: Account.AcNo, LossType: "", claimDate: NoteDate, LossAmount:"",LossDetails:""}]);
    const [ordersData,setordersData]=useState([]);
    const [userInput, setUserInput] = useReducer(
      (state, newState) => ({...state, ...newState}),
      {
        ClaimID: uuidv4(),
        AcNo : Account.AcNo,
        LossType : '',
        claimDate : NoteDate,
        LossAmount:'',
        LossDetails : '',
        Adjuster : '',
        Status : '',
        Insurer : ''
      }
    );
    const defaultuserInput =  {
      ClaimID: uuidv4(),
        AcNo : Account.AcNo,
        LossType : '',
        claimDate : NoteDate,
        LossAmount:'',
        LossDetails : '',
        Adjuster : '',
        Status : '',
        Insurer : ''
      }
    const [status, setstatus] = useState([{
        
        'Status': 'Open'
      },
      {
          
          'Status': 'Close'
        }]);
    const dispatch = useDispatch();
    const pageSizes = [5, 10, 20];
    
    //  const Datasource = { 
    //       ordersData: new CustomStore({
    //         key: 'ClaimID',
    //         load: () => updateData(`${API_URL}Policy/get-cliams-list-by-account/${Account.AcNo}`)
    //           }) 
    // };
     
      const updateData = (url,data) => {
   
        // const Bdata = JSON.stringify({
        //   Acno : data.Acno
        // });
        
        const requestOptions = {
          method: 'GET',
          headers: { 'Content-Type': 'application/json' }
          //body: Bdata
          };
          return fetch(url, requestOptions)
          .then(handleResponse, handleError)
            .then(user => {
                // login successful if there's a jwt token in the response
                if (user && user.Success) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    //localStorage.setItem('user', JSON.stringify(user));
                }
                return user.Data;
                
            });
      }

      
        const handleResponse =(response) => {
        return new Promise((resolve, reject) => {
            if (response.ok) {
                // return json if it was returned in the response
                var contentType = response.headers.get("content-type");
                if (contentType && contentType.includes("application/json")) {
                    response.json().then(json => resolve(json));
                } else {
                    resolve();
                }
            } else {
                // return error message from response body
                response.text().then(text => reject(text));
            }
        });
    }
    
    const handleError = (error) => {
        return Promise.reject(error && error.message)
    }
     
    const [Typeoflosss, setTypeoflosss] = useState('');
  const alert = useAlert();
  const handleQuoteExpiryDate = (date) => {
  //  alert(i);
  console.log("event",date);
  setNoteDate(date);
//   setUserInput(userInput => ({
//     ...userInput, 
//     ['claimDate']: date
// }));
setUserInput({['claimDate']: date});

  };
  const [count, setCount] = useState(1);
  //const dispatch = useDispatch();
  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...ownerList];
    list[index][name] = value;
    setOwnerList(list);
    
  };

  const handleChange = evt => {
    const name = evt.target.name;
    let  newValue = evt.target.value;
    setUserInput({[name]: newValue});
  }

  const TypeofLoss = ([
    {code:"None"},{code:"Flood"},{code:"DOCS"},{code:"DECS"}
    ]); 
  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...ownerList];
    list.splice(index, 1);
    setOwnerList(list);
    setCount(prevCount => prevCount - 1);
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    setOwnerList([...ownerList, { ClaimID: uuidv4(),AcNo: Account.AcNo, LossType: "", claimDate: NoteDate, LossAmount:"",LossDetails:""}]);  
    setCount(prevCount => prevCount + 1);
  };
    const Editcell = (data) => {
        // return <img src={data.value} />/users/${user.id} activeClassName="active"
        // setclaimid(data.value);
        // setclicked(true);
       // return <Link to={{pathname:`/myaccounts/accountDetails/Createnote`, state:{id:data.value}}} ><EditIcon /></Link>
      };
     const handleClick= (event)=> {
      setUserInput(defaultuserInput);
        setclicked(true);
      }

      useEffect(()=>{
        setclicked(false);
        handleClose();
        loadData();
      },[]);

      const handleClose = () => {
        setclicked(false);
      };
      const save =()=>{
        axios.post(`${API_URL}Policy/create-claim`, userInput)
        .then(res => {
          
            if(res.data.Success){    
                //let bdmList = res.data.Data;
                alert.success(res.data.Message,{
                  timeout:5000,
                  onClose: () => {
                   setordersData(res.data.Data); 
                   handleClose();
                  }
                });
                //alert(res.data.Message);
             //  console.log(res.data.Message);
             //  history.push("/UserComp");
              }
              else {
               alert.info(res.data.Message);
              }
      
        // console.log(operationList);
         
      
      });
      };
      //`${API_URL}Policy/get-cliams-list-by-account/${Account.AcNo}`
      const editdata=(e)=>{
        console.log("event",e);
        axios.post(`${API_URL}Policy/get-claim/${e.row.data.ClaimID}`)
        .then(res => {
          
            if(res.data.Success){    
                
              //   let E1 = [];

              //   res.data.Data.map((dat)=>{
              //       E1.push(dat);

              // });
              console.log("Edit Data",res.data);
              setUserInput(res.data.Data[0]);
              // setCount(1);
              setclicked(true);
              }
              else {
              console.log(res.data);
              }
    
        // console.log(operationList);
        console.log("response",res.data);
      
      });
      };

      const loadData=()=>{
        
        axios.get(`${API_URL}Policy/get-cliams-list-by-account/${Account.AcNo}`)
        .then(res => {
          
            if(res.data.Success){    
                
              //   let E1 = [];

              //   res.data.Data.map((dat)=>{
              //       E1.push(dat);

              // });
              console.log("load Data",res.data);
              setordersData(res.data.Data);
               }
              else {
              console.log(res.data);
              }
    
        // console.log(operationList);
        console.log("response",res.data);
      
      });
      };
      function cellRender(data) {
      let num =  new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD'
        }).format(data.data.LossAmount);
        
      return num
      }

  return(
<React.Fragment>
        <Container maxWidth="">
            <Grid container >
        <Grid item md={12}>
          <div className="mainContent"><h1>All Claims</h1></div>
        </Grid>
        </Grid>
        <Paper elevation={3} className="paper">
        <Grid container>
        <Container maxWidth="xl">
            <Grid item md={12}>
                
                <div className="mainContent">
                {/* <Link to='/UserRegistration'>
                <Button id="add"
                        text="ADD USER"
                        height={34}
                        onClick={this.adduser}
                    />
                 </Link> */}
                 
                    <Button onClick={handleClick} variant="contained" color="primary">
                        + Create Claim
                    </Button>

                    <DataGrid
                        id="gridContainer"
                        dataSource={ordersData}
                        // keyExpr="UserId"
                        showBorders={true}
                        showRowLines={true}
                        showColumnLines={false}
                        wordWrapEnabled = {true}>
                    <SearchPanel visible={true} highlightCaseSensitive={true} />
                            <Column dataField="LossType" caption="Type of Loss"  />
                            <Column dataField="LossAmount" caption="Loss Amount" cellRender={cellRender} />
                            <Column dataField="claimDate" dataType="date" caption="Date" />
                            <Column dataField="LossDetails" caption="Loss Detaill" />
                            <Column dataField="Insurer" caption="Insurer" />
                            <Column caption="Actions" type="buttons" 
                            
                              buttons={[
                                {
                                hint: 'Click to Edit',
                                icon: 'edit',
                                onClick: (e) => { editdata(e) }
                               }
                               ]} />
                           
                    </DataGrid>
                    
                </div>
                {/* {clicked && <Addclamis id={claimid} />} */}
            </Grid>
            </Container>
        </Grid>
   
    </Paper>
   </Container>
   <Dialog onClose={handleClose}  className="buldingModal" aria-labelledby="customized-dialog-title" open={clicked}>
   <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Add Claim
        </DialogTitle>
        <DialogContent dividers>
 
        <Grid container spacing={3}>
        <Grid item md={4}>
       
      <FormControl variant="outlined" className="select">
      <InputLabel id="demo-simple-select-outlined-label">Type of Loss</InputLabel>
      <Select
        labelId="demo-simple-select-outlined-label"
        id="LossType"
        name="LossType"
        value={userInput.LossType}
        onChange={e => handleChange(e)}
        label="Type of Loss"
      >
        
        { TypeofLoss && TypeofLoss.map((item, index)=>{
                    return(
                    <MenuItem key={index} value={item.code}>{item.code}</MenuItem>)

                    })}  
       
      </Select>
    </FormControl>
    
        </Grid>
        <Grid item md={4}>
        <MuiPickersUtilsProvider  utils={DateFnsUtils}>
    <Grid container justify="space-around" className="datePicker">
      <KeyboardDatePicker
        disableToolbar
        variant="inline"
        format="MM/dd/yyyy"
        margin="normal"
        name="claimDate"
        id="claimDate"
        label="claimDate"
        shouldDisableDate={disablePrevDates(startDate)}
        value={userInput.claimDate}
       onChange={handleQuoteExpiryDate}
        KeyboardButtonProps={{
          'aria-label': 'change date',
        }}
      />
     </Grid>
  </MuiPickersUtilsProvider>
        </Grid>
        <Grid item md={4}>
        <TextField  name="LossAmount" id="LossAmount" value={userInput.LossAmount}
                    onChange={e => handleChange(e)} label="Loss Amount" variant="outlined" />
        </Grid>
        </Grid>
        <Grid container spacing={3}>
        <Grid item md={4}>
        <TextField   name="Adjuster" id="Adjuster" value={userInput.Adjuster}
                    onChange={e => handleChange(e)} label="Adjuster" variant="outlined" />
        </Grid>

        <Grid item md={4}>
        <TextField   name="Insurer" id="Insurer" value={userInput.Insurer}
                    onChange={e => handleChange(e)} label="Insurer" variant="outlined" />
        </Grid>

        <Grid item md={4}>
        <FormControl variant="outlined" className="select">
      <InputLabel id="demo-simple-select-outlined-label">Status</InputLabel>
      <Select
        labelId="demo-simple-select-outlined-label"
        id="Status"
        name="Status"
        value={userInput.Status}
        onChange={e => handleChange(e)}
        label="Status"
      >
        
        { status && status.map((item, index)=>{
                    return(
                    <MenuItem key={index} value={item.Status}>{item.Status}</MenuItem>)

                    })}  
       
      </Select>
    </FormControl>
        </Grid>
        </Grid>
      
        <Grid container spacing={3}>
        <Grid item md={12}>
        {/* <TextField   name="LossDetails" id="LossDetails" value={userInput.LossDetails}
                    onChange={e => handleChange(e)} label="Loss Detaill" variant="outlined" /> */}
                    <div className="description">
                    
          <textarea id="LossDetails" name="LossDetails" value={userInput.LossDetails} onChange={handleChange}  placeholder="LossDetails"></textarea>
          </div>
        </Grid>
        </Grid>

        </DialogContent>
        <DialogActions>
        <div className="btn-group">
           <Button variant="contained" onClick={handleClose}>CANCEL</Button>
<Button variant="contained" color="primary" autoFocus onClick={save}>
  SAVE
</Button>
           </div>
         
        </DialogActions>
        {/* <Addclamis id={claimid} /> */}
   </Dialog>
      </React.Fragment>
  
  );
  
};
  export default ClaimList;