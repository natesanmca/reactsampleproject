import React,{useState, useEffect, useReducer} from 'react';
import axios from 'axios';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import { DataGrid, Column, Editing, Scrolling, Lookup, Summary, TotalItem,SearchPanel } from 'devextreme-react/data-grid';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import FormControl from '@material-ui/core/FormControl';
import DateFnsUtils from '@date-io/date-fns';
import InputLabel from '@material-ui/core/InputLabel';
import {useAlert,positions,Provider as AlertProvider} from 'react-alert'
import {
MuiPickersUtilsProvider,
KeyboardTimePicker,
KeyboardDatePicker,
} from '@material-ui/pickers';
import { useSelector, useDispatch } from "react-redux";
import { v4 as uuidv4 } from 'uuid';
import Select from '@material-ui/core/Select';
import { useHistory } from "react-router-dom";
import FileViewe from './fileview';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import FormLabel from '@material-ui/core/FormLabel';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
const API_URL = process.env.REACT_APP_BASE_API_URL;

const CreatecertificateInsurance = (props) => {
  const alert = useAlert();
  //console.log("bdm", props)
const [selectedFiles, setSelectedFiles] = useState([]);
const [currentFile, setCurrentFile] = useState([]);
const [fileInfos, setFileInfos] = useState([]);
const [NoteDate, setNoteDate] = React.useState(new Date());
const [DueDate, setDueDate] = React.useState(new Date());
const [createdby, setcreatedby] = useState('');
const [Accountname, setAccountname] = useState('');
const [description, setdescription] = useState('');
// const [producer, setproducer] = useState('');
const [notes, setnotes] = useState('');
const [selectedDate, setSelectedDate] = React.useState(new Date());
const [producer,setproducer] = React.useState([]);
const [Inforce,setInforce] = React.useState([]);
const [Holder,setHolder] = React.useState([]);
const allstate = useSelector((state) => state);
const [certificateId,setid]= React.useState('0');
const BussinessDetails = useSelector(state=>state);
const [disable, setdisable] = React.useState(false);
const [open, setOpen] = React.useState(false);

console.log("stats",allstate);

const API_URL = process.env.REACT_APP_BASE_API_URL;
const handleQuoteExpiryDate = (date) => {
  setNoteDate(date);
  setUserInput(userInput => ({
    ...userInput, 
    ['NoteDate']: date
}));
};
const handleDuedate = (date) => {
  setDueDate(date);
  setUserInput(userInput => ({
    ...userInput, 
    ['DueDate']: date
}));
};

const handleClose = () => {  
  if(allstate.Account.AcNo != ""){
    history.push("/myaccounts/accountDetails/Certificate");
    }else{
     history.push("/List");
    }
};

const NoteCodes = ([
  {code:"None"},{code:"APPS"},{code:"DOCS"},{code:"DECS"}
  ]);

  const priority = ([
    {code:"None"},{code:"Low"},{code:"Medium"},{code:"High"}
    ]);

    

    const [userInput, setUserInput] = useState(
    { 
      CertificateID: '',
        AcNo : allstate.Account.AcNo,
        HolderName : '',
        InsuredName : '',
       Addtional:false,
      Mortgagee:false,
      LossPayee:false,
        Address:'',
        PostalCode : '',
        NoticeDays : '',
        Operation : '',
        Internal : ''
  
    }
    );
const handleChange = evt => {
const name = evt.target.name;
let newValue = evt.target.value;
setUserInput(userInput => ({
  ...userInput, 
  [name]: newValue
})
);
}

const selectFile = (event) => {
// let filedatas = event.target.files;
// filedatas && filedatas.map((file)=>{
// setSelectedFiles(file);
// })
setSelectedFiles([
...selectedFiles,
event.target.files[0]
,
]);
setFileInfos([
  ...fileInfos,
  {
    Id:uuidv4(),
    Name:event.target.files[0].name,
    fileUrl:URL.createObjectURL(event.target.files[0]),
    filetype:event.target.files[0].type,
  },
]);

//setSelectedFiles(event.target.files[0]);
console.log("Files Data",event.target.files[0]);
console.log("SelectedFiles",selectedFiles);
};

let history = useHistory();

const createcertificate = (evt) => {
  evt.preventDefault();
  ///alert("test");
  console.log("vales",userInput)
        let formData = new FormData();
formData.append('CertificateID',certificateId);
formData.append('AcNo',allstate.Account.AcNo);
formData.append('HolderName', userInput.HolderName);
formData.append('InsuredName', userInput.InsuredName);

formData.append('Addtional',userInput.Addtional);
formData.append('Mortgagee',userInput.Mortgagee);
formData.append('LossPayee',userInput.LossPayee);
formData.append('Address',userInput.Address);
formData.append('PostalCode', userInput.PostalCode);
formData.append('NoticeDays', userInput.NoticeDays);
formData.append('Operation', userInput.Operation);
formData.append('Internal', userInput.Internal);
formData.append('UserId',  BussinessDetails.login.user.UserId);

selectedFiles && selectedFiles.map((file)=>{
  formData.append("files", file);
  console.log("files",file);
  })
console.log("append",selectedFiles);

  
  axios.post(`${API_URL}Policy/create-certificate`,formData)
      .then(res => {
        if(res.data.Success){    
            console.log(res.data);
            }

          //   if(res.data.Success){    
          //    //let bdmList = res.data.Data;
          //    history.push("/myaccounts/accountDetails/Notes");
          //    //alert(res.data.Message);
          // //  console.log(res.data.Message);//
          // //  history.push("/UserComp");
          //  }
          //  else {
          //   alert.info(res.data.Message);
          //  }

           if(res.data.Success){    
            
            alert.success(res.data.Message,{
              timeout:5000,
              onClose: () => {
                if(allstate.Account.AcNo != ""){
                  history.push("/myaccounts/accountDetails/Certificate");
                  }else{
                   history.push("/List");
                  }
              }
            });

            // if(allstate.Account.AcNo != ""){
            // history.push("/myaccounts/accountDetails/Certificate");
            // }else{
            //  history.push("/List");
            // }
          
          }
          else {
           alert.info(res.data.Message);
          }
   
     });
}
const producerChange = (event) => {
  setproducer(event.target.value);
  
};
const createdbyChange = (event) => {
  setcreatedby(event.target.value);
  
};

const descriptionChange = (event) => {
  setdescription(event.target.value);
  
};
const notesChange = (event) => {
    setnotes(event.target.value);
    
  };
const handleDateChange = (date) => {
  setSelectedDate(date);
};
const userDetailsinfo = useSelector(state=>state);
useEffect(()=>{




let formData = new FormData();
selectedFiles && selectedFiles.map((file)=>{
formData.append("file", file);
console.log("file",file);
})
let item = userInput;
item['FileDatas'] = formData;
setUserInput(item);
console.log("formData",formData);
console.log("date", userInput)
},[selectedFiles])


useEffect(()=>{
  axios.get(`${API_URL}Quote/get-CertificateAccountName/${allstate.Account.AcNo}`)
  .then(res => {
    if(res.data.Success){    
           //let bdmList = res.data.Data;
           //let stateinput = input;
           console.log("account",res.data.Resource.Data)
           setproducer(res.data.Resource.Data);
         
         
         }

  });
console.log("props Certificate",props);
  if (props.location.state.id !=="" && props.location.state.id !== "0") {

    axios.post(`${API_URL}Policy/get-certificate/${props.location.state.id}`)
    .then(res => {
      if(res.data.Success){    
             //let bdmList = res.data.Data;
             //let stateinput = input;
             setUserInput(res.data.Data.Certificates);
             res.data.Data.Files.map((data)=>{
              data.fileUrl = API_URL + data.fileUrl;
              });
             setFileInfos(res.data.Data.Files);
             setid(res.data.Data.Certificates.CertificateID);
           
           }
  
    });
    
  }

},[props.location]);


const cellRenderfile = (data) => {
  return <FileViewe Path={data.data.fileUrl} Name={data.data.Name}/>
}
const handleCheckedChange = (evt) => {
  console.log("test",evt.target.name)
  const name = evt.target.name;
let newValue = evt.target.checked;
setUserInput(userInput => ({
  ...userInput, 
  [name]: newValue
})
);
  
};
const NoticeDays = ([
  {code:"None"},{code:"30"},{code:"60"},{code:"90"}
  ]);

return(
  <div className="Quotemain">
  <form className="field"
        onSubmit={createcertificate}
        autoComplete="off">

<Grid container spacing={3} >
<Grid item md={12}><h3></h3></Grid>
</Grid>

<Grid container spacing={3}>

  <Grid item md={4}>
     
    {/* <TextField   id="name" name="HolderName"   onChange={handleChange} label="Certified Holder Name" variant="outlined" value={userInput.HolderName}  /> */}
    <FormControl variant="outlined" className="select">
                     <InputLabel id="demo-simple-select-outlined-label">Named Insured</InputLabel>
                     <Select
                       labelId="demo-simple-select-outlined-label"
                       id="demo-simple-select-outlined"
                       name="InsuredName"
                       value={userInput.InsuredName}
                       onChange={handleChange}
                       label="Note code"
                     >
                       
                       { producer && producer.map((item, index)=>{
                                   return(
                                   <MenuItem key={index} value={item.Value}>{item.Value}</MenuItem>)
               
                                   })}  
                      
                     </Select>
                   </FormControl>
      </Grid>
      

      <Grid item md={4}>
      {/* <TextField required  id="CreatedBy" name="CreatedBy" readonly label="CreatedBy" variant="outlined"  value={userDetailsinfo.login.user.NameEn} onChange={producerChange} /> */}
    
      <TextField   id="HolderName" name="HolderName"   onChange={handleChange} label="Certified Holder Name" variant="outlined" value={userInput.HolderName}
     />
      </Grid>
      <Grid item md={4} className="revenuecheck">
                        <FormControlLabel
                        control={<Checkbox checked={userInput.Addtional}  onChange={e => handleCheckedChange(e)} name="Addtional" />}
                        label="Additional Insured"
                        />
                        
  <FormControlLabel control={<Checkbox checked={userInput.Mortgagee}  onChange={e => handleCheckedChange(e)} />} name="Mortgagee" label="Mortgagee" />
  
  
  <FormControlLabel control={<Checkbox checked={userInput.LossPayee}  onChange={e => handleCheckedChange(e)} />}  name="LossPayee"  label="Loss Payee" />

{/* <FormControl component="fieldset" className="radio-row">
         <FormLabel component="legend"><h6>LOSS TYPE</h6></FormLabel> */}
        {/* <RadioGroup aria-label="gender" name="gender1" > */}
        {/* <RadioGroup aria-label="BusinessType" name="InsuranceType" value={userInput.InsuranceType} onChange={handleChange}>
        <FormControlLabel value={"1"} control={<Radio />} label="Addtional" />
        <FormControlLabel value={"2"} control={<Radio />} label="Mortgagee" />
        <FormControlLabel value={"3"} control={<Radio />} label="Loss Payee" />
   
         </RadioGroup>
        </FormControl> */}
  </Grid>
 
      
      </Grid>
  
    
      <Grid container spacing={6}>
      <Grid item md={6}>
      {/* <TextField required  id="CreatedBy" name="CreatedBy" readonly label="CreatedBy" variant="outlined"  value={userDetailsinfo.login.user.NameEn} onChange={producerChange} /> */}
      <TextField   id="Address" name="Address"   onChange={handleChange} label="Address" variant="outlined" value={userInput.Address}
     />
      </Grid>
      <Grid item md={3}>
      {/* <TextField required  id="CreatedBy" name="CreatedBy" readonly label="CreatedBy" variant="outlined"  value={userDetailsinfo.login.user.NameEn} onChange={producerChange} /> */}
      <TextField required  id="PostalCode" name="PostalCode"   onChange={handleChange} label="Picode " variant="outlined" value={userInput.PostalCode}
     />
      </Grid> 
      <Grid item md={3}>
      {/* <TextField required  id="CreatedBy" name="CreatedBy" readonly label="CreatedBy" variant="outlined"  value={userDetailsinfo.login.user.NameEn} onChange={producerChange} /> */}
      {/* <TextField required  id="Noticedays" name="NoticeDays"   onChange={handleChange} label="Cancellation Notice Days" variant="outlined" value={userInput.NoticeDays}
     /> */}
     <FormControl variant="outlined" className="select">
      <InputLabel id="demo-simple-select-outlined-label">Cancellation Notice Days</InputLabel>
      <Select
        labelId="demo-simple-select-outlined-label"
        id="NoticeDays"
        name="NoticeDays"
       value={userInput.NoticeDays}
      onChange={e => handleChange(e)}
        label="NoticeDays"
      >
        
        { NoticeDays && NoticeDays.map((item, index)=>{
                    return(
                    <MenuItem key={index} value={item.code}>{item.code}</MenuItem>)

                    })}  
       
      </Select>
    </FormControl>
      </Grid> 
     </Grid>
      
          <br /> 
      <Grid container spacing={6}>
      <Grid item md={6}>
          <div className="description">
          <textarea id="Description" name="Operation" value={userInput.Operation} onChange={handleChange}  placeholder="Description of Operation"></textarea>
          </div>
            
        </Grid>
        <Grid item md={6}>
          <div className="description">
          <textarea id="InternalDescription" name="Internal" value={userInput.Internal} onChange={handleChange}  placeholder="Internal Description"></textarea>
          </div>
            
        </Grid>
        <Grid item md={4}></Grid>
        <Grid item md={4}></Grid>
        </Grid>
        <Grid container spacing={3} >
<Grid item md={12}>
<h4 className="clrBlue" style={{'marginTop':'20px'}}>UPLOAD YOUR FILES</h4>
<div className="fileUpload">
<Button variant="contained" name="save" color="primary" type="button" ><CloudUploadIcon></CloudUploadIcon></Button>
<input type="file" className="custom-file-upload" onChange={selectFile} />
</div>

</Grid>
</Grid>
<Grid container spacing={3} >
<Grid item md={12}>
<div className="card">
<div className="card-header">List of Files</div>
<DataGrid
id="gridContainer"
dataSource={fileInfos}
//keyExpr="ID"
showBorders={true}
>
<Editing
mode="row"
useIcons={true}
allowUpdating={true}
allowDeleting={true}
/>
<Column type="buttons" width={110}
buttons={['delete']} />
<Column dataField="Name" caption="Name" />
<Column dataField="Id"  caption="Actions" width={200} cellRender={cellRenderfile} />
</DataGrid>
</div>

</Grid>
</Grid>

<div className="btn-group">
           <Button variant="contained" onClick={handleClose}>CANCEL</Button>
<Button variant="contained" color="primary" type="submit">
  SAVE
</Button>
           </div>


</form>
</div>
)
}
export default CreatecertificateInsurance