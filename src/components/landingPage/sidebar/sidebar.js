import React, { useEffect } from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PersonIcon from '@material-ui/icons/Person';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import AssessmentIcon from '@material-ui/icons/Assessment';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import GroupAddOutlinedIcon from '@material-ui/icons/GroupAddOutlined';
import NotesIcon from '@material-ui/icons/Notes';
import {Link} from "react-router-dom";
import "./sidebar.css";
import {useSelector, useDispatch,connect} from "react-redux";


const useStyles = makeStyles({
    root: {
     
      
    },
  });

const Sidebar = ()=> {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const user = useSelector((state) => state.login.user);
    const handleChange = (event, newValue) => {
      setValue(newValue);
    };
  
    return (
      <Paper square className="sideBar">
      
        <Tabs
        classname="sidebar"
          value={value}
          onChange={handleChange}
          variant="fullWidth"
          indicatorColor="secondary"
          textColor="secondary"
          aria-label="icon label tabs example"
        >
               
          <Tab icon={<DashboardIcon />}  to="/" label="Dashboard" component={Link}/>
          <Tab icon={<PersonIcon />}  to={{pathname:"/myaccounts"}} component={Link} label="my account" />
          <Tab icon={<PersonAddIcon />} to={{pathname:"/Account/New", state:{id:"0"}}} component={Link} label="create account" />
          <Tab icon={<NotesIcon />}   to={{pathname:"/List"}} label="Notes / Task" component={Link}/>
          <Tab icon={<NotesIcon />}   to={{pathname:"/Log"}} label="Logs" component={Link}/>
          <Tab icon={<AssessmentIcon />} to={{pathname:"/Statistics"}} component={Link} label="statistics" />
          {/* <Tab icon={<SettingsIcon />} label="settings" /> */}
          {user.UserTypeName !=="BDM" && <Tab icon={<AssessmentIcon />} to={{pathname:"/classlist"}} component={Link} label="Class and Operation" /> }
          {user.UserTypeName !=="BDM" && <Tab icon={<GroupAddOutlinedIcon />}  to={{pathname:"/UserComp"}} component={Link} label="User Management" /> }
          
          {/* <Tab icon={<DashboardIcon />}   to={{pathname:"/business"}} label="PDFView" component={Link}/> */}
        </Tabs>

      </Paper>
    );
  }
export default Sidebar;