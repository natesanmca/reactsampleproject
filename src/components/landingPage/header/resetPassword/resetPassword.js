import React, { useState } from "react";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";

import TextField from "@material-ui/core/TextField";

import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import { useSelector, useDispatch } from "react-redux";
import { setResetPassword } from "../../../../Redux/Actions/actions";

const ResetPassword = () => {
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confrimNewPassword, setConfrimNewPassword] = useState("");
  const [IsoldPassError, setIsoldPassError] = useState(false);
  const [IsoldPassErrorTxt, setIsoldPassErrorTxt] = useState("");
  const [IsnewPassError, setIsnewPassError] = useState(false);
  const [IsnewPassErrorTxt, setIsnewPassErrorTxt] = useState("");
  const [IsconfrimPassError, setIsconfrimPassError] = useState(false);
  const [IsconfrimPassErrorTxt, setIsconfrimPassErrorTxt] = useState(false);
  const [IsResetbtnClicked, setIsResetbtnClicked] = useState(false);
  const [isPasswordNewDirty, setIsPassNewDirty] = useState(false);
  const [isPasswordConfrimDirty, setisPasswordConfrimDirty] = useState(false);

  const userDetailsinfo = useSelector((state) => state);
  const dispatch = useDispatch();
  const previousPass = userDetailsinfo.login.user.Password;
  //console.log(previousPass);
  const onChangeold = (event) => {
    setIsoldPassError(false);
    const oldPass = event.target.value;
    setOldPassword(oldPass);
    if (IsResetbtnClicked) {
      resetValidation(oldPass);
    }
  };
  const onChangenew = (event) => {
    setIsnewPassError(false);
    setIsPassNewDirty(true);
    const newPass = event.target.value;
    setNewPassword(newPass);
    if (IsResetbtnClicked) {
      resetValidation(oldPassword, newPass, confrimNewPassword, true);
    }
  };
  const onChangeConfrim = (event) => {
    setIsconfrimPassError(false);
    setisPasswordConfrimDirty(true);
    const confrimPass = event.target.value;
    setConfrimNewPassword(confrimPass);
    if (IsResetbtnClicked) {
      resetValidation(
        oldPassword,
        newPassword,
        confrimPass,
        isPasswordNewDirty,
        true
      );
    }
  };

  const resetValidation = (
    oldPass = oldPassword,
    newPass = newPassword,
    confrimPass = confrimNewPassword,
    isNewPasswordDirty = isPasswordNewDirty,
    isConfirmpasswordDirty = isPasswordConfrimDirty
  ) => {
    let isValid = true;
    // if (!(oldPass == previousPass)) {
    //   setIsoldPassError(true);
    //   setIsoldPassErrorTxt("Please enter the correct Password");
    //   isValid = false;
    // } else {
    //   setIsoldPassError(false);
    //   setIsoldPassErrorTxt("");
    // }
    var reg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$/;
    var PasswordPattern = reg.test(newPass);
    if ((!PasswordPattern) && isNewPasswordDirty) {
      setIsnewPassError(true);
      setIsnewPassErrorTxt(
        "Password must be at least 4- 8 character, and must include at least one upper case letter, one lower case letter, and one numeric digit."
      );
      isValid = false;
    } else {
      setIsnewPassError(false);
      setIsnewPassErrorTxt("");
    }
    if (!(newPass == confrimPass) && (isConfirmpasswordDirty|| isNewPasswordDirty)) {
      setIsconfrimPassError(true);
      setIsconfrimPassErrorTxt(
        "Confrim Password doesn't match with new password"
      );
      isValid = false;
    } else {
      setIsconfrimPassError(false);
      setIsconfrimPassErrorTxt("");
    }
    return isValid;
  };

  const resetSubmit = (event) => {
    event.preventDefault();
    setIsResetbtnClicked(true);
    const Userid = userDetailsinfo.login.user.UserId;
    if (resetValidation(oldPassword, newPassword, confrimNewPassword)) {
      const user = {
        UserId: Userid,
        OldPassword: oldPassword,
        NewPassword: newPassword,
      };
      console.log(user);
      dispatch(setResetPassword(user));
      // setOldPassword("");
      // setNewPassword("");
      // setConfrimNewPassword("");
    }
  };

  return (
    <React.Fragment>
      <Container maxWidth="">
        <Grid container>
          <Grid item md={12}>
            <div className="mainContent">
              <h1>Reset Password</h1>
            </div>
          </Grid>
        </Grid>

        <Grid container className="justify-center">
          <Container maxWidth="xs">
            <Paper elevation={3} className="paper">
              <form
                className="reset-field"
                onSubmit={resetSubmit}
                autoComplete="off"
              >
                <Grid container spacing={3}>
                  <Grid item sm={12}>
                    <TextField
                    required
                      id="outlined-basic"
                      type="password"
                      name="oldPassword"
                      value={oldPassword}
                      error={IsoldPassError}
                      helperText={IsoldPassErrorTxt}
                      onChange={onChangeold}
                      label="Old Password"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item sm={12}>
                    <TextField
                    required
                      id="outlined-basic1"
                      type="password"
                      name="newPassword"
                      value={newPassword}
                      error={IsnewPassError}
                      helperText={IsnewPassErrorTxt}
                      onChange={onChangenew}
                      label="New Password"
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item sm={12}>
                    <TextField
                    required
                      id="outlined-basic"
                      type="password"
                      name="confrimNewPassword"
                      value={confrimNewPassword}
                      error={IsconfrimPassError}
                      helperText={IsconfrimPassErrorTxt}
                      onChange={onChangeConfrim}
                      label="Confrim Password"
                      variant="outlined"
                    />
                  </Grid>
                </Grid>

                <Grid container spacing={3}>
                  <Grid item sm={12}>
                    <Button type="submit" variant="contained" color="primary">
                      submit
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </Paper>
          </Container>
        </Grid>
      </Container>
    </React.Fragment>
  );
};

export default ResetPassword;
