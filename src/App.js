import React from 'react';
//import logo from './logo.svg';
import './App.css';
//import Login from './components/login/login';
import Login from './components/loginnew/login';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './components/home/home';
import Account from './components/account/account';
import { useHistory } from "react-router-dom";
import LandingPage from './components/landingPage';


function App() {
console.log("Base_Url",process.env.REACT_APP_BASE_API_URL);
  return (
    <Router >
      <Route path="/" exact component={Login} />
      <Route path="/Home" component={Home} />
      <Route path="/LandingPage" render={()=>
<LandingPage/>
      }/>
     
    </Router>
    
    // <div className="App" style={{height:"100%"}}>
    //   <Login/>
    // </div>
  );
}

export default App;
